package tr.com.metamorfoz.core.test;
import java.util.Properties;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.ota.domain.OtaSimProfile;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;

public class HibernateSchemaGenerator {

    public static void main(String[] args) {
    	
        Configuration config = new Configuration();

        Properties properties = new Properties();

        properties.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
        properties.put("hibernate.connection.url", "jdbc:oracle:thin:@127.0.0.1:1521:ORCL"); 
        properties.put("hibernate.connection.username", "SCPC");
        properties.put("hibernate.connection.password", "P_SCPC");
        properties.put("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "validate");
        config.setProperties(properties);

        config.addAnnotatedClass(DstkCampaignStatistic.class);
        config.addAnnotatedClass(DstkInstance.class);
        config.addAnnotatedClass(DstkModule.class);
        config.addAnnotatedClass(RfmGsmOperator.class);
//        config.addAnnotatedClass(OtaSimProfile.class);
        config.addAnnotatedClass(OtaTransportKey.class);

        //UPDATE
        SchemaUpdate schemaUpdate = new SchemaUpdate(config);
        schemaUpdate.setOutputFile("schema_update.sql");
        schemaUpdate.execute(true, false);
        
        //FIRST CREATE
//        SchemaExport schemaExport = new SchemaExport(config);
//        schemaExport.setOutputFile("schema_create.sql");
//        schemaExport.create(true, false);

    }

}


