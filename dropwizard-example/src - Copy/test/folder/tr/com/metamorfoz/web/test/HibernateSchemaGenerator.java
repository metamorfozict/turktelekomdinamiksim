package tr.com.metamorfoz.web.test;
import java.util.Properties;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.ota.domain.OtaSimProfile;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;

public class HibernateSchemaGenerator {

    public static void main(String[] args) {
    	
        Configuration config = new Configuration();

        Properties properties = new Properties();

        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.connection.url", "jdbc:mysql://127.0.0.1:3306/otass?useUnicode=true&characterEncoding=UTF-8"); 
        properties.put("hibernate.connection.username", "root");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        config.setProperties(properties);

        config.addAnnotatedClass(DstkCampaignStatistic.class);
        config.addAnnotatedClass(DstkInstance.class);
        config.addAnnotatedClass(DstkModule.class);
        config.addAnnotatedClass(RfmGsmOperator.class);
        config.addAnnotatedClass(OtaSimProfile.class);
        config.addAnnotatedClass(OtaTransportKey.class);

        //UPDATE
        SchemaUpdate schemaUpdate = new SchemaUpdate(config);
        schemaUpdate.setOutputFile("schema_update.sql");
        schemaUpdate.execute(true, true);
        
        //FIRST CREATE
//        SchemaExport schemaExport = new SchemaExport(config);
//        schemaExport.setOutputFile("schema_create.sql");
//        schemaExport.create(true, true);

    }

}


