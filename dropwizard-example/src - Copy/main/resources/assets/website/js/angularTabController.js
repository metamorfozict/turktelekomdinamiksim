'use strict';

/* App Module */

var tabApp = angular.module('tabApp', []);

tabApp.controller('TabsCtrl', ['$scope',
   function ($scope) {
		
	$scope.tabs = [
	               { "label" : "Jobs" },
	               { "label" : "Invoices" },
	               { "label" : "Payments" }
               	  ]; 
	
	$scope.selectedTab = $scope.tabs[0];    
	   $scope.setSelectedTab = function(tab) {
	     $scope.selectedTab = tab;
	   }
	   
	   $scope.tabClass = function(tab) {
	     if ($scope.selectedTab == tab) {
	       return "active";
	     } else {
	       return "";
	     }
	   }
	
	
   }]);

