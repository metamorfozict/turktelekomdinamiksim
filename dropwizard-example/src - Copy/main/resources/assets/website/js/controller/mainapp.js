'use strict';

/* App Module */
var mainApp = angular.module('mainApp', [ 'ui.sortable', 'ui.bootstrap.tpls', 'ui.bootstrap.modal', 'ui.select2', 'ui.slider', 'ui.utils', 'ngRoute', 'mainControllers' ]);

mainApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/transportkeytable', {
		templateUrl : 'transportkeytable.html',
		controller : 'tableController'
	}).when('/transportkey/:itemId', {
		templateUrl : 'transportkeydetail.html',
		controller : 'detailController'
	}).when('/home', {
		templateUrl : 'executivereport.html',
		controller : 'executiveReportController'
	}).when('/campaign/:campaignType', {
		templateUrl : 'campaign.html',
		controller : 'campaignController'
	}).when('/rfmdesign', {
		templateUrl : 'rfmdesign.html',
		controller : 'rfmDesignController'
	}).when('/ramdesign', {
		templateUrl : 'ramdesign.html',
		controller : 'ramDesignController'
	}).when('/interactifdesign', {
		templateUrl : 'interactifdesign.html',
		controller : 'interactifDesignController'
	}).when('/menudesign', {
		templateUrl : 'menudesign.html',
		controller : 'menuDesignController'
	}).when('/instancedesign', {
		templateUrl : 'instancedesign.html',
		controller : 'instanceDesignController'
	}).when('/ramdesign', {
		templateUrl : 'ramdesign.html',
		controller : 'ramDesignController'
	}).when('/rfmdesign', {
		templateUrl : 'rfmdesign.html',
		controller : 'rfmDesignController'
	}).when('/scheduler', {
		templateUrl : 'scheduler.html',
		controller : 'schedulerController'
	}).when('/msisdnreport', {
		templateUrl : 'msisdnreport.html',
		controller : 'schedulerController'
	}).when('/deviceinfo', {
		templateUrl : 'deviceinfo.html',
		controller : 'tableController'
	}).when('/locationinfo', {
		templateUrl : 'locationinfo.html',
		controller : 'tableController'
	}).when('/designstable', {
		templateUrl : 'designstable.html',
		controller : 'designsTableController'
	}).when('/code', {
		templateUrl : 'codeconfiguration.html',
		controller : 'detailController'
	}).
    otherwise({
        redirectTo: '/home'
    });
} ]);

mainApp.directive('knobSent', function() {
	return {
		require : 'ngModel',
		scope : {
			model : '=ngModel'
		},
		controller : function($scope, $element, $timeout) {
			var el = $($element);
			$scope.$watch('model', function(v) {
				var el = $($element);
				el.val(v).trigger('change');
			});
		},

		link : function($scope, $element, $attrs, $ngModel) {
			var el = $($element);
			el.val($scope.value).knob({
				'change' : function(v) {
					$scope.$apply(function() {
						$ngModel.$setViewValue(v);
					});
				}
			});
		}
	}

});

mainApp.directive('donutChart', function() {

	return {

		// required to make it work as an element
		restrict : 'E',
		template : '<div></div>',
		replace : true,
		// observe and manipulate the DOM
		link : function($scope, element, attrs) {

			var data = $scope[attrs.data];

			// Morris Donut Chart
			Morris.Donut({
				element : element,
				data : data,
				labelColor : '#59839f',
				colors : [ "#59839f", "#9db7c9", "#d7dfec" ],
				formatter : function(y) {
					return "%" + y
				}
			});
		}

	};

});

mainApp
		.directive(
				'lineChart',
				function() {

					return {

						// required to make it work as an element
						restrict : 'E',
						template : '<div></div>',
						replace : true,
						// observe and manipulate the DOM
						link : function($scope, element, attrs) {

							var data = $scope[attrs.data], xkey = $scope[attrs.xkey], ykeys = $scope[attrs.ykeys], labels = $scope[attrs.labels];

							// Morris Line Chart
							Morris.Line({
								element : element,
								data : data,
								xkey : xkey,
								xLabels : 'day',
								ykeys : ykeys,
								labels : labels
							});
						}

					};

				});


var chart = null, opts = {
		series : {
			lines : {
				show : true,
				lineWidth : 1,
				fill : true,
				fillColor : {
					colors : [ {
						opacity : 0.05
					}, {
						opacity : 0.09
					} ]
				}
			},
			points : {
				show : true,
				lineWidth : 2,
				radius : 3
			},
			shadowSize : 0,
			stack : true
		},
		grid : {
			hoverable : true,
			clickable : true,
			tickColor : "#f9f9f9",
			borderWidth : 0
		},
		legend : {
			// show: false
			labelBoxBorderColor : "#fff"
		},
		colors : [ "#a7b5c5", "#30a0eb" ],
		xaxis : {
			ticks : [],
			font : {
				size : 12,
				family : "Open Sans, Arial",
				variant : "small-caps",
				color : "#9da3a9"
			}
		},
		yaxis : {
			ticks : 3,
			tickDecimals : 0,
			font : {
				size : 12,
				color : "#9da3a9"
			}
		}
	};

mainApp.directive('flotChart', function() {
	return {
		restrict : 'E',
		template : '<div></div>',
		replace : true,
		link : function($scope, elem, attrs) {

			opts.xaxis.ticks = attrs.ngModel.ticks;
			
			$scope.$watch(attrs.ngModel, function(v) {
			
				opts.xaxis.ticks = v.ticks;
				chart = $.plot(elem, v.data, opts);
				elem.show();
			});
			
		}
	};
});

var mainControllers = angular.module('mainControllers', []);

mainApp.directive("customSort", function() {
	return {
		restrict : 'A',
		transclude : true,
		scope : {
			order : '=',
			sort : '='
		},
		template : ' <a ng-click="sort_by(order)">'
				+ '    <span ng-transclude></span>'
				+ '    <i ng-class="selectedCls(order)"></i>' + '</a>',
		link : function(scope) {

			// change sorting order
			scope.sort_by = function(newSortingOrder) {
				var sort = scope.sort;

				if (sort.sortingOrder == newSortingOrder) {
					sort.reverse = !sort.reverse;
				}

				sort.sortingOrder = newSortingOrder;
			};

			scope.selectedCls = function(column) {
				if (column == scope.sort.sortingOrder) {
					return ('icon-chevron-' + ((scope.sort.reverse) ? 'down'
							: 'up'));
				} else {
					return 'icon-sort'
				}
			};
		}// end link
	}
});

mainApp.factory('alert', ['$compile', '$rootScope', '$timeout', '$animate', function($compile, $rootScope, $timeout, $animate) {
	var currentAlert = {};
	return function(title, message) {
		var scope = $rootScope.$new();

		scope.message = message;
		scope.title = title;

		if (currentAlert.close)
			currentAlert.close();

		var alert = $($('#alert-box').html());
		$compile(alert.contents())(scope);

		$('body').append(alert);
		$timeout(function() {
			alert.addClass('in');
		});

		currentAlert.close = scope.close = function() {
			alert.removeClass('in');
			$timeout(function() {
				alert.remove();
			}, 2500);
		}
    }
}]);

//Alert
window.alert = function(title, message) {
  var elem = angular.element(document.querySelector('[ng-controller]'));
  injector = elem.injector();
  var alert = injector.get('alert');
  alert(title, message);
}


//Add / Edit item modal
mainApp.directive('addEditModal', ['$animate', '$timeout', '$compile', '$rootScope', '$document', '$controller', '$window', function($animate, $timeout, $compile, $rootScope, $document, $controller, $window) {
	var currentModal = {};
	$document.click(function() {
		if (currentModal.close) {
			currentModal.close();
		}
	});

	return {
		scope: {
			items: '=',
			item: '='
		},
		link: function(scope, element, attrs) {
			element.click(function(e) {

				e.stopPropagation();
				if (element.hasClass('active')) {
					return;
				}

				element.addClass('active');

				if (currentModal.remove) {
					currentModal.close();
				}

				// Create modal position
				var template = $(attrs.template  || '#editModal').html();
				currentModal = angular.element(template);

				currentModal.find('.arrow').css({
					'top': element.offset().top-$($window).scrollTop()
				});

				if(attrs.addEditModal != 'static')
					currentModal.css({
						left:element.offset().left+element.width()+10,
						top: element.offset().top-25
					});

				currentModal.click(function(e){
					e.stopPropagation();
				});

				// Scope
				var _scope = $rootScope.$new(true);
				$compile(currentModal)(_scope);

				_scope._item = {};
				if (scope.item) {
					_scope._item = angular.copy(scope.item)
					_scope._edit = true;
				}
				if (scope.items) {
					_scope.items = scope.items;
					_scope._edit = false;
				}

				_scope.add = function() {
					if (!_scope._item.title && !_scope._item.function)
						return window.alert('Alert', 'Please select/fill any field!');
					if (!_scope._item.items)
						_scope._item.items = [];
					scope.items.push(_scope._item);
					currentModal.close();
				}

				_scope.edit = function() {
					if (!_scope._item.title && !_scope._item.function)
						return window.alert('Alert', 'Please select/fill any field!');
					
					angular.forEach(_scope._item, function(el, i) {
						if (i.substr(0, 2) != '$$') {
							console.log(i);
							scope.item[i] = _scope._item[i];
						}
					});

					currentModal.close();
				}

				_scope.close = currentModal.close = function() {
					currentModal.remove();
					currentModal = {};
					element.removeClass('active');
				}

				// Controller For Modal
				var ctrlLocals = {};
				ctrlLocals.$scope = _scope;
				$controller(['$scope', function($scope) {

				}], ctrlLocals);


				$document.find('body').append(currentModal);
				$timeout(function(){
					$animate.addClass(currentModal, 'open in');
				});

				e.stopPropagation();
			});
		}
	}
}]);

// Tree view fix!
mainApp.directive('lastElement', function() {
	return {
		scope: {
			last: '=',
			item: '='
		},
		link: function(scope, element, attrs) {
			scope.$watch(function() {
				if (scope.last)
					if(element.find('ul').height())
						element.css({
							height: 55,
							marginBottom:element.find('ul').height()
						});
			}, true);
		}
	}
});

mainApp.filter('toDash', function() {
	return function(value) {
		if (value)
			return value.replace(/([A-Z])/g, function($1){ return "-"+$1.toLowerCase(); });
		else
			return '';
	}
});

mainApp.directive('uniform',function($timeout){
  return {
    restrict:'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      $(element).uniform({useID: false});
      scope.$watch(function() {return ngModel.$modelValue}, function() {
        $timeout(jQuery.uniform.update, 0);
      });
    }
  };
});