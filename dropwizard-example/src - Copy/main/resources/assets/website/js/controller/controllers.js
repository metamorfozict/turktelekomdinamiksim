// Mobile menu controller
mainControllers.controller('menuDesignController', ['$scope', '$http', function($scope, $http) {
	$scope.editMode = false;
	
	$scope.cancelMenu = function() {
		
		$scope.menuItems = {
				title: "",
				items: []
		};
	}
	
	$scope.activeItem = null;

	$scope.toggleEditMode = function() {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		$scope.editMode = !$scope.editMode;
	}
	
	// Item Actions
	$scope.toggleItemActive = function(item) {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		item.active = true;
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.subItemEdit = function(item) {
		if (item.items && item.title && !$scope.editMode) {
			$scope.activeItem = item;
			$scope.editMode = false;
		}
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y',
		handle: ".icon-exchange"
	};

  	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
		
		$http({
  		    method: 'POST',
  		    url: 'http://127.0.0.1:8080/metamorfoz-sgui/DSTKMenuParser',
  		    data: $scope.operation + "dstkmenu" + $scope.order + "dstkmenu" + $scope.version + "dstkmenu" + $scope.instance + "dstkmenu" + JSON.stringify($scope.menuItems),
  		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  		}).
        success(function(data, status, headers, config) {
        	window.alert("Alert", "Menu version : " + $scope.version + " instance : " + $scope.instance + " " + data);
        }).
        error(function(data, status, headers, config) {
        	window.alert("Alert", "Menu version : " + $scope.version + " instance : " + $scope.instance + " error status : " + status + " " + data);
        });
		
	};

}]);

mainControllers.filter("truncate", function(){
    return function(text, length){
    	
    	if (text){
    		return text.substring(0, text.indexOf("_v"));	
    	} else {
    		return "";
    	}
    	
        
    }
});

// Mobile menu controller
mainControllers.controller('instanceDesignController', ['$scope', '$http', function($scope, $http) {
	$scope.editMode = false;

	$scope.activeItem = null;

	$scope.toggleEditMode = function() {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		$scope.editMode = !$scope.editMode;
	}
	
	// Item Actions
	$scope.toggleItemActive = function(item) {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		item.active = true;
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.subItemEdit = function(item) {
		if (item.items && item.title && !$scope.editMode) {
			$scope.activeItem = item;
			$scope.editMode = false;
		}
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y',
		handle: ".icon-exchange"
	};

  	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
		
		$http({
  		    method: 'POST',
  		    url: 'http://127.0.0.1:8080/metamorfoz-sgui/DSTKMainMenuParser',
  		    data: $scope.operation + "dstkmenu" + $scope.instance + "dstkmenu" + $scope.version + "dstkmenu" + JSON.stringify($scope.menuItems),
  		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  		}).
        success(function(data, status, headers, config) {
        	window.alert("Alert", "Menu name : " + $scope.version + " " + data);
        }).
        error(function(data, status, headers, config) {
        	window.alert("Alert", "Menu name : " + $scope.version + " error status : " + status + " " + data);
        });
		
	};

}]);

// Mobile menu controller
mainControllers.controller('menuDesignController', ['$scope', '$http', function($scope, $http) {
	$scope.editMode = false;

	$scope.activeItem = null;

	$scope.toggleEditMode = function() {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		$scope.editMode = !$scope.editMode;
	}
	
	// Item Actions
	$scope.toggleItemActive = function(item) {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		item.active = true;
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.subItemEdit = function(item) {
		if (item.items && item.title && !$scope.editMode) {
			$scope.activeItem = item;
			$scope.editMode = false;
		}
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y',
		handle: ".icon-exchange"
	};

  	$scope.exportJSON = function() {
		
		var json = {
					  "instance" : $scope.instance,
					  "instanceHeader" :  $scope.instanceHeader,
					  "instanceVersion" :  $scope.version,
					  "menuContent" : $scope.menuItems
					};
		
		console.log("json:" + JSON.stringify(json));
		
		$http({
  		    method: 'POST',
  		    url: 'http://localhost:8080/api/dstkinstance',
  		    data: json,
  		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  		}).
        success(function(data, status, headers, config) {
        	console.log("success");
        	window.alert("Alert", "Menu name : " + $scope.version + " " + data);
        }).
        error(function(data, status, headers, config) {
        	console.log("error");
        	window.alert("Alert", "Menu name : " + $scope.version + " error status : " + status + " " + data);
        });
		
	};

}]);

// Tree menu controller
mainControllers.controller('ramDesignController', ['$scope', function($scope) {
	$scope.menuItems = [
		{
			title: "ISD (Card Manager - A0000000000000189010111200000001)",
			items: [
				{
					title: "RFM PKG (A00000000000001890141100B0000101)",
					pkg:{aid:"A00000000000001890141100B0000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",	
					items:[]
				},
				{
					title: "RAM PKG (A00000000000001890141100B0000101)",
					pkg:{aid:"A00000000000001890141100B0000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",	
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010101)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010201)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010301)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010301)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				}
				
			]
		}
	];

	$scope.removeItem = function(items, item) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
	}

}]);

// Tree menu controller
mainControllers.controller('rfmDesignController', ['$scope', function($scope) {
	$scope.menuItems = [
		{
			title: "Root Folder [3F 00]",
			description: "GSM DDF",
			type: "DDF",
			value: "",
			items: [
			        {
					title: "GSM [7F20]",
					description: "GSM DDF",
					type: "DDF",
					value: "",
					items:[{ title: "IMSI [6F07]"
						   , description: "holds the IMSI"
						   , type: "Transparent"
						   , value: "090807060504030201\r\n010203040506070809\r\n"}
						  ]
			        }
			        ]
		}
				
			];

	$scope.removeItem = function(items, item) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
	}

}]);

mainControllers.controller('interactifDesignController', ['$scope', '$modal', '$timeout', '$window', '$http', function($scope, $modal, $timeout, $window, $http) {
	
	$scope.selectItem = function(item) {
		$scope.currentItem = item;
		
		if ($scope.currentItem.type == 'getInput') {
			$window.document.getElementsByClassName("min")[0].innerHTML = $scope.currentItem.options.range[0];
			$window.document.getElementsByClassName("max")[0].innerHTML = $scope.currentItem.options.range[1];
		}
		
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
			$scope.currentItem = null;
			
			if (items.length != 0) {
				if (i == 0) {
					$scope.selectItem(items[0]);
					$scope.selectItem(items[0]);
				} else {
					$scope.selectItem(items[i - 1]);
					$scope.selectItem(items[i - 1]);
				}
			}
			
		}
	}

	$scope.addItem = function(type, items) {
		var item = {
			title: '',
			type: type,
			items: [],
			options: {}
		};

		if (!items) {
			
			if (item.type == 'getInput') {
				item.options.range = [0,15];
			}
			
			$scope.items.push(item);
			$scope.currentItem = item;
			
			if ($scope.currentItem.type == 'setupCall') {
				$scope.currentItem.title = $scope.defCall;
			}
			
			if ($scope.currentItem.type == 'sendSms') {
				$scope.currentItem.title = $scope.defSmsMess;
				$scope.currentItem.destination = $scope.defSmsNo;
			}
			
			if ($scope.currentItem.type == 'getInput') {
				$window.document.getElementsByClassName("min")[0].innerHTML = item.options.range[0];
				$window.document.getElementsByClassName("max")[0].innerHTML = item.options.range[1];
			}
			
			
		} else {
			var modalInstance = $modal.open({
				templateUrl: 'item-modal.html',
				controller: ['$scope', function($sscope) {
					$sscope.addItem = function(type) {
						item.type = type;
						
						if (type == 'getInput') {
							item.options.range = [0,15];
						}
						
						items.push(item);
						$scope.currentItem = item;
						
						if ($scope.currentItem.type == 'setupCall') {
							$scope.currentItem.title = $scope.defCall;
						}
						
						if ($scope.currentItem.type == 'sendSms') {
							$scope.currentItem.title = $scope.defSmsMess;
							$scope.currentItem.destination = $scope.defSmsNo;
						}

						if ($scope.currentItem.type == 'getInput') {
							try {
								$window.document.getElementsByClassName("min")[0].innerHTML = item.options.range[0];
								$window.document.getElementsByClassName("max")[0].innerHTML = item.options.range[1];
							} catch (err) {
								console.log(err);
							}
						}
						
						modalInstance.close();
					}

					$sscope.close = function() {
						modalInstance.dismiss('cancel');
					}
				}],
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});
		}

	}
	
	$scope.editOption = function(option) {

	}

	$scope.removeOption = function(option, options) {
		var i = options.indexOf(option);
		options.splice(i, 1);
	}

	$scope.editOption = function(option) {
		var modalInstance = $modal.open({
			templateUrl: 'add-option-modal.html',
			controller: ['$scope', function($sscope) {
				$timeout(function() {
					$('.modal').addClass('modal-sm');
				});

				$sscope.item = angular.copy(option);

				$sscope.addOption = function() {
					option.options.key = $sscope.item.options.key;
					option.options.value = $sscope.item.options.value;
					option.branch = $sscope.item.branch;
					modalInstance.close();
				}

				$sscope.close = function() {
					modalInstance.dismiss('cancel');
				}
			}],
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});
	}

	$scope.addOption = function(items) {
		var modalInstance = $modal.open({
			templateUrl: 'add-option-modal.html',
			controller: ['$scope', function($sscope) {
				$timeout(function() {
					$('.modal').addClass('modal-sm');
				});

				$sscope.item = {};

				$sscope.addOption = function() {
					if (!$sscope.item.options || !$sscope.item.options.key || !$sscope.item.options.value) {
						alert('Please fill all required field!');
						return;
					}

					items.push({
						type: 'selectOption',
						options: {
							key:{
								value: $sscope.item.options.key.value
							},
							value: {
								value: $sscope.item.options.value.value
							},
						},
						branch: $sscope.item.branch,
						items:[]
					});
					modalInstance.close();
				}

				$sscope.close = function() {
					modalInstance.dismiss('cancel');
				}
			}],
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y'
	};

	// Slider options
	$scope.sliderOptions = {
		range: true,
		min : 0,
		max : 120,
		values : [0,15],
		create: function(event, ui) {
			$(this).find('.ui-slider-handle').eq(0).append($('<span class="min">' + $(this).slider('values')[0] + '</span>'));
			$(this).find('.ui-slider-handle').eq(1).append($('<span class="max">' + $(this).slider('values')[1] + '</span>'));
		},
		slide: function( event, ui ) {
			$(this).find(".min").html(ui.values[0]);			
			$(this).find(".max").html(ui.values[1]);
		}
	};

	$scope.exportJSON = function() {
		
		console.log(JSON.stringify($scope.items));
		
		$http({
  		    method: 'POST',
  		    url: 'http://127.0.0.1:8080/metamorfoz-sgui/DSTKServiceParser',
  		    data: $scope.serviceName + "dstkservice" + JSON.stringify($scope.items) + "dstkservice" + $scope.serviceType + "dstkservice" + $scope.operation + "dstkservice" + $scope.moduleId,
  		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  		}).
        success(function(data, status, headers, config) {
        	if (data != null && data.indexOf("Success") > -1) {
        		
        		var modalInstance = $modal.open({
        			templateUrl: 'item-test.html',
        			controller: ['$scope', function($sscope) {
        				$sscope.addItem = function(type) {
        					modalInstance.close();
        					$scope.items = [];
        				}

        				$sscope.close = function() {
        					modalInstance.dismiss('cancel');
        					$scope.items = [];
        				}
        				
        				$sscope.liveTest = function() {
        					
        					$http({
        			  		    method: 'POST',
        			  		    url: 'http://127.0.0.1:8080/metamorfoz-sgui/DSTKServiceSender',
        			  		    data:  "serviceName=" + $scope.serviceName,
        			  		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        			  		}).
        			        success(function(data, status, headers, config) {
        			        	window.alert("Alert", "For service : " + $scope.serviceName + " sent operation completed to : 0532 349 58 05.");
        			        }).
        			        error(function(data, status, headers, config) {
        			        	window.alert("Alert", "Error, could not send campaign! status : " + status + ", reason " + data);
        			        });	
        					
        				}
        				
        			}]
        		});
        		
        	}
        	
        }).
        error(function(data, status, headers, config) {
        	window.alert("Alert", "Service : " + $scope.serviceName + " error status : " + status + " " + data);
        });
		
	}


}]);

mainControllers.controller('schedulerController', ['$scope', '$modal', '$http', function($scope, $modal, $http) {

	$scope.sendNow = function() {
		window.alert("Alert", "Sending...");
	}

	$scope.sendLater = function() {
		
		var ModalInstanceCtrl = function ($scope, $modalInstance) {
			  $scope.close = function () {
			    $modalInstance.dismiss('cancel');
			  };
		};
		
		var modalInstance = $modal.open({
			templateUrl: 'item-schedule.html',
			controller: ModalInstanceCtrl,
			windowClass : 'app-modal-window'
		});
		
	}
	
}]);

mainControllers.controller('campaignController', [ '$scope', '$timeout', '$location', '$filter', '$http', '$routeParams',
                                                 function($scope, $timeout, $location, $filter, $http, $routeParams) {

    $scope.sent = 1000;
    $scope.total = 1000;
    $scope.blackListed = 1000;
    $scope.accepted = 1000;

    var cancelTimeout = 0;

    var poll = function() {

        $scope.sent = parseInt($scope.sent) + 100;
        $scope.accepted = parseInt($scope.accepted) + 100;
        $scope.total = parseInt($scope.total) + 100;
        $scope.blackListed = parseInt($scope.blackListed) + 100;
        if (cancelTimeout == 1) {
            return $timeout(poll, 300);
        }
    };

    $scope.tabs = [ {
        "label" : "ALL",
        "url" : "/api/dstkcampaignstatistic/search?type=" + $routeParams.campaignType
    }, {
        "label" : "ACTIVE",
        "url" : "/api/dstkcampaignstatistic/search?statusName=ACTIVE&type=" + $routeParams.campaignType
    }, {
        "label" : "PLANNED",
        "url" : "/api/dstkcampaignstatistic/search?statusName=PLANNED&type=" + $routeParams.campaignType
    }, {
        "label" : "COMPLETED",
        "url" : "/api/dstkcampaignstatistic/search?statusName=COMPLETED&type=" + $routeParams.campaignType
    }];

    $scope.selectedTab = $scope.tabs[0];
    $scope.setSelectedTab = function(tab) {
        $scope.selectedTab = tab;
        $scope.url = tab.url;
    }

    $scope.tabClass = function(tab) {
        if ($scope.selectedTab == tab) {
            return "active";
        } else {
            return "";
        }
    }

    $scope.$watch("url", function() {
        $http.get($scope.url).success(function(data) {
            $scope.items = data;

            $scope.filteredItems = $scope.items;
            $scope.removeItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            $scope.query = "";

            $scope.groupToPages();
        });
    });

    // init
    $scope.sort = {
        sortingOrder : 'id',
        reverse : false
    };

    $scope.getData = function(items, query) {
        $scope.filteredItems = $filter('filter')(items,
            query);
        $scope.groupToPages();
    };

    // calculate page in
    // place
    $scope.groupToPages = function() {

        $scope.currentPage = 0;
        $scope.pagedItems = [];

        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i
                    / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i
                    / $scope.itemsPerPage)]
                    .push($scope.filteredItems[i]);
            }
        }
    };

    $scope.range = function(size) {

        var ret = [];

        for (var i = 0; i < size; i++) {
            ret.push(i);
        }

        return ret;
    };

    $scope.prevPage = function() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = function() {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function() {
        $scope.currentPage = this.n;
    };

    $scope.putToRemoveBag = function(item) {

        var i = $scope.removeItems.indexOf(item);
        if (i >= 0) {
            $scope.removeItems.splice(i, 1);
            item.checked = " ";
        } else {
            $scope.removeItems.push(item);
            item.checked = "checked";
        }

    };
    
//    $scope.checked = " ";
    
//    $scope.putPageToRemoveBag = function() {
//
//    	if ($scope.checked === " ") {
//    		$scope.checked = "checked";
//    	} else {
//    		$scope.checked = " ";
//    	}
//    	
//    	for (var i = 0; i < $scope.pagedItems[$scope.currentPage].length; i++) {
//    		$scope.putToRemoveBag(item);
//    	}
//    	
//    };

    $scope.deleteAll = function() {
        if (confirm("Are you sure?")) {
            for (var i = 0; i < $scope.removeItems.length; i++) {
                var j = $scope.filteredItems
                    .indexOf($scope.removeItems[i]);
                $scope.filteredItems.splice(j, 1);
            }
        }
        $scope.removeItems = [];
        $scope.groupToPages();
    };

    $scope.editItem = function(item) {

        console.log(item);


        if (item.statusName === "ACTIVE") {

            $scope.sent = item.sent;
            $scope.accepted = item.accepted;
            $scope.total = item.total;
            $scope.blackListed = item.blackListed;

            cancelTimeout = 1;

            $timeout(poll,300);

        } else {
            cancelTimeout = 0;

            $timeout(function() {
                $scope.sent = item.sent;
                $scope.accepted = item.accepted;
                $scope.total = item.total;
                $scope.blackListed = item.blackListed;
            }, 400);
        }

    }

    $scope.addItem = function() {

        $location.path($scope.path + 'add*?#');

    }

} ]);




   mainControllers.controller('executiveReportController',
   		[
   				'$scope',
   				'$timeout',
   				function($scope, $timeout) {

   					$scope.sims = [ {
   						"type" : "Native SIM",
   						"subscribers" : "1,023,534",
   						"status" : "+27",
   						"statusClass" : "growth-status-high"
   					}, {
   						"type" : "Standart Java SIM",
   						"subscribers" : "2,543,788",
   						"status" : "+486",
   						"statusClass" : "growth-status-average"
   					}, {
   						"type" : "M2M Java SIM",
   						"subscribers" : "321,734",
   						"status" : "0",
   						"statusClass" : "growth-status-low"
   					}, {
   						"type" : "NFC Java SIM",
   						"subscribers" : "16,365",
   						"status" : "-20",
   						"statusClass" : "growth-status-low"
   					} ];

   					$scope.myModelDonut = [ {
   						label : 'Native',
   						value : 26.2
   					}, {
   						label : 'Standart',
   						value : 65.1
   					}, {
   						label : 'M2M',
   						value : 8.2
   					}, {
   						label : 'NFC',
   						value : 0.4
   					} ];

   					$scope.flotData = {
   							
   							"data" : [
   									{
   										data : [[1, 50], [2, 40], [3, 45], [4, 23],[5, 55],[6, 65],[7, 61],[8, 70],[9, 65],[10, 75],[11, 57],[12, 59]],
   										label : 'Success Delivery'
   									},
   									{
   										data : [[1, 25], [2, 50], [3, 23], [4, 48],[5, 38],[6, 40],[7, 47],[8, 55],[9, 43],[10,50],[11,47],[12, 39]],
   										label : 'Total Messages'
   									} ],
   									
   							"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], 
   													 [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
   													 
   					}
   					
   					$scope.monthStatus = 'active';
   					
   					$scope.changeToDate = function(period){
   						$scope.dayStatus = '';
   						$scope.monthStatus = '';
   						$scope.yearStatus = '';
   						
   						if (period === 'day'){
   							
   							$scope.dayStatus = 'active';
   							
   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 2], [2, 2], [3, 1], [4, 3],[5, 0],[6, 2],[7, 3]],
   													label : 'Success Delivery'
   												},
   												{
   													data : [[1, 2], [2, 1], [3, 1], [4, 2],[5, 0],[6, 1],[7, 2]],
   													label : 'Total Messages'
   												} ],
   											
   									"ticks" : [[1, "04-04"], [2, "05-04"], [3, "06-04"], [4,"07-04"], [5,"08-04"], [6,"09-04"], [7,"10-04"]]
   															 
   							}
   							
   							
   						} else if (period === 'month'){

   							$scope.monthStatus = 'active';

   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 50], [2, 40], [3, 45], [4, 23],[5, 55],[6, 65],[7, 61],[8, 70],[9, 65],[10, 75],[11, 57],[12, 59]],
   													label : 'Success Delivery'
   												},
   												{
   													data : [[1, 25], [2, 50], [3, 23], [4, 48],[5, 38],[6, 40],[7, 47],[8, 55],[9, 43],[10,50],[11,47],[12, 39]],
   													label : 'Total Messages'
   												} ],
   											
   									"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], 
   												 [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
   															 
   							}
   											 
   							
   						} else {
   							
   							$scope.yearStatus = 'active';
   							
   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 600], [2, 480], [3, 540], [4, 276],[5, 660]],
   													label : 'Success Delivery'
   												},
   												{
   													data : [[1, 450], [2, 360], [3, 405], [4, 207],[5, 495]],
   													label : 'Total Messages'
   												} ],
   											
   									"ticks" :  [[1, "2009"], [2, "2010"], [3, "2011"], [4,"2012"], [5,"2013"]]
   							}
   							
   						}
   					
   					}
   					
   					
   					$scope.taxData = [
   									{"period": "2014-04-04", "sent": 873465, "delivered": 655098},
   									{"period": "2014-04-05", "sent": 765609, "delivered": 574206},
   									{"period": "2014-04-06", "sent": 981276, "delivered": 735957},
   									{"period": "2014-04-07", "sent": 734651, "delivered": 550998},
   									{"period": "2014-04-08", "sent": 1209348, "delivered": 907011},
   									{"period": "2014-04-09", "sent": 1376451, "delivered": 1032338},
   									{"period": "2014-04-10", "sent": 510928, "delivered": 383196}
   					];					

   					$scope.xkeyLine = 'period';
   					$scope.ykeysLine = [ 'sent', 'delivered' ];
   					$scope.labelsLine = [ 'Total Messages', 'Success Delivery' ];

   					$scope.tabs = [ {
   						"label" : "SIM Card Usage Reports"
   					}, {
   						"label" : "System Traffic Reports"
   					}, {
   						"label" : "Campaign Service Reports"
   					} ];

   					$scope.selectedTab = $scope.tabs[0];
   					$scope.setSelectedTab = function(tab) {
   						$scope.selectedTab = tab;
   					}

   					$scope.tabClass = function(tab) {
   						if ($scope.selectedTab == tab) {
   							return "active";
   						} else {
   							return "";
   						}
   					}

   				} ]);

   mainControllers
   		.controller(
   				'tableController',
   				[
   						'$scope',
   						'$location',
   						'$filter',
   						'$http',
   						function($scope, $location, $filter, $http) {

   							$scope.$watch("url", function() {
   								$http.get($scope.url).success(function(data) {
   									$scope.items = data;

   									$scope.filteredItems = $scope.items;
   									$scope.removeItems = [];
   									$scope.itemsPerPage = 10;
   									$scope.pagedItems = [];
   									$scope.currentPage = 0;
   									$scope.query = "";

   									$scope.groupToPages();
   								});
   							});

   							// init
   							$scope.sort = {
   								sortingOrder : 'id',
   								reverse : false
   							};

   							$scope.getData = function(items, query) {
   								$scope.filteredItems = $filter('filter')(items,
   										query);
   								$scope.groupToPages();
   							};

   							// calculate
							// page in
							// place
   							$scope.groupToPages = function() {

   								$scope.currentPage = 0;
   								$scope.pagedItems = [];

   								for (var i = 0; i < $scope.filteredItems.length; i++) {
   									if (i % $scope.itemsPerPage === 0) {
   										$scope.pagedItems[Math.floor(i
   												/ $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
   									} else {
   										$scope.pagedItems[Math.floor(i
   												/ $scope.itemsPerPage)]
   												.push($scope.filteredItems[i]);
   									}
   								}
   							};

   							$scope.range = function(size) {

   								var ret = [];

   								for (var i = 0; i < size; i++) {
   									ret.push(i);
   								}

   								return ret;
   							};

   							$scope.prevPage = function() {
   								if ($scope.currentPage > 0) {
   									$scope.currentPage--;
   								}
   							};

   							$scope.nextPage = function() {
   								if ($scope.currentPage < $scope.pagedItems.length - 1) {
   									$scope.currentPage++;
   								}
   							};

   							$scope.setPage = function() {
   								$scope.currentPage = this.n;
   							};

   							$scope.putToRemoveBag = function(item) {

   								var i = $scope.removeItems.indexOf(item);
   								if (i >= 0) {
   									$scope.removeItems.splice(i, 1);
   									item.checked = " ";
   								} else {
   									$scope.removeItems.push(item);
   									item.checked = "checked";
   								}

   							};

   							$scope.deleteAll = function() {
   								if (confirm("Are you sure?")) {
   									
   									var ids = ""; 
   									
   									for (var i = 0; i < $scope.removeItems.length; i++) {
   										var j = $scope.filteredItems.indexOf($scope.removeItems[i]);
   										$scope.filteredItems.splice(j, 1);
   										ids = ids + "ids=" + $scope.removeItems[i].id + "&";  
   									}
   									console.log(ids);
   									
   									var url = $scope.url  + "?" + ids;
   									console.log("url:" + url);
   									
   									// Delete
   									if (ids){
   										$http.delete(url).success(function(data) {
   											$scope.item = data;
   										});										
   									}
   									
   									
   								}
   								$scope.removeItems = [];
   								$scope.groupToPages();
   							};

   							$scope.editItem = function(item) {

   								console.log(item);
   								
   								$location.path($scope.path + item.id);

   							}

   							$scope.addItem = function() {
   								
   								$location.path($scope.path + 'add*?#');

   							}

   						} ]);


   mainControllers.controller('designsTableController', [ '$scope', '$timeout', '$location', '$filter', '$http', '$routeParams',
                                                      function($scope, $timeout, $location, $filter, $http, $routeParams) {

                                                          $scope.tabs = [ {
                                                              "label" : "SERVICES",
                                                              "headers" : ["SERVICE TITLE","SERVICE TYPE","MODIFICATION DATE"],
                                                              "url" : "/api/dstkcampaignstatistic/search?type=" + $routeParams.campaignType
                                                          }, {
                                                              "label" : "MENUS",
                                                              "headers" : ["MENU TITLE","VERSION","MODIFICATION DATE"],
                                                              "url" : "/api/dstkcampaignstatistic/search?statusName=ACTIVE&type=" + $routeParams.campaignType
                                                          }, {
                                                              "label" : "MEIN MENUS",
                                                              "headers" : ["MENU TITLE","VERSION","MODIFICATION DATE"],
                                                              "url" : "/api/dstkcampaignstatistic/search?statusName=PLANNED&type=" + $routeParams.campaignType
                                                          }];

                                                          $scope.selectedTab = $scope.tabs[0];
                                                          $scope.setSelectedTab = function(tab) {
                                                              $scope.selectedTab = tab;
                                                              $scope.url = tab.url;
                                                          }

                                                          $scope.tabClass = function(tab) {
                                                              if ($scope.selectedTab == tab) {
                                                                  return "active";
                                                              } else {
                                                                  return "";
                                                              }
                                                          }

                                                          $scope.$watch("url", function() {
                                                              $http.get($scope.url).success(function(data) {
                                                                  $scope.items = data;

                                                                  $scope.filteredItems = $scope.items;
                                                                  $scope.removeItems = [];
                                                                  $scope.itemsPerPage = 10;
                                                                  $scope.pagedItems = [];
                                                                  $scope.currentPage = 0;
                                                                  $scope.query = "";

                                                                  $scope.groupToPages();
                                                              });
                                                          });

                                                          // init
                                                          $scope.sort = {
                                                              sortingOrder : 'id',
                                                              reverse : false
                                                          };

                                                          $scope.getData = function(items, query) {
                                                              $scope.filteredItems = $filter('filter')(items,
                                                                  query);
                                                              $scope.groupToPages();
                                                          };

                                                          // calculate page in
                                                          // place
                                                          $scope.groupToPages = function() {

                                                              $scope.currentPage = 0;
                                                              $scope.pagedItems = [];

                                                              for (var i = 0; i < $scope.filteredItems.length; i++) {
                                                                  if (i % $scope.itemsPerPage === 0) {
                                                                      $scope.pagedItems[Math.floor(i
                                                                          / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                                                                  } else {
                                                                      $scope.pagedItems[Math.floor(i
                                                                          / $scope.itemsPerPage)]
                                                                          .push($scope.filteredItems[i]);
                                                                  }
                                                              }
                                                          };

                                                          $scope.range = function(size) {

                                                              var ret = [];

                                                              for (var i = 0; i < size; i++) {
                                                                  ret.push(i);
                                                              }

                                                              return ret;
                                                          };

                                                          $scope.prevPage = function() {
                                                              if ($scope.currentPage > 0) {
                                                                  $scope.currentPage--;
                                                              }
                                                          };

                                                          $scope.nextPage = function() {
                                                              if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                                                  $scope.currentPage++;
                                                              }
                                                          };

                                                          $scope.setPage = function() {
                                                              $scope.currentPage = this.n;
                                                          };

                                                          $scope.putToRemoveBag = function(item) {

                                                              var i = $scope.removeItems.indexOf(item);
                                                              if (i >= 0) {
                                                                  $scope.removeItems.splice(i, 1);
                                                                  item.checked = " ";
                                                              } else {
                                                                  $scope.removeItems.push(item);
                                                                  item.checked = "checked";
                                                              }

                                                          };

                                                          $scope.deleteAll = function() {
                                                              if (confirm("Are you sure?")) {
                                                                  for (var i = 0; i < $scope.removeItems.length; i++) {
                                                                      var j = $scope.filteredItems
                                                                          .indexOf($scope.removeItems[i]);
                                                                      $scope.filteredItems.splice(j, 1);
                                                                  }
                                                              }
                                                              $scope.removeItems = [];
                                                              $scope.groupToPages();
                                                          };

                                                          $scope.editItem = function(item) {

                                                              console.log(item);


                                                              if (item.statusName === "ACTIVE") {

                                                                  $scope.sent = item.sent;
                                                                  $scope.accepted = item.accepted;
                                                                  $scope.total = item.total;
                                                                  $scope.blackListed = item.blackListed;

                                                                  cancelTimeout = 1;

                                                                  $timeout(poll,300);

                                                              } else {
                                                                  cancelTimeout = 0;

                                                                  $timeout(function() {
                                                                      $scope.sent = item.sent;
                                                                      $scope.accepted = item.accepted;
                                                                      $scope.total = item.total;
                                                                      $scope.blackListed = item.blackListed;
                                                                  }, 400);
                                                              }

                                                          }

                                                          $scope.addItem = function() {

                                                              $location.path($scope.path + 'add*?#');

                                                          }

                                                      } ]);
   
   
   mainControllers.controller('detailController', [ '$scope', '$http', '$routeParams',
    		'$location', function($scope, $http, $routeParams, $location) {

    			console.log("detailController is called.");
    	
    			if ($routeParams.itemId === "add*?#") {
    				$scope.screen = {
    					"operation" : "add",
    					"header" : "Add",
    					"button" : "Add"
    				};
    				$scope.item = {};
    			} else {
    				$scope.screen = {
    					"operation" : "edit",
    					"header" : "Edit",
    					"button" : "Update"
    				};

    				var url = "/api/transportkey/" + $routeParams.itemId;
    				console.log("Url:" + url);
    				
    				$http.get(url).success(function(data) {
    					$scope.item = data;
    				});
    				
    			}
    			
    			$scope.add = function() {

    				var json = JSON.stringify($scope.item);
    				console.log("json:" + json);

    				var url = "/api/otatransportkey/";
    				console.log("url:" + url);
    				
    				// Save
    				$http.post(url, json).success(function(data) {
    					$scope.item = data;
    				});
    				
    				$location.path($scope.path);
    				
    			}

    			$scope.edit = function() {

    				var json = JSON.stringify($scope.item);
    				console.log("json:" + json);

    				var url = "/api/otatransportkey/";
    				url = url + $scope.item.id;
    				console.log("url:" + url);
    				
    				// Update
    				$http.put(url, json).success(function(data) {
    					
    					console.log("success put");
    					$scope.item = data;
    					
    					console.log("$scope.path:" + $scope.path)
    					$location.path($scope.path);
    				})
    		        .error(function(data, status, headers, config) {
    		        	
    					console.log("error put");
    					console.log("headers : " + headers + " error status : " + status + " data: " + data + " config:" + config);
    		        	// alert("headers : " + headers + " error status : " +
						// status + " data: " + data + " config:" + config);
// alert("errors : " + data.errors);
    					window.alert("Alert", "errors : " + data.errors);
    					
    		        });
    				
    			}

    			$scope.cancel = function() {
    				$location.path($scope.path);
    			}

    		} ]);   