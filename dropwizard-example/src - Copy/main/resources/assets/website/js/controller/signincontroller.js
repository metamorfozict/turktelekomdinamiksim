'use strict';

/* App Module */
var signinApp = angular.module('signinApp', []);

signinApp.controller('signinController', ['$scope', '$window',
   function ($scope, $window) {
	
	   $scope.reset = function() {
	    $timeout(jQuery.uniform.update, 0);
	   };	
	
	   $scope.signin = {"username" : "", "password" : ""}; 
	             
	   $scope.checkCredentials = function() {
		   
		   if ($scope.signin.username === "admin" && $scope.signin.password === "metamorfoz") {
			   $window.location.href = 'main.html';	
		   }
		   
	   }
	   
   }]);




