'use strict';

/* App Module */

var tabApp = angular.module('tabApp', ['ngRoute',
                                        'tabControllers'
                                      ]);

tabApp.config(['$routeProvider',
                    function($routeProvider) {
                      $routeProvider.
                        when('/jobs', {
                          templateUrl: 'jobs-partial.html',
                          controller: 'JobsCtrl'
                        }).
                        when('/invoices', {
                          templateUrl: 'invoices-partial.html',
                          controller: 'InvoicesCtrl'
                        }).
                        when('/payments', {
                            templateUrl: 'payments-partial.html',
                            controller: 'PaymentsCtrl'
                         }).
                        otherwise({
                          redirectTo: '/jobs'
                        });
                    }]);

tabApp.controller('TabsCtrl', ['$scope',
   function ($scope) {
	
		$scope.tabs = [
		               { "link" : "#/jobs", "label" : "Jobs" },
		               { "link" : "#/invoices", "label" : "Invoices" },
		               { "link" : "#/payments", "label" : "Payments" }
	               	  ]; 
	             
	   $scope.selectedTab = $scope.tabs[0];    
	   $scope.setSelectedTab = function(tab) {
	     $scope.selectedTab = tab;
	   }
	   
	   $scope.tabClass = function(tab) {
	     if ($scope.selectedTab == tab) {
	       return "active";
	     } else {
	       return "";
	     }
	   }
	
   }]);

var tabControllers = angular.module('tabControllers', []);

tabControllers.controller('JobsCtrl', ['$scope', '$http',
    function ($scope, $http) {
	
    }]);

tabControllers.controller('InvoicesCtrl', ['$scope', '$http',
    function($scope, $http) {

    }]);

tabControllers.controller('PaymentsCtrl', ['$scope', '$http',
    function ($scope, $http) {
	
	}]);
