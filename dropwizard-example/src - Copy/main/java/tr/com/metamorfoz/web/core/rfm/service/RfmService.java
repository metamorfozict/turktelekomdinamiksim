package tr.com.metamorfoz.web.core.rfm.service;

import tr.com.metamorfoz.web.core.rfm.service.rfmdomainservice.RfmGsmOperatorDomainService;

public class RfmService {

	private RfmGsmOperatorDomainService gsmOperatorDomainService;

	public RfmService(RfmGsmOperatorDomainService gsmOperatorDomainService) {
		super();
		this.setGsmOperatorDomainService(gsmOperatorDomainService);
	}

	public RfmGsmOperatorDomainService getGsmOperatorDomainService() {
		return gsmOperatorDomainService;
	}

	public void setGsmOperatorDomainService(RfmGsmOperatorDomainService gsmOperatorDomainService) {
		this.gsmOperatorDomainService = gsmOperatorDomainService;
	}
	
	
	

	
	
}
