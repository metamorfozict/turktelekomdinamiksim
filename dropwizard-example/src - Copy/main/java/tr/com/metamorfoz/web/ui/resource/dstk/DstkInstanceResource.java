package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkInstanceApiDtoService;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceDto;
import tr.com.metamorfoz.web.shared.model.dstk.request.DstkInstanceDtoSaveRequest;

@Path("/api/dstkinstance")
@Produces(MediaType.APPLICATION_JSON)
public class DstkInstanceResource {

	private DstkInstanceApiDtoService dstkInstanceApiDtoService;
	
	public DstkInstanceResource(DstkInstanceApiDtoService dstkInstanceApiDtoService) {
		super();
		this.dstkInstanceApiDtoService = dstkInstanceApiDtoService;
	}

	@GET
	@UnitOfWork
	public List<DstkInstanceDto> findAll(){
		return dstkInstanceApiDtoService.findAll();
	}
		
//	@POST
//	@UnitOfWork
//	public DstkInstanceDto create(DstkInstanceDto dstkInstanceDto) {
//		return dstkInstanceApiDtoService.create(dstkInstanceDto);
//	}

	@POST
	@UnitOfWork
	public DstkInstanceDto create(@Valid DstkInstanceDtoSaveRequest request) {
		return dstkInstanceApiDtoService.createDstkInstanceDto(request, "A");
	}

	@PUT
	@UnitOfWork
	public DstkInstanceDto update(@Valid DstkInstanceDtoSaveRequest request) {
		return dstkInstanceApiDtoService.createDstkInstanceDto(request, "E");
	}

	//TODO:Search yapılacak. Parametreler: 
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public DstkInstanceDto find(@PathParam("id") int id) {
		return dstkInstanceApiDtoService.findSafely(id);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public DstkInstanceDto update(@PathParam("id") int id, DstkInstanceDto dstkInstanceDto) {
		return dstkInstanceApiDtoService.update(id, dstkInstanceDto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		dstkInstanceApiDtoService.deleteAll(ids);
	}
	
}
