package tr.com.metamorfoz.web.core.dstk.api;

import tr.com.metamorfoz.web.core.common.api.AbstractApiDtoCrudService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkCampaignStatisticConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkCampaignStatisticDomainService;
import tr.com.metamorfoz.web.shared.model.dstk.DstkCampaignStatisticDto;

public class DstkCampaignStatisticApiDtoService extends AbstractApiDtoCrudService<DstkCampaignStatistic, DstkCampaignStatisticDto, DstkCampaignStatisticDomainService, DstkCampaignStatisticConverter> {

	public DstkCampaignStatisticApiDtoService(DstkCampaignStatisticDomainService dstkCampaignStatisticDomainService, DstkCampaignStatisticConverter dstkCampaignStatisticConverter) {
		super(dstkCampaignStatisticDomainService, dstkCampaignStatisticConverter);
	}

}
