package tr.com.metamorfoz.web.core.rfm.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

@Entity
@Table(name="TBL_OTASS_GSM_OPERATORS")
public class RfmGsmOperator extends BaseEntity {

	@NotBlank
	@Length(max = 128)
	@Column(name="T_CODE", length=128)
	private String code;
	
	@Length(max = 128)
	@Column(name="T_NAME", length=128)
	private String name;

	@Length(max = 128)
	@Column(name="T_DESCRIPTION", length=128)
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
