package tr.com.metamorfoz.web.core.ota.constants;

public class OtaCampaignConstants {
	
	public static final int    DSTK_MAX_CNTNT_LEN_MT = 106;
    public static final int    DSTK_MAX_CNTNT_LEN_MO = 121;
   
    public static final String DSTK_MOBILE_ERR_DISP  = "Komut Desteklenmiyor";
   
    public static final String DSTK_COMMAND_SEPARATOR = ";";

}
