package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModuleDao;

public class DstkModuleDomainService extends AbstractDomainCrudService<DstkModule, DstkModuleDao> {

	public DstkModuleDomainService(DstkModuleDao dao) {
		super(dao);
	}

	public DstkModule findUniqueDstkModuleByName(String moduleName) {
		return getDao().findUniqueDstkModuleByName(moduleName);
	}
}
