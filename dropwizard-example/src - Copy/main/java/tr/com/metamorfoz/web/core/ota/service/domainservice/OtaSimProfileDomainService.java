package tr.com.metamorfoz.web.core.ota.service.domainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.ota.dao.OtaSimProfileDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaSimProfile;

public class OtaSimProfileDomainService extends AbstractDomainCrudService<OtaSimProfile, OtaSimProfileDao> {

	public OtaSimProfileDomainService(OtaSimProfileDao otaSimProfileDao) {
		super(otaSimProfileDao);
	}

}
