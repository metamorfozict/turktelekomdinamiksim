package tr.com.metamorfoz.web.core.ota.service;

import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkInstanceDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkModuleDomainService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaSimProfileDomainService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaTransportKeyDomainService;

public class OtaService  {

	private DstkInstanceDomainService dstkInstanceDomainService;
	private DstkModuleDomainService dstkModuleDomainService;
	private OtaSimProfileDomainService simProfileDomainService;
	private OtaTransportKeyDomainService transportKeyDomainService;
	
	public OtaService(DstkInstanceDomainService dstkInstanceDomainService,
			DstkModuleDomainService dstkModuleDomainService,
			OtaSimProfileDomainService simProfileDomainService,
			OtaTransportKeyDomainService transportKeyDomainService) {
		super();
		this.dstkInstanceDomainService = dstkInstanceDomainService;
		this.dstkModuleDomainService = dstkModuleDomainService;
		this.simProfileDomainService = simProfileDomainService;
		this.transportKeyDomainService = transportKeyDomainService;
	}
	
	public DstkInstanceDomainService getDstkInstanceDomainService() {
		return dstkInstanceDomainService;
	}

	public void setDstkInstanceDomainService(
			DstkInstanceDomainService dstkInstanceDomainService) {
		this.dstkInstanceDomainService = dstkInstanceDomainService;
	}

	public DstkModuleDomainService getDstkModuleDomainService() {
		return dstkModuleDomainService;
	}

	public void setDstkModuleDomainService(
			DstkModuleDomainService dstkModuleDomainService) {
		this.dstkModuleDomainService = dstkModuleDomainService;
	}

	public OtaSimProfileDomainService getSimProfileDomainService() {
		return simProfileDomainService;
	}

	public void setSimProfileDomainService(
			OtaSimProfileDomainService simProfileDomainService) {
		this.simProfileDomainService = simProfileDomainService;
	}

	public OtaTransportKeyDomainService getTransportKeyDomainService() {
		return transportKeyDomainService;
	}

	public void setTransportKeyDomainService(
			OtaTransportKeyDomainService transportKeyDomainService) {
		this.transportKeyDomainService = transportKeyDomainService;
	}

	
	
	
	

	
}
