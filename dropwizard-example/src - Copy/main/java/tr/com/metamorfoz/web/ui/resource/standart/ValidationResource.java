package tr.com.metamorfoz.web.ui.resource.standart;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tr.com.metamorfoz.web.shared.model.common.TestRequest;

@Path("api/validation")
public class ValidationResource {

	@POST
	@Path("hello") 
	public String hello(@NotNull @QueryParam("name") String name, 
			 			@NotNull @QueryParam("surname") String surname) {
		return name + " " + surname;
	}
	
	@POST
	@Path("hello2") 
	@NotNull
	public String hello2(@Valid TestRequest testRequest) {
		return testRequest.toString();
	}
	
	@POST
	@Path("hello3") 
	public Response hello3() {
	
		TestRequest testRequest = new TestRequest();
		testRequest.setName("test");
				
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "User Name exists. Try another user name");
		map.put("testRequest", testRequest);
		return Response.status(Status.BAD_REQUEST).entity(map).build();

	}
	
	
}
	
	
