package tr.com.metamorfoz.web.shared.model.ota;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class OtaTransportKeyDto  {

	private Integer id;
	
	@NotBlank
	@Length(max = 5)
	private String no;

	@NotBlank
	@Length(max = 50)
	private String value;
	
	@NotBlank
	@Length(max = 10)
	private String algorithmName;	
	
	@NotBlank
	@Length(max = 10)
	private String algorithmMode;

	public OtaTransportKeyDto() {
	}
	
	public OtaTransportKeyDto(Integer id, String no, String value,
			String algorithmName, String algorithmMode) {
		super();
		this.id = id;
		this.no = no;
		this.value = value;
		this.algorithmName = algorithmName;
		this.algorithmMode = algorithmMode;
	}



	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getAlgorithmMode() {
		return algorithmMode;
	}

	public void setAlgorithmMode(String algorithmMode) {
		this.algorithmMode = algorithmMode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	
	
}