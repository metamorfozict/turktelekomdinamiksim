package tr.com.metamorfoz.web.core.dstk.domain;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;

public class DstkModuleDao extends AbstractDao<DstkModule> {

	public DstkModuleDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public DstkModule findUniqueDstkModuleByName(String moduleName) {
		//Assumption:Veritabanında sadece 1 kayıt olduğu kabul edilmiştir.
		DstkModule dstkModule = (DstkModule) currentSession().createCriteria(DstkModule.class)
													.add(Restrictions.eq("moduleName", moduleName))
													.uniqueResult();
		return dstkModule;
	}
}

