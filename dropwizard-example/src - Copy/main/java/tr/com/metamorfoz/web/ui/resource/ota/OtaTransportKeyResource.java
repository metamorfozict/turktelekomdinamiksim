package tr.com.metamorfoz.web.ui.resource.ota;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.ota.api.OtaApiService;
import tr.com.metamorfoz.web.shared.model.ota.OtaTransportKeyDto;

@Path("/api/otatransportkey")
@Produces(MediaType.APPLICATION_JSON)
public class OtaTransportKeyResource {

	private OtaApiService otaApiService;
	
	public OtaTransportKeyResource(OtaApiService otaApiService) {
		super();
		this.otaApiService = otaApiService;
	}
	
	@GET
	@UnitOfWork
	public List<OtaTransportKeyDto> findAll(){
		return otaApiService.findAllTransportKeyDto();
	}
	
	@GET
	@Path("/search")
	@UnitOfWork
	public List<OtaTransportKeyDto> search(@QueryParam("status") String status){
		return otaApiService.searchTransportKeyDto(status);
	}
	
	@POST
	@UnitOfWork
	public OtaTransportKeyDto create(OtaTransportKeyDto transportKeyDto) {
		return otaApiService.createTransportKeyDto(transportKeyDto);
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public OtaTransportKeyDto find(@PathParam("id") int id) {
		return otaApiService.findTransportKeyDtoSafely(id);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public OtaTransportKeyDto update(@PathParam("id") int id, OtaTransportKeyDto transportKeyDto) {
		return otaApiService.updateTransportKeyDto(id, transportKeyDto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		otaApiService.deleteAllTransportKeyDto(ids);
	}
	
}
