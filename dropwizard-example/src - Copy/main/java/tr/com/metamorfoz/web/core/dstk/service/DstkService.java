package tr.com.metamorfoz.web.core.dstk.service;

import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkCampaignStatisticDomainService;

public class DstkService {
	
	private DstkCampaignStatisticDomainService campaignStatisticDomainService;

	public DstkService(DstkCampaignStatisticDomainService campaignStatisticDomainService) {
		super();
		this.setCampaignStatisticDomainService(campaignStatisticDomainService);
	}

	public DstkCampaignStatisticDomainService getCampaignStatisticDomainService() {
		return campaignStatisticDomainService;
	}

	public void setCampaignStatisticDomainService(
			DstkCampaignStatisticDomainService campaignStatisticDomainService) {
		this.campaignStatisticDomainService = campaignStatisticDomainService;
	}

	

}
