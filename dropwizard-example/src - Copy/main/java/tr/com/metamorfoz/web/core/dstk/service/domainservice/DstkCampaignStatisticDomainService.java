package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.dao.DstkCampaignStatisticDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;

public class DstkCampaignStatisticDomainService extends AbstractDomainCrudService<DstkCampaignStatistic, DstkCampaignStatisticDao> {

	public DstkCampaignStatisticDomainService(DstkCampaignStatisticDao dstkCampaignStatisticDao) {
		super(dstkCampaignStatisticDao);
	};


}
