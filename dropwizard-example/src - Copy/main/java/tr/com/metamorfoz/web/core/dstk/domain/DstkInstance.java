package tr.com.metamorfoz.web.core.dstk.domain;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.utility.ObjectUtil;
import tr.com.metamorfoz.web.utility.StringUtil;

@Entity
@Table(name="TBL_OTASS_DSTK_INSTANCES")
public class DstkInstance extends BaseEntity {

	private static final Integer DEFAULT_VALUE_FOR_INSTANCE = 1;
	private static final String DEFAULT_VALUE_FOR_APP_ID = "A00000000000000000000012131415";
	private static final String DEFAULT_VALUE_FOR_INTR_BLOCK = "8B819541000481724040F48C02700000870D00000000200101000000000000";
	private static final String DEFAULT_VALUE_FOR_PROFILE = "09C48C081A15015232340A05";
	private static final String DEFAULT_VALUE_FOR_TAR = "200101";
	  
	@Length(max = 100)
	@Column(name="T_PK_INSTANCE_HEADER", length=100)
	private String instanceHeader;
	
	@Column(name="N_PK_INSTANCE")   
	private Integer instance;
	
	@Column(name="T_CONTENT")
	private Blob content;
	
	@Column(name="T_JSON")
	private Blob json;
	
	@NotBlank
	@Length(max = 32)
	@Column(name="T_AID", length=32)
	private String appId;
	
	@Length(max = 250)
	@Column(name="T_INTR_BLOCK", length=250)
	private String intrBlock;
	
	@Length(max = 200)
	@Column(name="T_PROFILE", length=200)
	private String profile;
	
	@Length(max = 10)
	@Column(name="T_TAR", length=10)
	private String tar;
	
	@Column(name="N_PK_ORDER")
	private Integer instanceVersion;

	public DstkInstance() {
		super();
	}

//	public DstkInstance(String instanceHeader, Integer instance, String appId, String intrBlock, String profile,
//			String tar, Integer instanceVersion) {
//		super();
//		this.instanceHeader = instanceHeader;
//		this.instance = instance;
//		this.setAppId(appId);
//		this.intrBlock = intrBlock;
//		this.profile = profile;
//		this.tar = tar;
//		this.setInstanceVersion(instanceVersion);
//	}

	public String getInstanceHeader() {
		return instanceHeader;
	}

	public void setInstanceHeader(String instanceHeader) {
		this.instanceHeader = instanceHeader;
	}

	public Integer getInstance() {
		return (Integer) ObjectUtil.defaultIfNull(instance, DEFAULT_VALUE_FOR_INSTANCE);
	}

	public void setInstance(Integer instance) {
		this.instance = instance;
	}

	public Blob getContent() {
		return content;
	}

	public void setContent(Blob content) {
		this.content = content;
	}

	public Blob getJson() {
		return json;
	}

	public void setJson(Blob json) {
		this.json = json;
	}


	public String getIntrBlock() {
		return StringUtil.defaultIfBlank(intrBlock, DEFAULT_VALUE_FOR_INTR_BLOCK);
	}

	public void setIntrBlock(String intrBlock) {
		this.intrBlock = intrBlock;
	}

	public String getProfile() {
		return StringUtil.defaultIfBlank(profile, DEFAULT_VALUE_FOR_PROFILE);
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getTar() {
		return StringUtil.defaultIfBlank(tar, DEFAULT_VALUE_FOR_TAR);
	}

	public void setTar(String tar) {
		this.tar = tar;
	}

	public String getAppId() {
		 return StringUtil.defaultIfBlank(appId, DEFAULT_VALUE_FOR_APP_ID);
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getInstanceVersion() {
		return instanceVersion;
	}

	public void setInstanceVersion(Integer instanceVersion) {
		this.instanceVersion = instanceVersion;
	}


	
}

