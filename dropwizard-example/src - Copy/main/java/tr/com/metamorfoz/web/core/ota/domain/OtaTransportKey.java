package tr.com.metamorfoz.web.core.ota.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

@Entity
@Table(name="TBL_OTASS_TRANSPORT_KEYS")
public class OtaTransportKey extends BaseEntity {

	@NotBlank
	@Length(max = 5)	
	@Column(name="T_PK_TRANSPORT_KEY_NO", length=5)
	private String no;

	@NotBlank
	@Length(max = 50)
	@Column(name="T_TRANSPORT_KEY", length=50)
	private String value;
	
	@NotBlank
	@Length(max = 10)	
	@Column(name="T_ALGO_NAME", length=10)
	private String algorithmName;	
	
	@NotBlank
	@Length(max = 10)
	@Column(name="T_ALGO_MODE", length=10)
	private String algorithmMode;

	public OtaTransportKey() {
	}
	
	public void update(String no, String value, String algorithmName, String algorithmMode) {
		this.no = no;
		this.value = value;
		this.algorithmName = algorithmName;
		this.algorithmMode = algorithmMode;
	}
	
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getAlgorithmMode() {
		return algorithmMode;
	}

	public void setAlgorithmMode(String algorithmMode) {
		this.algorithmMode = algorithmMode;
	}
	
	
	
}
