package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkCampaignStatisticApiDtoService;
import tr.com.metamorfoz.web.shared.model.dstk.DstkCampaignStatisticDto;

@Path("/api/dstkcampaignstatistic")
@Produces(MediaType.APPLICATION_JSON)
public class DstkCampaignStatisticResource {

	private DstkCampaignStatisticApiDtoService apiDtoService;

	public DstkCampaignStatisticResource(DstkCampaignStatisticApiDtoService dstkCampaignStatisticApiDtoService) {
		super();
		this.apiDtoService = dstkCampaignStatisticApiDtoService;
	}

	@GET
	@UnitOfWork
	public List<DstkCampaignStatisticDto> findAll(){
		return apiDtoService.findAll();
	}
	
	@POST
	@UnitOfWork
	public DstkCampaignStatisticDto create(DstkCampaignStatisticDto dto) {
		return apiDtoService.create(dto);
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public DstkCampaignStatisticDto find(@PathParam("id") int id) {
		return apiDtoService.findSafely(id);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public DstkCampaignStatisticDto update(@PathParam("id") int id, DstkCampaignStatisticDto dto) {
		return apiDtoService.update(id, dto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		apiDtoService.deleteAll(ids);
	}
	
}
