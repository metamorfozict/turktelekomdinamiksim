package tr.com.metamorfoz.web.core.ota.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;

public class OtaTransportKeyDao extends AbstractDao<OtaTransportKey> {

	public OtaTransportKeyDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public List<OtaTransportKey> search(String status) {
		
		@SuppressWarnings("unchecked")
		List<OtaTransportKey> list = currentSession().createCriteria(OtaTransportKey.class)
													.add(Restrictions.eq("status", status))
													.list();
		
		return list;
	}

}
