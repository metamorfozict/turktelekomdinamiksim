package tr.com.metamorfoz.web.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


//TODO:Utility sınıfları Common projesine atılacak.
public class CommonUtil {
	
	public static final String[] PSEUDO = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
	
	/**
	 * This function converts the long to byte array. 
	 * 
	 * @param 					value						The long value to be converted.
	 * @return					byte[]						The byte array representation.
	 */
	public static final byte[] convertLongToByteArray(long value) {
		return new byte[]{
				(byte)(value >>> 56),
				(byte)(value >> 48 & 0xff),
				(byte)(value >> 40 & 0xff),
				(byte)(value >> 32 & 0xff),
				(byte)(value >> 24 & 0xff),
				(byte)(value >> 16 & 0xff), 
				(byte)(value >> 8 & 0xff), 
				(byte)(value & 0xff) };
	}
	
	/**
	 * This function converts the integer to byte array. 
	 * 
	 * @param 					value						The int value to be converted.
	 * @return					byte[]						The byte array representation.
	 */
	public static byte[] convertIntToByteArray(int value) {
		
		return new byte[] {
				
				(byte) ((value >>> 24) & 0xff),
				(byte) ((value >>> 16) & 0xff),
				(byte) ((value >>> 8) & 0xff),
				(byte) ((value >>> 0) & 0xff)
		};
		
	}
	
	/**
	 * This function converts the short to byte array. 
	 * 
	 * @param 					value						The short value to be converted.
	 * @return					byte[]						The byte array representation.
	 */
	public static byte[] convertShortToByteArray(short value) {
		
		return new byte[] {
				(byte) ((value >>> 8) & 0xff),
				(byte) ((value >>> 0) & 0xff)
		};
		
	}
	
	/**
	* Convert a hex string to a byte array. Permits upper or lower case hex.
	*
	* @param 					value 					String must have even number of characters, and be formed only of digits 0-9 A-F or
	* 													a-f. No spaces, minus or plus signs.
	* @return 					byte[]					corresponding byte array.
	*/
	public static byte[] hexStringToByteArray(String value) {
		
		if (value == null) {
			throw new NullPointerException ("hexStringToByteArray requires not null value");
		}
		
		int stringLength = value.length();
		
		if ((stringLength & 0x1) != 0) {
			throw new IllegalArgumentException ("hexStringToByteArray requires an even number of hex characters");
		}
		
		byte[] result = new byte[stringLength / 2];

		for (int i = 0, j = 0; i < stringLength; i += 2, j++) {
			int high = charToNibble(value.charAt(i));
			int low  = charToNibble(value.charAt(i+1));
			result[j] = (byte)(( high << 4 ) | low);
		}
		
		return result;
	}

	/**
	* convert a single char to corresponding nibble.
	*
	* @param 					c 					char to convert. must be 0-9 a-f A-F, no
	* 												spaces, plus or minus signs.
	* @return 					int					corresponding integer
	*/
	private static int charToNibble (char c) {
		
		if ('0' <= c && c <= '9') {
			return c - '0';
		} else if ('a' <= c && c <= 'f') {
			return c - 'a' + 0xa;
		} else if ('A' <= c && c <= 'F') {
			return c - 'A' + 0xa;
		} else {
			throw new IllegalArgumentException ("Invalid hex character: " + c);
		}
		
	}
	
	/**
	 * This function converts an object to raw byte array.
	 * 
	 * @param 					object					the object.
	 * @return					byte[]					the byte array representation of the object.
	 * @throws 					IOException
	 */
	public static byte[] composeBytes(Object object) throws IOException {
  
		if (object == null) {
			return null;
		}
		
		ByteArrayOutputStream 	baos = new ByteArrayOutputStream();
		ObjectOutputStream		oout = new ObjectOutputStream(baos);
		
		oout.writeObject(object);
		oout.close();

		return baos.toByteArray();
	}
	
	/**
	 * This function restore the object from byte array.
	 * 
	 * @param					buffer					The byte array to be restored.
	 * @return					Object					The object that is restored from the buffer. 
	 * @throws 					IOException
	 * @throws					ClassNotFoundException 
	 */
	public static Object restoreObject(byte[] buffer) throws IOException, ClassNotFoundException { 
		
		if (buffer == null) {
			return null;
		}
		
		ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(buffer));
		return objectInputStream.readObject();
		
	}
	
	/**
	 * This function compares given number of bytes.
	 * 
	 * @param 					leftValue					The left value.
	 * @param 					rightValue					The right value.
	 * @param 					compareLen					The compare length
	 * @return					boolean						true if all bytes are equal, false o/w.
	 */
	public static boolean compareBytes(byte[] leftValue, byte[] rightValue, int compareLen) {
		
		for (int i = 0; i < compareLen; i++) {
			
			if (leftValue[i] != rightValue[i]) {
				return false;
			}
			
		}
		
		return true;
		
	}
	
	/**
	 * swaps the string parameters characters.
	 * 
	 * @param 					value					the value.
	 * @return					String					the swapped value.
	 */
	public static String swapChars(String value) {
		
		char[] chars = value.toCharArray();
		char   tmp;
		for (int i = 0; i < chars.length; i++) {
			tmp 	   = chars[i];
			chars[i]   = chars[i + 1];
			chars[++i] = tmp; 
		}
		
		return new String(chars);
	}
	
	/**
	 * This function swap nibbles of the bytes of the input. 
	 * 
	 * @param 					value					The value to be swapped.
	 * @return					byte[]					The swapped array.
	 */
	public static byte[] swapBytes(byte[] value) {
		
		byte[] result  = value;
		int nibbleLow  = 0;
		int nibbleHigh = 0;
		//==================================================================================================
		// swap all byte - nibbles.
		//==================================================================================================
		for (int i = 0; i < result.length; i++) {
			
			nibbleLow  = (result[i] << 4)  & 0xF0;
			nibbleHigh = (result[i] >>> 4) & 0x0F;
			result[i] = (byte)((nibbleLow | nibbleHigh));
			
		}
		
		return result;		
		
	}
	
	/**
	 * This helper function add chars from the head.
	 * 
	 * @param 					value					The value to be padded.
	 * @param 					paddingChar				The character to be padded.
	 * @return					String					The whole characters.
	 */
	public static String paddFromHead(String value, String paddingChar) {
		
		if (value.length() % 2 != 0) {
			value = paddingChar + value;
		}
		
		return value;
		
	}
	
	/**
	 * This helper function composes the byte array representation by swapping bytes and padding F.
	 * 
	 * @param 					abyte0					the value to e converted
	 * @return					byte[]					the converted value.
	 */
	public static byte[] scramble(byte abyte0[]) {
        byte abyte1[];
        abyte1 = new byte[(abyte0.length % 2 != 1 ? abyte0.length : abyte0.length + 1) / 2];
        int i = 0;
        for(int j = 0; j < abyte0.length;) {
            int k = j + 1 >= abyte0.length ? 240 : (abyte0[j + 1] - 48 & 0xF) << 4;
            int l = abyte0[j] - 48 & 0xF;
            abyte1[i] = (byte)((k | l) & 0xFF);
            j += 2;
            i++;
        }

        return abyte1;
    
    }

	/**
	 * This helper function composes the byte array representation by swapping bytes and removing F.
	 * 
	 * @param 					abyte0					the value to e converted
	 * @return					byte[]					the converted value.
	 */
    public static byte[] unscramble(byte abyte0[]) {
        byte abyte1[];
        byte byte0 = abyte0[0];
        abyte1 = new byte[byte0];
        for(int i = 0; 2 * i < byte0; i++) {
            abyte1[2 * i] = (byte)((abyte0[i + 2] & 0xf) + 48);
            if(byte0 > 2 * i + 1) {
                abyte1[2 * i + 1] = (byte)((abyte0[i + 2] >> 4 & 0xf) + 48);
            }
        }

        return abyte1;
        
    }
	
	/**
	 * The function pads form queue until the given length is reached. 
	 * 
	 * @param 					value						The value to be pad.					
	 * @param 					paddingByte					The padding char.
	 * @param 					totalLength					The total length.
	 * @return					byte[]						The result.
	 */
	public static byte[] paddFromQueue(byte[] value, byte paddingByte, int totalLength) {
		
		byte[] result = new byte[totalLength];
		
		System.arraycopy(value, 0, result, 0, value.length);
		
		if (value.length  < totalLength) {
			for (int i = value.length; i < totalLength; i++) {
				result[i] = paddingByte;
			}
		}
		
		return result;
		
	}	
	
	/**
	 * This function computes the ceil of the given base.
	 * 
	 * @param 					currentLength					the current length.
	 * @param 					base							the base.
	 * @return					int								the computed length according to given base.
	 */
	public static int computeCeil(int currentLength, int base) {
		
		return computePartSize(currentLength, base) * base;
		
	}
	
	/**
	 * This function computes the floor of the given base.
	 * 
	 * @param 					currentLength					the current length.
	 * @param 					base							the base.
	 * @return					int								the computed length according to given base.
	 */
	public static int computeFloor(int currentLength, int base) {
		
		return (computePartSize(currentLength, base) - 1) * base;
		
	}
	
	/**
	 * The difference between ceil according to the base and the current length. 
	 * 
	 * @param 					currentLength					the current length.
	 * @param 					base							the base.
	 * @return					int								the difference between current length and the base.
	 */
	public static int computeDifferenceBetweenCeil(int currentLength, int base) {
		
		return computeCeil(currentLength, base) - currentLength;
		
	}
	
	/**
	 * This function returns the last n byte of the given byte array.
	 * 
	 * @param 					value							the byte array where the last n byte returned.
	 * @param					position						the position where cutting begins
	 * @param 					numberOfBytes					the number of bytes from the end.
	 * @return					byte[]							the returned byte array.
	 */
	public static byte[] getLastBytes(byte[] value, int position, int numberOfBytes) {
		
		byte[] result = new byte[numberOfBytes];
		
		System.arraycopy(value, value.length - position, result, 0, numberOfBytes);
		
		return result;
		
	}
	
	/**
	 * This function computes the total number of part needed to encapsulate each part composing of each part length. 
	 * 
	 * @param 					totalLength					the total length of the packet.
	 * @param 					partLength					each part length to be divided.
	 * @return					int							number of parts.
	 */
	public static int computePartSize(int totalLength, int partLength) {
		
		return (totalLength / partLength) + ((totalLength % partLength) == 0 ? 0 : 1);
		
	}
	
	/**
	 * This function converts the boolean string representation to boolean value.
	 * 
	 * @param 					strValue					the boolean value represented by a string
	 * @return					boolean						the boolean representation.
	 */
	public static boolean convertToBoolean(String strValue) {
		return Boolean.valueOf(strValue);
	}
	
	/**
	 * This function converts the boolean string representation to boolean value.
	 * 
	 * @param 					intValue					the boolean value represented by a string
	 * @return					boolean						the boolean representation.
	 */
	public static boolean convertToBoolean(int intValue) {
		return intValue == 0 ? false : true;
	}
	
	/**
	 * This function converts the boolean representation to string value.
	 * 
	 * @param 					boolValue					the boolean value represented by a string
	 * @return					boolean						the boolean representation.
	 */
	public static String convertToString(boolean boolValue) {
		return String.valueOf(boolValue).toUpperCase();
	}
	
	/**
	 * This function converts the boolean representation to string value.
	 * 
	 * @param 					boolValue					the boolean value represented by a string
	 * @return					boolean						the boolean representation.
	 */
	public static int convertToInt(boolean boolValue) {
		return boolValue ? 1 : 0;
	}
	
	/**
	* Convert a byte[] array to readable string format. This makes the "hex" readable!
	* 
	* @param 					in 					byte[] buffer to convert to string format.
	* @return 					result 				String buffer in String format.
	*/
	public static String byteArrayToHexString(byte in[]) {
	    byte	ch 	= 0x00;
	    int		i 	= 0;
	    
	    if (in == null || in.length <= 0) {
	        return null;
	    }
	    
	    StringBuffer out = new StringBuffer(in.length * 2);
	    
	    while (i < in.length) {
	    	
	        ch = (byte) (in[i] & 0xF0); // strip off high nibble
	        ch = (byte) (ch >>> 4);
	        
	        // shift the bits down
	        ch = (byte) (ch & 0x0F);    
	        
	        // must do this is high order bit is on!
	        out.append(PSEUDO[ (int) ch]); 		// convert the nibble to a String Character
	        ch = (byte) (in[i] & 0x0F); 		// strip off low nibble 
	        out.append(PSEUDO[ (int) ch]); 		// convert the nibble to a String Character
	        i++;
	        
	    }
	    
	    return new String(out);
	}
	
	/**
	 * reads the fixed length byte array.
	 * 
	 * @param 					len					the byte array length.
	 * @param 					dataInputStream		where to read data.
	 * @return					byte[]				the data.
	 * @throws IOException 
	 */
	public static byte[] readByteArray(int len, DataInputStream dataInputStream) throws IOException {
		
		byte[] value = new byte[len];
		dataInputStream.readFully(value);
		return value;
		
	}
	
	/**
	 * reads the fixed length byte array.
	 * 
	 * @param 					len					the byte array length.
	 * @param 					dataInputStream		where to read data.
	 * @return					byte[]				the data.
	 * @throws IOException 
	 */
	public static String readString(int len, DataInputStream dataInputStream) throws IOException {

		if (len == 0) {
			return null;
		}
		
		return new String(readByteArray(len, dataInputStream));
		
	}
	
	/**
	 * reads the fixed length byte array.
	 * 
	 * @param 					len					the byte array length.
	 * @param 					dataInputStream		where to read data.
	 * @return					byte[]				the data.
	 * @throws IOException 
	 */
	public static byte[] readByteArrayWithLen(int len, DataInputStream dataInputStream, String fieldName) throws IOException {
		
		if (len < 0) {
			throw new IllegalArgumentException("CANNOT deserialize, zero sized field -> " + fieldName);
		}
		
		return readByteArray(len, dataInputStream);
		
	}
	
	/**
	 * Reads given number of bytes from the stream and converts to string. 
	 * 
	 * @param 					len						the length of the data to be read.
	 * @param 					dataInputStream			where to read the data.
	 * @param 					fieldName				the field name.
	 * @return					String					converted data.
	 * @throws IOException 
	 */
	public static String readStringWithLen(int len, DataInputStream dataInputStream, String fieldName) throws IOException {
		
		return new String(readByteArrayWithLen(len, dataInputStream, fieldName));
		
	}
	
	/**
	 * checks if the value is null, o/w throws exception.
	 * 
	 * @param 					value					the value to check.
	 * @param 					fieldName				the message in case of error.
	 */
	public static void checkIfNull(Object value, String fieldName) {
		
		if (value != null) {
			return;
		}
		
		throw new IllegalArgumentException("The content CANNOT be null, for the field -> " + fieldName);
		
	}
	
	/**
	 * copies the length and the content (e.g. in LV format) to the destination. 
	 * 
	 * @param 					src						the source array.
	 * @param 					dest					the destination array.
	 * @param 					destPos					the destination position.
	 * @param 					length					the length of the source.
	 * @return					int						the index in the destination array.
	 * 
	 * ASSUMPTION : Although length can be calculated, it is given here for performance reason.
	 */
	public static int copyToArray(byte[] src, byte[] dest, int destPos, int length) {
		
		System.arraycopy(src, 0, dest, destPos, length);
		return destPos + length;
		
	}
	
	/**
	 * copies the length and the content (e.g. in LV format) to the destination. 
	 * 
	 * @param 					src						the source array.
	 * @param 					dest					the destination array.
	 * @param 					destPos					the destination position.
	 * @param 					length					the length of the source.
	 * @return					int						the index in the destination array.
	 */
	public static int copyToArrayWithByteLen(byte[] src, byte[] dest, int destPos, int length) {
		
		dest[destPos++] = (byte) length;
		return copyToArray(src, dest, destPos, length);
		
	}
	
	/**
	 * copies the length and the content (e.g. in LV format) to the destination. 
	 * 
	 * @param 					src						the source array.
	 * @param 					dest					the destination array.
	 * @param 					destPos					the destination position.
	 * @param 					length					the length of the source.
	 * @return					int						the index in the destination array.
	 */
	public static int copyToArrayWithShortLen(byte[] src, byte[] dest, int destPos, int length) {
		
		System.arraycopy(convertShortToByteArray((short) length), 0, dest, destPos, 2);
		destPos +=2;
		return copyToArray(src, dest, destPos, length);
		
	}
	
	public static void main(String[] args) {
		
		System.out.println(CommonUtil.byteArrayToHexString("Vodafone Cep Cuzdan".getBytes()));
		
	}
	
}
