package tr.com.metamorfoz.web.exception;

public class MMConversionException extends MMException {

	public MMConversionException(String message) {
		super(message);
	}
	
	public MMConversionException(String message, Throwable e) {
		super(message, e);
	}

	
}
