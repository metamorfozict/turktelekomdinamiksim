package tr.com.metamorfoz.web.shared.model.dstk.dstkinstance;

import java.util.List;

public class DstkInstanceJsonContentDto {

	private String title;
	private List<DstkInstanceJsonContentItemDto> items;
	
	public DstkInstanceJsonContentDto() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<DstkInstanceJsonContentItemDto> getItems() {
		return items;
	}

	public void setItems(List<DstkInstanceJsonContentItemDto> items) {
		this.items = items;
	}

	
      
}
