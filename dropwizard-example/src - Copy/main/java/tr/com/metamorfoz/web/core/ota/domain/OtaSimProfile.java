package tr.com.metamorfoz.web.core.ota.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.utility.ObjectUtil;
import tr.com.metamorfoz.web.utility.StringUtil;

public class OtaSimProfile extends BaseEntity {

	private static final String DEFAULT_VALUE_FOR_AUTH_METHOD = "MILENAGE";
	private static final String DEFAULT_VALUE_FOR_MIN_CIPHERING_LVL = "CIPHER";
	private static final String DEFAULT_VALUE_FOR_FLAG_REDUNDACY_CHECKSUM = "TRUE";
	private static final String DEFAULT_VALUE_FOR_FLAG_CRYPTOGRAPHIC_CHECKSUM = "TRUE";
	private static final String DEFAULT_VALUE_FOR_FLAG_DIGITAL_SIGNATURE = "TRUE";
	private static final String DEFAULT_VALUE_FOR_FLAG_CIPHERING_DES = "FALSE";
	private static final String DEFAULT_VALUE_FOR_FLAG_CIPHERING_3DES = "FALSE";
	private static final String DEFAULT_VALUE_FOR_RFM_APP_REF = "B00010";
	private static final Integer DEFAULT_VALUE_FOR_CMD_PACKETID_LEN = 0;
	private static final String DEFAULT_VALUE_FOR_CMD_CIPHER_DATA_PAD_CHAR = "00";
	private static final Integer DEFAULT_VALUE_FOR_RESPONSE_PACKETID_LEN = 0;
	private static final String DEFAULT_VALUE_FOR_EEPROM_SIZE = "64K";
	private static final String DEFAULT_VALUE_FOR_DEFAULT_CHANNELS = "";
	private static final String DEFAULT_VALUE_FOR_OTHER_PLATFORM_APP_REF = "200102";
	private static final String DEFAULT_VALUE_FOR_SPN_UPDATE_COMMAND = "A0A40000023F00A0A40000027F20A0A40000026F38A0D600000DFF3FFF0F03003F03000C000000A0A40000023F00A0A40000027F20A0A40000026F46A0D6000011";
	private static final String DEFAULT_VALUE_FOR_RAM_APP_REF = "000000";
	private static final String DEFAULT_VALUE_FOR_PUSH_APP_REF = "B00010";
	private static final String DEFAULT_VALUE_FOR_SUPP_APP_REF = "524A45";

	private static final String DEFAULT_VALUE_FOR_FLAG_NFC_SUPPORT = "FALSE";
	private static final String DEFAULT_VALUE_FOR_FLAG_JAVA_SUPPORT = "TRUE";
	private static final String DEFAULT_VALUE_FOR_MSIGN_SUPPORT = "NO";
	private static final String DEFAULT_VALUE_FOR_FLAG_OTASIMCHANGE_SUPPORT = "FALSE";
	
	@Length(max=20)
	@Column(length=20)
	private String authMethod; 

	@Length(max=15)
	@Column(length=15)
	private String minCipheringLvl; 
	
	@Length(max=5)
	@Column(length=5)
	private String flagRedundancyChecksum; 
	
	
	@Length(max=5)
	@Column(length=5)
	private String flagCryptographicChecksum; 
	
	@Length(max=5)
	@Column(length=5)
	private String flagDigitalSignature; 
	
	private Integer checksumLen;
	
	@Length(max=5)
	@Column(length=5)
	private String flagCipheringDes;

	
	@Length(max=5)
	@Column(length=5)
	private String flagCiphering3Des;
	
	@Length(max=6)
	@Column(length=6)
	private String rfmAppRef;

	private Integer cmdPacketIdLen;

	@Length(max=30)
	@Column(length=30)
	private String cmdPacketId;

	@Length(max=2)
	@Column(length=2)
	private String cmdHeaderId;
	
	@Length(max=2)
	@Column(length=2)
	private String cmdCipherDataPadChar;

	//TODO:Bu alan NUMBER(38) idi. Ne yapalım?
	private Integer responsePacketIdLen;

	@Length(max=30)
	@Column(length=30)
	private String responsePacketId;

	@Length(max=2)
	@Column(length=2)
	private String responseHeaderId;
	
	@Length(max=30)
	@Column(length=30)
	private String userName;

	@NotBlank
	@Length(max=5)
	@Column(length=5)
	private String eePromSize;
	
	@Length(max=50)
	@Column(length=50)
	private String defaultChannels;
	
	@Length(max=6)
	@Column(length=6)
	private String otherPlatformAppRef;

	@Length(max=500)
	@Column(length=500)
	private String spnUpdateCommand;

	@Length(max=6)
	@Column(length=6)
	private String ramAppRef;

	@Length(max=6)
	@Column(length=6)
	private String pushAppRef;

	@Length(max=6)
	@Column(length=6)
	private String suppAppRef;
	
	@Length(max=6)
	@Column(length=6)
	private String flagNfcSupport;
	
	@Length(max=6)
	@Column(length=6)
	private String flagJavaSupport;
	
	@Length(max=20)
	@Column(length=20)
	private String msignSupport;
	
	@Length(max=6)
	@Column(length=6)
	private String flagOtasimChangeSupport;

	public OtaSimProfile() {
		super();
	}

	public String getAuthMethod() {
		return StringUtil.defaultIfBlank(authMethod, DEFAULT_VALUE_FOR_AUTH_METHOD);
	}

	public void setAuthMethod(String authMethod) {
		this.authMethod = authMethod;
	}

	public String getMinCipheringLvl() {
		return StringUtil.defaultIfBlank(minCipheringLvl, DEFAULT_VALUE_FOR_MIN_CIPHERING_LVL);
	}

	public void setMinCipheringLvl(String minCipheringLvl) {
		this.minCipheringLvl = minCipheringLvl;
	}

	public String getFlagRedundancyChecksum() {
		return StringUtil.defaultIfBlank(flagRedundancyChecksum, DEFAULT_VALUE_FOR_FLAG_REDUNDACY_CHECKSUM);
	}

	public void setFlagRedundancyChecksum(String flagRedundancyChecksum) {
		this.flagRedundancyChecksum = flagRedundancyChecksum;
	}

	public String getFlagCryptographicChecksum() {
		return StringUtil.defaultIfBlank(flagCryptographicChecksum, DEFAULT_VALUE_FOR_FLAG_CRYPTOGRAPHIC_CHECKSUM);
	}

	public void setFlagCryptographicChecksum(String flagCryptographicChecksum) {
		this.flagCryptographicChecksum = flagCryptographicChecksum;
	}

	public String getFlagDigitalSignature() {
		return StringUtil.defaultIfBlank(flagDigitalSignature, DEFAULT_VALUE_FOR_FLAG_DIGITAL_SIGNATURE);
	}

	public void setFlagDigitalSignature(String flagDigitalSignature) {
		this.flagDigitalSignature = flagDigitalSignature;
	}

	public Integer getChecksumLen() {
		return checksumLen;
	}

	public void setChecksumLen(Integer checksumLen) {
		this.checksumLen = checksumLen;
	}

	public String getFlagCipheringDes() {
		return StringUtil.defaultIfBlank(flagCipheringDes, DEFAULT_VALUE_FOR_FLAG_CIPHERING_DES);
	}

	public void setFlagCipheringDes(String flagCipheringDes) {
		this.flagCipheringDes = flagCipheringDes;
	}

	public String getFlagCiphering3Des() {
		return StringUtil.defaultIfBlank(flagCiphering3Des, DEFAULT_VALUE_FOR_FLAG_CIPHERING_3DES);
	}

	public void setFlagCiphering3Des(String flagCiphering3Des) {
		this.flagCiphering3Des = flagCiphering3Des;
	}

	public String getRfmAppRef() {
		return StringUtil.defaultIfBlank(rfmAppRef, DEFAULT_VALUE_FOR_RFM_APP_REF);
	}

	public void setRfmAppRef(String rfmAppRef) {
		this.rfmAppRef = rfmAppRef;
	}

	public Integer getCmdPacketIdLen() {
		return cmdPacketIdLen;
	}

	public void setCmdPacketIdLen(Integer cmdPacketIdLen) {
		this.cmdPacketIdLen = cmdPacketIdLen;
	}

	public String getCmdPacketId() {
		return cmdPacketId;
	}

	public void setCmdPacketId(String cmdPacketId) {
		this.cmdPacketId = cmdPacketId;
	}

	public String getCmdHeaderId() {
		return cmdHeaderId;
	}

	public void setCmdHeaderId(String cmdHeaderId) {
		this.cmdHeaderId = cmdHeaderId;
	}

	public String getCmdCipherDataPadChar() {
		return StringUtil.defaultIfBlank(cmdCipherDataPadChar, DEFAULT_VALUE_FOR_CMD_CIPHER_DATA_PAD_CHAR);
	}

	public void setCmdCipherDataPadChar(String cmdCipherDataPadChar) {
		this.cmdCipherDataPadChar = cmdCipherDataPadChar;
	}

	public Integer getResponsePacketIdLen() {
		return (Integer) ObjectUtil.defaultIfNull(responsePacketIdLen, DEFAULT_VALUE_FOR_RESPONSE_PACKETID_LEN);
	}

	public void setResponsePacketIdLen(Integer responsePacketIdLen) {
		this.responsePacketIdLen = responsePacketIdLen;
	}

	public String getResponsePacketId() {
		return responsePacketId;
	}

	public void setResponsePacketId(String responsePacketId) {
		this.responsePacketId = responsePacketId;
	}

	public String getResponseHeaderId() {
		return responseHeaderId;
	}

	public void setResponseHeaderId(String responseHeaderId) {
		this.responseHeaderId = responseHeaderId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEePromSize() {
		return StringUtil.defaultIfBlank(eePromSize, DEFAULT_VALUE_FOR_EEPROM_SIZE);
	}

	public void setEePromSize(String eePromSize) {
		this.eePromSize = eePromSize;
	}

	public String getDefaultChannels() {
		return StringUtil.defaultIfBlank(defaultChannels, DEFAULT_VALUE_FOR_DEFAULT_CHANNELS);
	}

	public void setDefaultChannels(String defaultChannels) {
		this.defaultChannels = defaultChannels;
	}

	public String getOtherPlatformAppRef() {
		return StringUtil.defaultIfBlank(otherPlatformAppRef, DEFAULT_VALUE_FOR_OTHER_PLATFORM_APP_REF);
	}

	public void setOtherPlatformAppRef(String otherPlatformAppRef) {
		this.otherPlatformAppRef = otherPlatformAppRef;
	}

	public String getSpnUpdateCommand() {
		return StringUtil.defaultIfBlank(spnUpdateCommand, DEFAULT_VALUE_FOR_SPN_UPDATE_COMMAND);
	}

	public void setSpnUpdateCommand(String spnUpdateCommand) {
		this.spnUpdateCommand = spnUpdateCommand;
	}

	public String getRamAppRef() {
		return StringUtil.defaultIfBlank(ramAppRef, DEFAULT_VALUE_FOR_RAM_APP_REF);
	}

	public void setRamAppRef(String ramAppRef) {
		this.ramAppRef = ramAppRef;
	}

	public String getPushAppRef() {
		return StringUtil.defaultIfBlank(pushAppRef, DEFAULT_VALUE_FOR_PUSH_APP_REF);
	}

	public void setPushAppRef(String pushAppRef) {
		this.pushAppRef = pushAppRef;
	}

	public String getSuppAppRef() {
		return StringUtil.defaultIfBlank(suppAppRef, DEFAULT_VALUE_FOR_SUPP_APP_REF);
	}

	public void setSuppAppRef(String suppAppRef) {
		this.suppAppRef = suppAppRef;
	}

	public String getFlagNfcSupport() {
		return StringUtil.defaultIfBlank(flagNfcSupport, DEFAULT_VALUE_FOR_FLAG_NFC_SUPPORT);
	}

	public void setFlagNfcSupport(String flagNfcSupport) {
		this.flagNfcSupport = flagNfcSupport;
	}

	public String getFlagJavaSupport() {
		return StringUtil.defaultIfBlank(flagJavaSupport, DEFAULT_VALUE_FOR_FLAG_JAVA_SUPPORT);
	}

	public void setFlagJavaSupport(String flagJavaSupport) {
		this.flagJavaSupport = flagJavaSupport;
	}

	public String getMsignSupport() {
		return StringUtil.defaultIfBlank(msignSupport, DEFAULT_VALUE_FOR_MSIGN_SUPPORT);
	}

	public void setMsignSupport(String msignSupport) {
		this.msignSupport = msignSupport;
	}

	public String getFlagOtasimChangeSupport() {
		return StringUtil.defaultIfBlank(flagOtasimChangeSupport, DEFAULT_VALUE_FOR_FLAG_OTASIMCHANGE_SUPPORT);
	}

	public void setFlagOtasimChangeSupport(String flagOtasimChangeSupport) {
		this.flagOtasimChangeSupport = flagOtasimChangeSupport;
	}

	
	
	
}



