package tr.com.metamorfoz.web.core.ota.dao;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaSimProfile;

public class OtaSimProfileDao extends AbstractDao<OtaSimProfile> {

	public OtaSimProfileDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

}
