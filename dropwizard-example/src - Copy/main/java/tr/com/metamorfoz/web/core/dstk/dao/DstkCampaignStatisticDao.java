package tr.com.metamorfoz.web.core.dstk.dao;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;

public class DstkCampaignStatisticDao extends AbstractDao<DstkCampaignStatistic> {

	public DstkCampaignStatisticDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	
}
