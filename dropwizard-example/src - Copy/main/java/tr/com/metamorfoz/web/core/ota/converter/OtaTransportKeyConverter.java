package tr.com.metamorfoz.web.core.ota.converter;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.shared.model.ota.OtaTransportKeyDto;

public class OtaTransportKeyConverter extends AbstractEntityConverter<OtaTransportKey, OtaTransportKeyDto>  {

	@Override
	public OtaTransportKey createEntity(final OtaTransportKeyDto dto) {
		OtaTransportKey entity = new OtaTransportKey();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(final OtaTransportKey entity, OtaTransportKeyDto dto) {
		entity.setNo(dto.getNo());
		entity.setValue(dto.getValue());
		entity.setAlgorithmName(dto.getAlgorithmName());
		entity.setAlgorithmMode(dto.getAlgorithmMode());
	}

	@Override
	public OtaTransportKeyDto convertToDto(final OtaTransportKey entity) {
		OtaTransportKeyDto keyDto = new OtaTransportKeyDto(entity.getId(), entity.getNo(), entity.getValue(), entity.getAlgorithmName(), entity.getAlgorithmMode());
		return keyDto;
	}




}
