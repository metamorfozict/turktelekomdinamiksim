package tr.com.metamorfoz.web.exception;

public abstract class MMException extends RuntimeException {

	public MMException(String message) {
		super(message);
	}
	
	public MMException(String message, Throwable e) {
		super(message, e);
	}
	
}
