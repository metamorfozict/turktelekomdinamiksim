package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import java.util.List;
import java.util.Map;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstanceDao;

public class DstkInstanceDomainService extends AbstractDomainCrudService<DstkInstance, DstkInstanceDao> {

	public DstkInstanceDomainService(DstkInstanceDao dao) {
		super(dao);
	}

	public DstkInstance findUniqueDstkInstanceByVersion(int instance, String instanceHeader, int instanceVersion) {
		return 	getDao().findUniqueDstkInstanceByVersion(instance, instanceHeader, instanceVersion);
	}
	
}
