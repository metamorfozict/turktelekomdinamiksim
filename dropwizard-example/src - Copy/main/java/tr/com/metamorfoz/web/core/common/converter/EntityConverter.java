package tr.com.metamorfoz.web.core.common.converter;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

public interface EntityConverter<E extends BaseEntity, D> {

	public E createEntity(final D dto);
	public void updateEntity(final E entity, D dto);
	public D convertToDto(final E entity);
	
}
