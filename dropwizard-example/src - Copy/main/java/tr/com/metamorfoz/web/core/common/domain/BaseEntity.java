package tr.com.metamorfoz.web.core.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;

@MappedSuperclass
public abstract class BaseEntity {

	@Id
	@SequenceGenerator(name="SEQ_OTASS_GENERIC")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_OTASS_GENERIC")
	@Column(name="N_ID")
	private Integer id;
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name="D_ENT_DATE")
	private Date creationTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="D_MOD_DATE")
	private Date modificationTime;
	
	@PreUpdate
	public void preUpdate() {
	    modificationTime = new Date();
	}
	  
	@PrePersist
	public void prePersist() {
	    Date now = new Date();
	    creationTime = now;
	    modificationTime = now;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}


}    