package tr.com.metamorfoz.web.core.dstk.converter;

import java.lang.reflect.InvocationTargetException;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.exception.MMConversionException;
import tr.com.metamorfoz.web.shared.model.dstk.DstkCampaignStatisticDto;
import tr.com.metamorfoz.web.utility.BeanUtil;

public class DstkCampaignStatisticConverter extends AbstractEntityConverter<DstkCampaignStatistic, DstkCampaignStatisticDto> {

	@Override
	public DstkCampaignStatistic createEntity(DstkCampaignStatisticDto dto) {
		DstkCampaignStatistic entity = new DstkCampaignStatistic();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(DstkCampaignStatistic entity, DstkCampaignStatisticDto dto) {
		entity.setAccepted(dto.getAccepted());
		entity.setBlackListed(dto.getBlackListed());
		entity.setDelivered(dto.getDelivered());
		entity.setDescription(dto.getDesciption());
//		entity.setDetail(detail);
		entity.setNonDelivered(dto.getNonDelivered());
		entity.setSent(dto.getSent());
		entity.setServiceId(dto.getServiceId());
		entity.setStartTime(dto.getStartTime());
		entity.setStatus(dto.getStatus());
		entity.setTotal(dto.getTotal());
		entity.setType(dto.getType());
	}

	@Override
	public DstkCampaignStatisticDto convertToDto(DstkCampaignStatistic entity) {
		DstkCampaignStatisticDto dto = new DstkCampaignStatisticDto();
		try {
			BeanUtil.copyProperties(dto, entity);
		} catch (Exception e) {
			throw new MMConversionException("Conversion error:", e);
		}
		return dto;
	}




}
