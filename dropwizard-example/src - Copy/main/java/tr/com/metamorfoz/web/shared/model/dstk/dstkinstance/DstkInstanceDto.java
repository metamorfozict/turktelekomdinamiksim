package tr.com.metamorfoz.web.shared.model.dstk.dstkinstance;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class DstkInstanceDto {

	private Integer id;
	
	@Length(max = 100)
	private String instanceHeader;
	
	private Integer instance;
	
	@NotBlank
	@Length(max = 32)
	private String appId;
	
	@Length(max = 250)
	private String intrBlock;
	
	@Length(max = 200)
	private String profile;
	
	@Length(max = 10)
	private String tar;
	
	private Integer instanceVersion;

	public DstkInstanceDto() {
	}
	
	public DstkInstanceDto(Integer id, String instanceHeader, Integer instance,
			String appId, String intrBlock, String profile, String tar,
			Integer instanceVersion) {
		super();
		this.id = id;
		this.instanceHeader = instanceHeader;
		this.instance = instance;
		this.setAppId(appId);
		this.intrBlock = intrBlock;
		this.profile = profile;
		this.tar = tar;
		this.setInstanceVersion(instanceVersion);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInstanceHeader() {
		return instanceHeader;
	}

	public void setInstanceHeader(String instanceHeader) {
		this.instanceHeader = instanceHeader;
	}

	public Integer getInstance() {
		return instance;
	}

	public void setInstance(Integer instance) {
		this.instance = instance;
	}

	public String getIntrBlock() {
		return intrBlock;
	}

	public void setIntrBlock(String intrBlock) {
		this.intrBlock = intrBlock;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getTar() {
		return tar;
	}

	public void setTar(String tar) {
		this.tar = tar;
	}


	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getInstanceVersion() {
		return instanceVersion;
	}

	public void setInstanceVersion(Integer instanceVersion) {
		this.instanceVersion = instanceVersion;
	}
	
	
}
