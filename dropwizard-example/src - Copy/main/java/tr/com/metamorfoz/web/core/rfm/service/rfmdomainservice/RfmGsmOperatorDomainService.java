package tr.com.metamorfoz.web.core.rfm.service.rfmdomainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.rfm.dao.RfmGsmOperatorDao;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;

public class RfmGsmOperatorDomainService extends AbstractDomainCrudService<RfmGsmOperator, RfmGsmOperatorDao> {

	public RfmGsmOperatorDomainService(RfmGsmOperatorDao dao) {
		super(dao);
	}

}
