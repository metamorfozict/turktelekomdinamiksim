package tr.com.metamorfoz.web.core.common.api;

import java.util.List;

import tr.com.metamorfoz.web.core.common.converter.EntityConverter;
import tr.com.metamorfoz.web.core.common.converter.EntityConverterUtil;
import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.core.common.service.DomainCrudService;

public class AbstractApiDtoCrudService<E extends BaseEntity, D, 
									O extends DomainCrudService<E>,  
									C extends EntityConverter<E, D>> {

	private O domainCrudService;
	private C converter;
	private int fakeId;

	public AbstractApiDtoCrudService(O domainCrudService, C converter){
		this.domainCrudService = domainCrudService;
		this.converter = converter;
	}
	
	public D create(D dto) {
		//Assumption:Validation should be done at ui.
		E entity = converter.createEntity(dto);
		entity.setId(++fakeId);
		entity = domainCrudService.save(entity);
		dto = converter.convertToDto(entity);
		return dto;
	}

	public D update(int id, D dto) {
		E entity = domainCrudService.findSafely(id);
		converter.updateEntity(entity, dto);
		//Kayıtların id'lerinin değiştirilmemesini istediğimiz için önce id ile kaydın varlığını kontrol ettik.
		//Sonrasında da dto'daki id farklı olabilir diye id'yi kontrol ettiğimiz id ile tekrar güncelledik.
//		entity.setId(id);  
		entity = domainCrudService.save(entity);
		return converter.convertToDto(entity);
	}
	
	public List<D> findAll(){
		List<E> entityList = domainCrudService.findAll();
		return EntityConverterUtil.convertToDtoList(converter, entityList);
	}
	
	public D findSafely(int id)   {
		E entity = domainCrudService.find(id);
		return converter.convertToDto(entity);
	}
	
	public void delete(int id) {
		E entity = domainCrudService.findSafely(id);
		domainCrudService.delete(entity);
	}
	
	public int deleteAll(List<Integer> ids) {
		return domainCrudService.deleteAll(ids);
	}

	public O getDomainCrudService() {
		return domainCrudService;
	}

	public C getConverter() {
		return converter;
	}


}
