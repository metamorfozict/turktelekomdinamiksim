package tr.com.metamorfoz.web;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.web.core.dstk.api.DstkCampaignStatisticApiDtoService;
import tr.com.metamorfoz.web.core.dstk.api.DstkInstanceApiDtoService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkCampaignStatisticConverter;
import tr.com.metamorfoz.web.core.dstk.converter.DstkInstanceConverter;
import tr.com.metamorfoz.web.core.dstk.dao.DstkCampaignStatisticDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstanceDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModuleDao;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkCampaignStatisticDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkInstanceDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkModuleDomainService;
import tr.com.metamorfoz.web.core.ota.api.OtaApiService;
import tr.com.metamorfoz.web.core.ota.dao.OtaSimProfileDao;
import tr.com.metamorfoz.web.core.ota.dao.OtaTransportKeyDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.core.ota.service.OtaService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaSimProfileDomainService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaTransportKeyDomainService;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkCampaignStatisticResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkInstanceResource;
import tr.com.metamorfoz.web.ui.resource.ota.OtaTransportKeyResource;
import tr.com.metamorfoz.web.ui.resource.standart.ValidationResource;

import com.example.helloworld.cli.RenderCommand;

public class MMWebApplication extends Application<MMWebConfiguration> {
	
    public static void main(String[] args) throws Exception {
        new MMWebApplication().run(args);
    }

    private final HibernateBundle<MMWebConfiguration> hibernateBundle = new HibernateBundle<MMWebConfiguration>(DstkCampaignStatistic.class, DstkInstance.class, DstkModule.class, RfmGsmOperator.class, OtaTransportKey.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(MMWebConfiguration configuration) {
                	DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
                	dataSourceFactory.setAutoCommitByDefault(true);
                    return dataSourceFactory;
                }
                
               
            };

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<MMWebConfiguration> bootstrap) {
        bootstrap.addCommand(new RenderCommand());
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new MigrationsBundle<MMWebConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(MMWebConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(MMWebConfiguration configuration, Environment environment) throws ClassNotFoundException {   	
       
    	SessionFactory sessionFactory = hibernateBundle.getSessionFactory();

    	/***********   DSTK  *****************/
        final DstkInstanceDao dstkInstanceDao = new DstkInstanceDao(sessionFactory);
        final DstkModuleDao dstkModuleDao = new DstkModuleDao(sessionFactory);
        final DstkCampaignStatisticDao dstkCampaignStatisticDao = new DstkCampaignStatisticDao(sessionFactory);
      
		final DstkInstanceDomainService dstkInstanceDomainService = new DstkInstanceDomainService(dstkInstanceDao);
		final DstkModuleDomainService dstkModuleDomainService = new DstkModuleDomainService(dstkModuleDao);
		
        
        final DstkInstanceDomainService domainCrudService = new DstkInstanceDomainService(dstkInstanceDao);
        final DstkInstanceConverter converter = new DstkInstanceConverter();
		final DstkInstanceApiDtoService dstkInstanceApiDtoService = new DstkInstanceApiDtoService(domainCrudService, converter, dstkModuleDomainService);

		final DstkCampaignStatisticDomainService dstkCampaignStatisticDomainService = new DstkCampaignStatisticDomainService(dstkCampaignStatisticDao);
		final DstkCampaignStatisticConverter dstkCampaignStatisticConverter = new DstkCampaignStatisticConverter();
		final DstkCampaignStatisticApiDtoService dstkCampaignStatisticApiDtoService = new DstkCampaignStatisticApiDtoService(dstkCampaignStatisticDomainService, dstkCampaignStatisticConverter);

    	/***********   DSTK  RESOURCES *****************/
		
		environment.jersey().register(new DstkInstanceResource(dstkInstanceApiDtoService));
		
    	/***********   OTA   *****************/
    	final OtaSimProfileDao simProfileDao = new OtaSimProfileDao(sessionFactory);
    	final OtaTransportKeyDao transportKeyDao = new OtaTransportKeyDao(sessionFactory);
    	final OtaTransportKeyDomainService otaTransportKeyDomainService = new OtaTransportKeyDomainService(transportKeyDao);
    	final OtaSimProfileDomainService otaSimProfileDomainService = new OtaSimProfileDomainService(simProfileDao);
    	final OtaService otaDomainService = new OtaService(dstkInstanceDomainService, dstkModuleDomainService, otaSimProfileDomainService, otaTransportKeyDomainService); 
    	final OtaApiService otaApiService = new OtaApiService(otaDomainService);
    	environment.jersey().register(new OtaTransportKeyResource(otaApiService));
    	
    	/***********   OTA RESOURCES *****************/				
		
		environment.jersey().register(new DstkCampaignStatisticResource(dstkCampaignStatisticApiDtoService));

		/***********   OTHER RESOURCES *****************/
		
        environment.jersey().register(new ValidationResource());
        
        
    }
}
