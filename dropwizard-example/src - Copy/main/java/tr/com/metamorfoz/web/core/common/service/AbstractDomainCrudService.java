package tr.com.metamorfoz.web.core.common.service;

import java.util.List;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.exception.RecordNotFoundException;

public abstract class AbstractDomainCrudService<E extends BaseEntity, O extends AbstractDao<E>> implements DomainCrudService<E> {
	
	private O dao;

	public AbstractDomainCrudService(O dao) {
		this.dao = dao;
	}

	@Override
	public List<E> findAll(){
		return getDao().list();
	}

	@Override
	public E find(int id){
		return getDao().get(id);
	}
	
	@Override
	public E findSafely(int id) throws RecordNotFoundException  {
		E gsmOperator = find(id);
		if (gsmOperator==null)
			throw new RecordNotFoundException("Record not found:" + id);
		return gsmOperator;
	}
	
	@Override
	public E save(E t) {
		return getDao().persist(t);		
	}
	
	@Override
	public void delete(E t) {
		getDao().delete(t);		
	}
	
	@Override
	public int deleteAll(List<Integer> ids) {
		return getDao().deleteAll(ids);
	}

	public O getDao() {
		return dao;
	}

	
	
}
