package com.example.helloworld.core;

import org.hibernate.validator.constraints.NotBlank;

public class Sample {

	@NotBlank
	private String name;
	
	@NotBlank
	private String surname;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	
	
	
}
