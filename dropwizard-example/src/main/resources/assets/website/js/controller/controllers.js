
mainControllers.filter("truncate", function(){
    return function(text, length){
    	
    	if (text){
    		return text.substring(0, text.indexOf("_v"));	
    	} else {
    		return "";
    	}
    	
        
    }
});

// Mobile menu controller
mainControllers.controller('instanceDesignController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
	$scope.editMode = false;

	$scope.activeItem = null;

	$scope.toggleEditMode = function() {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		$scope.editMode = !$scope.editMode;
	}
	
	// Item Actions
	$scope.toggleItemActive = function(item) {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		item.active = true;
	}

	$scope.$watch("url", function() {
    	
    	if ($scope.url){
    		var url = $scope.url + "/" +  $routeParams.itemId;
    		console.log("Url:" + url)
    	
			$http.get(url).success(function(data) {
				console.log("data:" + data);
				var jsonObj = JSON.parse(data.json);
				console.log("jsonObj:" + jsonObj);
				$scope.menuItems = jsonObj;
			});		
    	}
    	
    });
	
	$scope.$watch("parameterUrl", function() {
    	
    	if ($scope.parameterUrl){
    	
			$http.get($scope.parameterUrl).success(function(data) {
				console.log(JSON.stringify(data));
				$scope.options = data;
				console.log(JSON.stringify($scope.options));
			});		
    	}
    	
    });
	
	$scope.parameterUrl = "/api/dstkinstance";
	
	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.subItemEdit = function(item) {
		if (item.items && item.title && !$scope.editMode) {
			$scope.activeItem = item;
			$scope.editMode = false;
		}
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y',
		handle: ".icon-exchange"
	};

  	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
		
		var json = JSON.stringify($scope.menuItems)
		console.log("json:" + json);
		
		var url = $scope.url;
		var method = 'POST';
		if ($routeParams && $routeParams.itemId){
			url = url + "/" + $routeParams.itemId;
			method = 'PUT';
		}
        console.log(url); 
		
       $http({
          method: method,
          url: url,
          data: json,
          headers: {'Content-Type': 'application/json'}
       }).
       success(function(data, status, headers, config) {
          window.alert("Alert", "Successfully saved");
       }).
       error(function(data, status, headers, config) {
          window.alert("Alert", "Menu name : " + $scope.version + " error status : " + status + " " + data);
       });

		
	};

}]);

mainControllers.controller('designsTableController', [ '$scope', '$timeout', '$location', '$filter', '$http', '$routeParams',
   function($scope, $timeout, $location, $filter, $http, $routeParams) {

		       $scope.tabs = [ {
						           "label" : "SERVICE",
								   "headers" : ["SERVICE TITLE","SERVICE TYPE","MODIFICATION DATE"],
								   "url" : "/api/dstkmodule",
								   "path" : "/interactifdesign" 	   
							   }, {
							       "label" : "MENU",
								   "headers" : ["MENU TITLE","VERSION","MODIFICATION DATE"],
								   "url" : "/api/dstkinstance",
								   "path" : "/menudesign"
							   }, {
							       "label" : "MAIN MENU",
								   "headers" : ["MENU TITLE","VERSION","MODIFICATION DATE"],
								   "url" : "/api/dstkmainmenu",
								   "path" : "/instancedesign"
							   }];
		
		   $scope.selectedTab = $scope.tabs[0];
		   $scope.setSelectedTab = function(tab) {
		       $scope.selectedTab = tab;
		       $scope.url = tab.url;
		   }
		
		   $scope.tabClass = function(tab) {
		       if ($scope.selectedTab == tab) {
		           return "active";
		   } else {
		       return "";
		       }
		   }
		
		   $scope.$watch("url", function() {
		   $http.get($scope.url).success(function(data) {
		       $scope.items = data;
		
		       $scope.filteredItems = $scope.items;
		       $scope.removeItems = [];
		       $scope.itemsPerPage = 10;
		       $scope.pagedItems = [];
		       $scope.currentPage = 0;
		       $scope.query = "";
		
		           $scope.groupToPages();
		       });
		   });
		
		   $scope.url = $scope.selectedTab.url;
		   
		   // init
		   $scope.sort = {
		       sortingOrder : 'id',
		       reverse : false
		   };
		
		   $scope.getData = function(items, query) {
		       $scope.filteredItems = $filter('filter')(items,
		           query);
		       $scope.groupToPages();
		   };
		
		   // calculate page in
		   // place
		   $scope.groupToPages = function() {
		
		       $scope.currentPage = 0;
		       $scope.pagedItems = [];
		
		       for (var i = 0; i < $scope.filteredItems.length; i++) {
		           if (i % $scope.itemsPerPage === 0) {
		               $scope.pagedItems[Math.floor(i
		                   / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
		           } else {
		               $scope.pagedItems[Math.floor(i
		                   / $scope.itemsPerPage)]
		                   .push($scope.filteredItems[i]);
		           }
		       }
		   };
		
		   $scope.range = function(size) {
		
		       var ret = [];
		
		       for (var i = 0; i < size; i++) {
		           ret.push(i);
		       }
		
		       return ret;
		   };
		
		   $scope.prevPage = function() {
		       if ($scope.currentPage > 0) {
		           $scope.currentPage--;
		       }
		   };
		
		   $scope.nextPage = function() {
		       if ($scope.currentPage < $scope.pagedItems.length - 1) {
		           $scope.currentPage++;
		       }
		   };
		
		   $scope.setPage = function() {
		       $scope.currentPage = this.n;
		   };
		
		   $scope.putToRemoveBag = function(item) {
		
		       var i = $scope.removeItems.indexOf(item);
		       if (i >= 0) {
		           $scope.removeItems.splice(i, 1);
		           item.checked = " ";
		   } else {
		       $scope.removeItems.push(item);
		       item.checked = "checked";
		       }
		
		   };
		
		   $scope.deleteAll = function() {
		       if (confirm("Are you sure?")) {
		    	   var ids = "";
		           for (var i = 0; i < $scope.removeItems.length; i++) {
		               var j = $scope.filteredItems.indexOf($scope.removeItems[i]);
		               $scope.filteredItems.splice(j, 1);
		               ids = ids + "ids=" + $scope.removeItems[i].id + "&";
		           }
		           
					var url = $scope.selectedTab.url  + "?" + ids;
					
					// Delete
					if (ids){
						$http.delete(url).success(function(data) {
							$scope.item = data;
						});										
					}
		           
		       }
		       
		       $scope.removeItems = [];
		       $scope.groupToPages();
		   };
		
		   $scope.editItem = function(item) {
		
		       console.log(item);
		       $location.path($scope.selectedTab.path + '/' + item.id);    
		
		   }
		
		   $scope.openDesignScreen = function() {
		
			   $location.path($scope.selectedTab.path);
		
			}

   } ]);


// Mobile menu controller
mainControllers.controller('menuDesignController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
	$scope.editMode = false;

	$scope.activeItem = null;

	$scope.toggleEditMode = function() {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		$scope.editMode = !$scope.editMode;
	}
	
	$scope.$watch("url", function() {
    	
    	if ($scope.url){
    		var url = $scope.url + "/" +  $routeParams.itemId;
    		console.log("Url:" + url)
    	
			$http.get(url).success(function(data) {
				console.log("data:" + data);
				var jsonObj = JSON.parse(data.json);
				console.log("jsonObj:" + jsonObj);
				
				$scope.instance = data.instance;
				$scope.instanceHeader = data.instanceHeader;
				$scope.instanceVersion = data.instanceVersion;
				$scope.menuItems = jsonObj;
			});		
    	}
    	
    });
	
	$scope.$watch("parameterUrl", function() {
    	
    	if ($scope.parameterUrl){
    	
			$http.get($scope.parameterUrl).success(function(data) {
				console.log(JSON.stringify(data));
				$scope.options = data;
				console.log(JSON.stringify($scope.options));
			});		
    	}
    	
    });
	
	$scope.parameterUrl = "/api/dstkmodule/searchmenumodule";
	
	// Item Actions
	$scope.toggleItemActive = function(item) {
		angular.forEach($scope.menuItems.items, function(item) {
			item.active = false;
		});
		item.active = true;
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.subItemEdit = function(item) {
		if (item.items && item.title && !$scope.editMode) {
			$scope.activeItem = item;
			$scope.editMode = false;
		}
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y',
		handle: ".icon-exchange"
	};

  	$scope.exportJSON = function() {
		
		var json = {
					  "instance" : $scope.instance,
					  "instanceHeader" :  $scope.instanceHeader,
					  "instanceVersion" :  $scope.instanceVersion,
					  "menuContent" : $scope.menuItems
					};
		
		console.log("json:" + JSON.stringify(json));
		
		var url = $scope.url;
		var method = 'POST';
		if ($routeParams && $routeParams.itemId){
			url = url + "/" + $routeParams.itemId;
			method = 'PUT';
		}
        console.log(url); 
		
		$http({
  		    method: method,
  		    url: url,
  		    data: json,
  		    headers: {'Content-Type': 'application/json'}
  		}).
        success(function(data, status, headers, config) {
        	console.log("success");
        	window.alert("Alert", "Successfully saved");
        }).
        error(function(data, status, headers, config) {
        	console.log("error");
        	// window.alert("Alert", "Menu name : " + $scope.version + " error
			// status : " + status + " " + data);
        	window.alert("Alert", "errors : " + data.errors);
        });
		
	};

}]);

// Tree menu controller
mainControllers.controller('ramDesignController', ['$scope', function($scope) {
	$scope.menuItems = [
		{
			title: "ISD (Card Manager - A0000000000000189010111200000001)",
			items: [
				{
					title: "RFM PKG (A00000000000001890141100B0000101)",
					pkg:{aid:"A00000000000001890141100B0000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",	
					items:[]
				},
				{
					title: "RAM PKG (A00000000000001890141100B0000101)",
					pkg:{aid:"A00000000000001890141100B0000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",	
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010101)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010201)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010301)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				},
				{
					title: "DSTK (A0000000000000189014110020010301)",
					pkg:{aid:"A0000000000000189014110020000101"},
					mdl:{aid:"A00000000000001890141100B0000101"},
					cls:{aid:"A00000000000001890141100B0000101"},
					pkgSize:"10000",
					insSize:"2000",
					insVolSize:"300",
					sysParams:"0A",
					appParams:"09C40C091515011632340A05",
					etsiParams:"0E0C0014000001032001001",
					cntlessParams:"",
					items:[]
				}
				
			]
		}
	];

	$scope.removeItem = function(items, item) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
	}

}]);

// Tree menu controller
mainControllers.controller('rfmDesignController', ['$scope', function($scope) {
	$scope.menuItems = [
		{
			title: "Root Folder [3F 00]",
			description: "GSM DDF",
			type: "DDF",
			value: "",
			items: [
			        {
					title: "GSM [7F20]",
					description: "GSM DDF",
					type: "DDF",
					value: "",
					items:[{ title: "IMSI [6F07]"
						   , description: "holds the IMSI"
						   , type: "Transparent"
						   , value: "090807060504030201\r\n010203040506070809\r\n"}
						  ]
			        }
			        ]
		}
				
			];

	$scope.removeItem = function(items, item) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
		}
	}

	$scope.exportJSON = function() {
		console.log(JSON.stringify($scope.menuItems));
	}

}]);

mainControllers.controller('interactifDesignController', ['$scope', '$modal', '$timeout', '$window', '$http', '$routeParams'
                                                          , function($scope, $modal, $timeout, $window, $http, $routeParams) {
	
	$http.get("/api/dstkmodule/searchterminalmodule").success(function(data) {
		
		$scope.options=data;
		
	});
	
	$scope.$watch("url", function() {
    	
    	if ($scope.url){
    		var url = $scope.url + "/" +  $routeParams.itemId;
    		console.log("Url:" + url)
    	
			$http.get(url).success(function(data) {
				
				var jsonObj = JSON.parse(data.json);
				$scope.moduleName = data.moduleName;
				$scope.type = data.type;
				$scope.moduleId = data.moduleId;
				$scope.id = data.id;
				$scope.items = jsonObj;
				$scope.currentItem = $scope.items[0];
				
			});		
    	}
    	
    });
	
	$scope.selectItem = function(item) {
		$scope.currentItem = item;
	}

	$scope.removeItem = function(item, items) {
		if (confirm("Are you sure?")) {
			var i = items.indexOf(item);
			items.splice(i, 1);
			$scope.currentItem = null;
			
			if (items.length != 0) {
				if (i == 0) {
					$scope.selectItem(items[0]);
					$scope.selectItem(items[0]);
				} else {
					$scope.selectItem(items[i - 1]);
					$scope.selectItem(items[i - 1]);
				}
			}
			
		}
	}

	$scope.addItem = function(type, items) {
		var item = {
			title: '',
			type: type,
			items: [],
			options: {}
		};

		if (!items) {
			
			$scope.items.push(item);
			$scope.currentItem = item;
			
			if ($scope.currentItem.type == 'setupCall') {
				$scope.currentItem.title = $scope.defCall;
			}
			
			if ($scope.currentItem.type == 'sendSms') {
				$scope.currentItem.title = $scope.defSmsMess;
				$scope.currentItem.destination = $scope.defSmsNo;
			}
			
		} else {
			var modalInstance = $modal.open({
				templateUrl: 'item-modal.html',
				controller: ['$scope', function($sscope) {
					
					$sscope.options = $scope.options;
					
					$sscope.addItem = function(type) {
						
						item.type = type;
						items.push(item);
						$scope.currentItem = item;
						
						if ($scope.currentItem.type == 'setupCall') {
							$scope.currentItem.title = $scope.defCall;
						}
						
						if ($scope.currentItem.type == 'sendSms') {
							$scope.currentItem.title = $scope.defSmsMess;
							$scope.currentItem.destination = $scope.defSmsNo;
						}
						modalInstance.close();
					}

					$sscope.close = function() {
						modalInstance.dismiss('cancel');
					}
				}],
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});
		}

	}
	
	$scope.editOption = function(option) {

	}

	$scope.removeOption = function(option, options) {
		var i = options.indexOf(option);
		options.splice(i, 1);
	}

	$scope.editOption = function(option) {
		var modalInstance = $modal.open({
			templateUrl: 'add-option-modal.html',
			controller: ['$scope', function($sscope) {
				$timeout(function() {
					$('.modal').addClass('modal-sm');
				});

				$sscope.item = angular.copy(option);

				$sscope.addOption = function() {
					option.options.key = $sscope.item.options.key;
					option.options.value = $sscope.item.options.value;
					option.branch = $sscope.item.branch;
					modalInstance.close();
				}

				$sscope.close = function() {
					modalInstance.dismiss('cancel');
				}
			}],
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});
	}

	$scope.addOption = function(items) {
		var modalInstance = $modal.open({
			templateUrl: 'add-option-modal.html',
			controller: ['$scope', function($sscope) {
				$timeout(function() {
					$('.modal').addClass('modal-sm');
				});

				$sscope.item = {};

				$sscope.addOption = function(is_more) {
					if (!$sscope.item.options || !$sscope.item.options.key || !$sscope.item.options.value) {
						alert('Please fill all required field!');
						return;
					}

					items.push({
						type: 'selectOption',
						options: {
							key:{
								value: $sscope.item.options.key.value
							},
							value: {
								value: $sscope.item.options.value.value
							},
						},
						branch: $sscope.item.branch,
						items:[]
					});

					if (is_more) {
						$sscope.item.options.key.value = "";
						$sscope.item.options.value.value = "";
						// Commented by Alp Sardag
						// $sscope.item.branch = false;
					}
					else {
						modalInstance.close();
					}
				}

				$sscope.close = function() {
					modalInstance.dismiss('cancel');
				}
			}],
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});
	}

	// Sortable options
	$scope.sortableOptions = {
		axis: 'y'
	};

	// Slider options
	// Slider options
	$scope.sliderOptions = {
		range: true,
		create: function(event, ui) {
			$(this).find('.ui-slider-handle').eq(0).append($('<span class="min"></span>'));
			$(this).find('.ui-slider-handle').eq(1).append($('<span class="max"></span>'));
			
			$(this).find(".min").html($(this).slider("values",0));
			$(this).find(".max").html($(this).slider("values",1));
		},
		slide: function(event, ui) {
			console.log("ui min : " + ui.values[0]);
			console.log("ui max : " + ui.values[1]);
			$(this).find(".min").html(ui.values[0]);
			$(this).find(".max").html(ui.values[1]);
		},
		change: function(event, ui) {
			console.log("ui min : " + ui.values[0]);
			console.log("ui max : " + ui.values[1]);
			$(this).find(".min").html(ui.values[0]);
			$(this).find(".max").html(ui.values[1]);
		}
	};

	$scope.exportJSON = function() {
		
		var jsonObj = {
				"id" : $scope.id, 
				"moduleName" : $scope.moduleName,
				"type" : $scope.type,
				"moduleId" :  $scope.moduleId,						
				"items" : $scope.items
			};
		var json = JSON.stringify(jsonObj);

		console.log(json);
		
		var url = $scope.url;
		var method = 'POST';
		if ($routeParams && $routeParams.itemId){
			url = url + "/" + $routeParams.itemId;
			method = 'PUT';
		}
        console.log(url); 
		
		$http({
  		    method: method,
  		    url: url,
  		    data: json,
  		    headers: {'Content-Type': 'application/json'}
  		}).
        success(function(data, status, headers, config) {
        		
        		var modalInstance = $modal.open({
        			templateUrl: 'item-test.html',
        			controller: ['$scope', function($sscope) {
        				$sscope.addItem = function(type) {
        					modalInstance.close();
        					$scope.items = [];
        				}

        				$sscope.close = function() {
        					modalInstance.dismiss('cancel');
        					$scope.items = [];
        				}
        				
        				$sscope.liveTest = function() {
        					url = '/api/dstkmoduletest/' + $scope.moduleName;
        					$http.get(url).success(function(data) {
        						window.alert("Alert", "Successfully sent");
        			  		}).
        			        success(function(data, status, headers, config) {
        			        	window.alert("Alert", "For service : " + $scope.moduleName + " sent operation completed to : 0532 349 58 05.");
        			        }).
        			        error(function(data, status, headers, config) {
        			        	window.alert("Alert", "Error, could not send campaign! status : " + status + ", reason " + data);
        			        });	
        					
        				}
        				
        			}]
        		});
        	
        }).
        error(function(data, status, headers, config) {
        	window.alert("Alert", "Service : " + $scope.serviceName + " error status : " + status + " " + data);
        });
		
	}


}]);

mainControllers.controller('schedulerController', ['$scope', '$modal', '$http', function($scope, $modal, $http) {

	$scope.$watch("optionUrl", function() {
    	
    	if ($scope.optionUrl){
			$http.get($scope.optionUrl).success(function(data) {
				$scope.options = data;
			});		
    	}
    	
    });
	
	 $scope.sendNow = function() {
			window.alert("Alert", "Sending...");
		}

		$scope.sendLater = function() {
	                
	                if ($scope.scheduleOption && $scope.scheduleOption==='later'){
	                    $scope.scheduleOption='now';
	                    }
	                else {
	                    $scope.scheduleOption='later';
	                    }
		}
	        
	        $scope.cancel = function() {
	                    $location.path($scope.path);
	        }
	
}]);





mainControllers.controller('campaignController', [ '$scope', '$timeout', '$location', '$filter', '$http', '$routeParams',
   		function($scope, $timeout, $location, $filter, $http, $routeParams) {

   			$scope.sent = 0;
                        $scope.delivered = 0;
   			$scope.blackListed = 0;
   			$scope.accepted = 0;

   			var cancelTimeout = 0;

   			var poll = function() {
                
                $scope.sent = parseInt($scope.sent) + 98;
                $scope.delivered = parseInt($scope.delivered) + 76;
                $scope.blackListed = parseInt($scope.blackListed) + 4;
                $scope.accepted = parseInt($scope.accepted) + 14;
                if (cancelTimeout == 1) {
                      return $timeout(poll, 300);
                }
           };

   			$scope.tabs = [ {
   				"label" : "ALL",
   				"url" : "/api/dstkcampaignstatistic/search?statusName=ALL&type=" + $routeParams.campaigntype
   			}, {
   				"label" : "ACTIVE",
   				"url" : "/api/dstkcampaignstatistic/search?statusName=ACTIVE&type=" + $routeParams.campaigntype
   			}, {
   				"label" : "PLANNED",
   				"url" : "/api/dstkcampaignstatistic/search?statusName=PLANNED&type=" + $routeParams.campaigntype
   			}, {
   				"label" : "COMPLETED",
   				"url" : "/api/dstkcampaignstatistic/search?statusName=COMPLETED&type=" + $routeParams.campaigntype
   			}];

   			$scope.selectedTab = $scope.tabs[0];
   			$scope.setSelectedTab = function(tab) {
   				$scope.selectedTab = tab;
   				$scope.url = tab.url;
   			}

   			$scope.tabClass = function(tab) {
   				if ($scope.selectedTab == tab) {
   					return "active";
   				} else {
   					return "";
   				}
   			}
   			
   			console.log($scope.url);
   			
   			$scope.$watch("url", function() {
   				$http.get($scope.url).success(function(data) {
   					$scope.items = data;

   					$scope.filteredItems = $scope.items;
   					$scope.removeItems = [];
   					$scope.itemsPerPage = 10;
   					$scope.pagedItems = [];
   					$scope.currentPage = 0;
   					$scope.query = "";

   					$scope.groupToPages();
   				});
   			});

   			$scope.url = $scope.selectedTab.url;
   			
   			// init
   			$scope.sort = {
   				sortingOrder : 'id',
   				reverse : false
   			};

   			$scope.getData = function(items, query) {
   				$scope.filteredItems = $filter('filter')(items,
   						query);
   				$scope.groupToPages();
   			};

   			// calculate page in place
   			$scope.groupToPages = function() {

   				$scope.currentPage = 0;
   				$scope.pagedItems = [];

   				for (var i = 0; i < $scope.filteredItems.length; i++) {
   					if (i % $scope.itemsPerPage === 0) {
   						$scope.pagedItems[Math.floor(i
   								/ $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
   					} else {
   						$scope.pagedItems[Math.floor(i
   								/ $scope.itemsPerPage)]
   								.push($scope.filteredItems[i]);
   					}
   				}
   			};

   			$scope.range = function(size) {

   				var ret = [];

   				for (var i = 0; i < size; i++) {
   					ret.push(i);
   				}

   				return ret;
   			};

   			$scope.prevPage = function() {
   				if ($scope.currentPage > 0) {
   					$scope.currentPage--;
   				}
   			};

   			$scope.nextPage = function() {
   				if ($scope.currentPage < $scope.pagedItems.length - 1) {
   					$scope.currentPage++;
   				}
   			};

   			$scope.setPage = function() {
   				$scope.currentPage = this.n;
   			};

   			$scope.putToRemoveBag = function(item) {

   				var i = $scope.removeItems.indexOf(item);
   				if (i >= 0) {
   					$scope.removeItems.splice(i, 1);
   					item.checked = " ";
   				} else {
   					$scope.removeItems.push(item);
   					item.checked = "checked";
   				}

   			};

   			$scope.deleteAll = function() {
   				if (confirm("Are you sure?")) {
   					for (var i = 0; i < $scope.removeItems.length; i++) {
   						var j = $scope.filteredItems
   								.indexOf($scope.removeItems[i]);
   						$scope.filteredItems.splice(j, 1);
   					}
   				}
   				$scope.removeItems = [];
   				$scope.groupToPages();
   			};

   			$scope.editItem = function(item) {
   				
   				console.log(item);
   				
   				if (item.statusName === "ACTIVE") {
   					$scope.total = item.total;
                                        $scope.sent = item.sent;
   					$scope.delivered = item.delivered;
   					$scope.blackListed = item.blackListed;
                                        $scope.accepted = item.accepted;	
                    cancelTimeout = 1;
                   
                    $timeout(poll,300);
                    
	              } else {
	                    cancelTimeout = 0;
	                        
	                    $timeout(function() {
   					$scope.total = item.total;
                                        $scope.sent = item.sent;
   					$scope.delivered = item.delivered;
   					$scope.blackListed = item.blackListed;
                                        $scope.accepted = item.accepted;	
	                             }, 400);
	              }   				

   			}

   			$scope.openScheduler = function() {
   				if ($routeParams.campaigntype === 'service') {
   					$location.path('/scheduler');
   				} else {
   					$location.path('/schedulermenu');
   				}

   			}

   		} ]);

   mainControllers.controller('executiveReportController',
   		[
   				'$scope',
   				'$timeout',
   				function($scope, $timeout) {
   					
   					$scope.tabs = [ {
   						"label" : "SIM Card User Report"
   					}, {
   						"label" : "System Usage Report"
   					}, {
   						"label" : "Campaign Acceptance Report"
   					} ];
   					
   					$scope.table = [ {
   						"label" : "SIM CARD TYPE"
   					}, {
   						"label" : "SUBSCRIBERS"
   					}, {
   						"label" : "STATUS"
   					} ];
   					
   					$scope.sims = [ {
   						"type" : "Standart Java SIM",
   						"subscribers" : "2,543,788",
   						"status" : "+5,206",
   						"statusClass" : "growth-status-high"
   					}, {
   						"type" : "M2M Java SIM",
   						"subscribers" : "321,734",
   						"status" : "+370",
   						"statusClass" : "growth-status-high"
   					}, {
   						"type" : "NFC Java SIM",
   						"subscribers" : "16,365",
   						"status" : "0",
   						"statusClass" : "growth-status-average"
   					}, {
   						"type" : "Native SIM",
   						"subscribers" : "1,023,534",
   						"status" : "-3,103",
   						"statusClass" : "growth-status-low"
   					}];

   					$scope.myModelDonut = [ {
   						label : 'Native',
   						value : 26.2
   					}, {
   						label : 'Standart',
   						value : 65.1
   					}, {
   						label : 'M2M',
   						value : 8.2
   					}, {
   						label : 'NFC',
   						value : 0.4
   					} ];

   					$scope.flotData = {
   							
   							"data" : [
                                                                                                {
   													data : [[1, 789345], [2, 845032], [3, 932023], [4, 1012932],[5, 1002344],[6, 934524],[7,834253],[8, 703456],[9, 845603],[10, 983456],[11, 1156784],[12, 1321468]],
   													label : 'Rejected Campaigns'
   												},
   												{
   													data : [[1, 202354], [2, 223535], [3, 218074], [4, 273932],[5, 282532],[6, 210524],[7,199253],[8, 183653],[9, 200360],[10, 210224],[11, 224684],[12, 248754]],
   													label : 'Accepted Campaigns'
   												} ],
   									
   							"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
   													 
   					}
   					
   					$scope.monthStatus = 'active';
   					
   					$scope.changeToDate = function(period){
   						$scope.dayStatus = '';
   						$scope.monthStatus = '';
   						$scope.yearStatus = '';
   						
   						if (period === 'day'){
   							
   							$scope.dayStatus = 'active';
   							
   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 43567], [2, 38543], [3, 89345], [4, 46321],[5, 41085],[6, 39294],[7, 52034]],
   													label : 'Rejected Campaigns'
   												},
   												{
   													data : [[1, 10213], [2, 9453], [3, 18243], [4, 14322],[5, 9873],[6, 8454],[7, 17343]],
   													label : 'Accepted Campaigns'
   												} ],
   											
   									"ticks" : [[1, "05-05"], [2, "05-06"], [3, "05-07"], [4,"05-08"], [5,"05-09"], [6,"05-10"], [7,"05-11"]]
   															 
   							}
   							
   							
   						} else if (period === 'month'){

   							$scope.monthStatus = 'active';

   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 789345], [2, 845032], [3, 932023], [4, 1012932],[5, 1002344],[6, 934524],[7,834253],[8, 703456],[9, 845603],[10, 983456],[11, 1156784],[12, 1321468]],
   													label : 'Rejected Campaigns'
   												},
   												{
   													data : [[1, 202354], [2, 223535], [3, 218074], [4, 273932],[5, 282532],[6, 210524],[7,199253],[8, 183653],[9, 200360],[10, 210224],[11, 224684],[12, 248754]],
   													label : 'Accepted Campaigns'
   												} ],
   											
   									"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
   															 
   							}
   											 
   							
   						} else {
   							
   							$scope.yearStatus = 'active';
   							
   							$scope.flotData = {
   									
   									"data" : [
   												{
   													data : [[1, 4536748],[2, 6036353],[3, 6536370], [4, 9347248], [5, 14232734]],
   													label : 'Rejected Campaigns'
   												},
   												{
   													data : [[1, 1702374],[2, 1944374],[3, 2023423], [4,2567543 ], [5, 3245746]],
   													label : 'Accepted Campaigns'
   												} ],
   											
   									"ticks" :  [[1, "2009"], [2, "2010"], [3, "2011"], [4, "2012"], [5, "2013"]]
   							}
   							
   						}
   					
   					}
   					
   					
   					$scope.taxData = [
   									{"period": "2014-05-05", "sent": 873465, "delivered": 655098},
   									{"period": "2014-05-06", "sent": 765609, "delivered": 574206},
   									{"period": "2014-05-07", "sent": 981276, "delivered": 735957},
   									{"period": "2014-05-08", "sent": 734651, "delivered": 550998},
   									{"period": "2014-05-09", "sent": 1209348, "delivered": 907011},
   									{"period": "2014-05-10", "sent": 1376451, "delivered": 1032338},
   									{"period": "2014-05-11", "sent": 510928, "delivered": 383196}
   					];					

   					$scope.xkeyLine = 'period';
   					$scope.ykeysLine = [ 'sent', 'delivered' ];
   					$scope.labelsLine = [ 'Sent Messages', 'Delivered Messages' ];

   					$scope.selectedTab = $scope.tabs[0];
   					$scope.setSelectedTab = function(tab) {
   						$scope.selectedTab = tab;
   					}

   					$scope.tabClass = function(tab) {
   						if ($scope.selectedTab == tab) {
   							return "active";
   						} else {
   							return "";
   						}
   					}

   				} ]);
   
   
   mainControllers.controller('executiveReportControllerSTUP',
	   		[
	   				'$scope',
	   				'$timeout',
	   				function($scope, $timeout) {
	   					
                                                $scope.today = new Date();
                                                
                                                $scope.tabs = [ {
	   						"label" : "Overview"
	   					}, {
	   						"label" : "Transaction Numbers Report"
	   					}, {
	   						"label" : "Campaign Results Report"
	   					} ];
                                                
                                                
                                                $scope.tableHeader = [ {
	   						"label" : new Date()
	   					}, {
	   						"label" : "VALUE"
	   					}, {
	   						"label" : "DAILY CHANGE"
	   					} ];
                                                
                                                
                                                $scope.tableContents = [ {
	   						"column1" : "Number of Users",
	   						"column2" : "273,788",
	   						"column3" : "+6,271",
	   						"column4" : "growth-status-high"
	   					}, {
	   						"column1" : "Number of Transactions",
	   						"column2" :"301,734",
	   						"column3" : "+7,953",
	   						"column4" : "growth-status-high"
	   					}, {
	   						"column1" :"Amount of Transactions",
	   						"column2" : "$4,352,956",
	   						"column3" : "-$5,433",
	   						"column4" :"growth-status-low"
	   					} ];
                                            

                                                $scope.table2Contents = {
	   						"column1" : "TOTAL NUMBER OF SUBSCRIBERS:",
	   						"column2" : "868,934",
	   						"column3" : "+6,832",
	   						"column4" : "growth-status-high"
	   					};

	   					$scope.myModelDonut = [ {
	   						label : '12 USD',
	   						value : 30.2
	   					}, {
	   						label : '15 USD',
	   						value : 45.1
	   					}, {
	   						label : '20 USD',
	   						value : 13.3
	   					}, {
	   						label : '25 USD',
	   						value : 11.4
	   					} ];

                                                $scope.taxData = [
	   									{"period": "2014-05-16", "total": 199654, "successful": 141734},
	   									{"period": "2014-05-17", "total": 245145, "successful": 203181},
	   									{"period": "2014-05-18", "total": 275456, "successful": 215324},
	   									{"period": "2014-05-19", "total": 291654, "successful": 237543},
	   									{"period": "2014-05-20", "total": 249423, "successful": 189645},
	   									{"period": "2014-05-21", "total": 239234, "successful": 192323},
	   									{"period": "2014-05-22", "total": 347654, "successful": 301734}
	   					];	
                                                
                                                
                                                $scope.xkeyLine = 'period';
	   					$scope.ykeysLine = [ 'total', 'successful' ];
	   					$scope.labelsLine = [ 'Sent Messages', 'Delivered Messages' ];

	   					$scope.selectedTab = $scope.tabs[0];
	   					$scope.setSelectedTab = function(tab) {
	   						$scope.selectedTab = tab;
	   					}
                                                
                                                $scope.flotData = {
	   							
	   							"data" : [
	                                                                                                {
	   													data : [[1, 789345], [2, 845032], [3, 932023], [4, 1012932],[5, 1002344],[6, 934524],[7,834253],[8, 703456],[9, 845603],[10, 983456],[11, 1156784],[12, 1321468]],
	   													label : 'Rejected Campaigns'
	   												},
	   												{
	   													data : [[1, 202354], [2, 223535], [3, 218074], [4, 273932],[5, 282532],[6, 210524],[7,199253],[8, 183653],[9, 200360],[10, 210224],[11, 224684],[12, 248754]],
	   													label : 'Accepted Campaigns'
	   												} ],
	   									
	   							"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
	   													 
	   					}
	   					
	   					$scope.monthStatus = 'active';
	   					
	   					$scope.changeToDate = function(period){
	   						$scope.dayStatus = '';
	   						$scope.monthStatus = '';
	   						$scope.yearStatus = '';
	   						
	   						if (period === 'day'){
	   							
	   							$scope.dayStatus = 'active';
	   							
	   							$scope.flotData = {
	   									
	   									"data" : [
	   												{
	   													data : [[1, 43567], [2, 38543], [3, 89345], [4, 46321],[5, 41085],[6, 39294],[7, 52034]],
	   													label : 'Rejected Campaigns'
	   												},
	   												{
	   													data : [[1, 10213], [2, 9453], [3, 18243], [4, 14322],[5, 9873],[6, 8454],[7, 17343]],
	   													label : 'Accepted Campaigns'
	   												} ],
	   											
	   									"ticks" : [[1, "05-05"], [2, "05-06"], [3, "05-07"], [4,"05-08"], [5,"05-09"], [6,"05-10"], [7,"05-11"]]
	   															 
	   							}
	   							
	   							
	   						} else if (period === 'month'){

	   							$scope.monthStatus = 'active';

	   							$scope.flotData = {
	   									
	   									"data" : [
	   												{
	   													data : [[1, 789345], [2, 845032], [3, 932023], [4, 1012932],[5, 1002344],[6, 934524],[7,834253],[8, 703456],[9, 845603],[10, 983456],[11, 1156784],[12, 1321468]],
	   													label : 'Rejected Campaigns'
	   												},
	   												{
	   													data : [[1, 202354], [2, 223535], [3, 218074], [4, 273932],[5, 282532],[6, 210524],[7,199253],[8, 183653],[9, 200360],[10, 210224],[11, 224684],[12, 248754]],
	   													label : 'Accepted Campaigns'
	   												} ],
	   											
	   									"ticks" : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4,"APR"], [5,"MAY"], [6,"JUN"], [7,"JUL"], [8,"AUG"], [9,"SEP"], [10,"OCT"], [11,"NOV"], [12,"DEC"]]
	   															 
	   							}
	   											 
	   							
	   						} else {
	   							
	   							$scope.yearStatus = 'active';
	   							
	   							$scope.flotData = {
	   									
	   									"data" : [
	   												{
	   													data : [[1, 4536748],[2, 6036353],[3, 6536370], [4, 9347248], [5, 14232734]],
	   													label : 'Rejected Campaigns'
	   												},
	   												{
	   													data : [[1, 1702374],[2, 1944374],[3, 2023423], [4,2567543 ], [5, 3245746]],
	   													label : 'Accepted Campaigns'
	   												} ],
	   											
	   									"ticks" :  [[1, "2009"], [2, "2010"], [3, "2011"], [4, "2012"], [5, "2013"]]
	   							}
	   							
	   						}
	   					
	   					}
	   					

	   					$scope.tabClass = function(tab) {
	   						if ($scope.selectedTab == tab) {
	   							return "active";
	   						} else {
	   							return "";
	   						}
	   					}

	   				} ]);
   
   mainControllers
   		.controller(
   				'tableController',
   				[
   						'$scope',
   						'$location',
   						'$filter',
   						'$http',
   						function($scope, $location, $filter, $http) {

   							$scope.url = new Date().getTime();
   							
   							$scope.$watch("url", function() {
   								$http.get($scope.url).success(function(data) {
   									$scope.items = data;

   									$scope.filteredItems = $scope.items;
   									$scope.removeItems = [];
   									$scope.itemsPerPage = 10;
   									$scope.pagedItems = [];
   									$scope.currentPage = 0;
   									$scope.query = "";

   									$scope.groupToPages();
   								});
   							});

   							// init
   							$scope.sort = {
   								sortingOrder : 'id',
   								reverse : false
   							};

   							$scope.getData = function(items, query) {
   								$scope.filteredItems = $filter('filter')(items,
   										query);
   								$scope.groupToPages();
   							};

   							// calculate
							// page in
							// place
   							$scope.groupToPages = function() {

   								$scope.currentPage = 0;
   								$scope.pagedItems = [];

   								for (var i = 0; i < $scope.filteredItems.length; i++) {
   									if (i % $scope.itemsPerPage === 0) {
   										$scope.pagedItems[Math.floor(i
   												/ $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
   									} else {
   										$scope.pagedItems[Math.floor(i
   												/ $scope.itemsPerPage)]
   												.push($scope.filteredItems[i]);
   									}
   								}
   							};

   							$scope.range = function(size) {

   								var ret = [];

   								for (var i = 0; i < size; i++) {
   									ret.push(i);
   								}

   								return ret;
   							};

   							$scope.prevPage = function() {
   								if ($scope.currentPage > 0) {
   									$scope.currentPage--;
   								}
   							};

   							$scope.nextPage = function() {
   								if ($scope.currentPage < $scope.pagedItems.length - 1) {
   									$scope.currentPage++;
   								}
   							};

   							$scope.setPage = function() {
   								$scope.currentPage = this.n;
   							};

   							$scope.putToRemoveBag = function(item) {

   								var i = $scope.removeItems.indexOf(item);
   								if (i >= 0) {
   									$scope.removeItems.splice(i, 1);
   									item.checked = " ";
   								} else {
   									$scope.removeItems.push(item);
   									item.checked = "checked";
   								}

   							};

   							$scope.deleteAll = function() {
   								if (confirm("Are you sure?")) {
   									
   									var ids = ""; 
   									
   									for (var i = 0; i < $scope.removeItems.length; i++) {
   										var j = $scope.filteredItems.indexOf($scope.removeItems[i]);
   										$scope.filteredItems.splice(j, 1);
   										ids = ids + "ids=" + $scope.removeItems[i].id + "&";  
   									}
   									
   									var url = $scope.url  + "?" + ids;
   									
   									// Delete
   									if (ids){
   										$http.delete(url).success(function(data) {
   											$scope.item = data;
   										});										
   									}
   									
   									
   								}
   								$scope.removeItems = [];
   								$scope.groupToPages();
   							};

   							$scope.editItem = function(item) {

   								$location.path($scope.path + item.id);

   							}

   							$scope.addItem = function() {
   								
   								$location.path($scope.path + 'add*?#');

   							}
   							
   							$scope.editService = function(item) {
   								
   						       console.log(item);
   						       $location.path('/interactifdesignstuprule/' + item.moduleName);    
   						
   						   }


   						} ]);


   
   mainControllers.controller('detailController', [ '$scope', '$http', '$routeParams',
    		'$location', function($scope, $http, $routeParams, $location) {

    			
				$scope.$watch("optionUrl", function() {
			    	
			    	if ($scope.optionUrl){
						$http.get($scope.optionUrl).success(function(data) {
							$scope.options = data;
						});		
			    	}
			    	
			    });
    				
    	
    			if ($routeParams.itemId === "add*?#") {
    				$scope.screen = {
    					"operation" : "add",
    					"header" : "Add",
    					"button" : "Add"
    				};
    				$scope.item = {};
    			} else {
    				$scope.screen = {
    					"operation" : "edit",
    					"header" : "Edit",
    					"button" : "Update"
    				};

           			$scope.$watch("url", function() {
           				
           				if ($scope.url){
           					
               				var url = $scope.url  + $routeParams.itemId;
            				
               				$http.get(url).success(function(data) {
               					$scope.item = data;

               				});

           				}
           				
           			});    				
    				
    			}
    			
    			$scope.add = function() {

    				var json = JSON.stringify($scope.item);

    				var url = $scope.url;
    				
    				// Save
    				$http.post(url, json).success(function(data) {
    					$scope.item = data;
    					$location.path($scope.path);
    				})
    				.error(function(data, status, headers, config) {
    		        	
    					window.alert("Alert", "errors : " + data.errors);
    					
    		        });
    				
    			}

    			$scope.edit = function() {

    				var json = JSON.stringify($scope.item);

    				var url = $scope.url;
    				url = url + $scope.item.id;
    				
    				// Update
    				$http.put(url, json).success(function(data) {
    					
    					$scope.item = data;
    					
    					$location.path($scope.path);
    				})
    		        .error(function(data, status, headers, config) {
    		        	
    					window.alert("Alert", "errors : " + data.errors);
    					
    		        });
    				
    			}

    			$scope.cancel = function() {
    				$location.path($scope.path);
    			}

    		} ]);   
   
   mainControllers.controller('themeController', [ '$scope', '$http', '$routeParams',
                                            		'$location', function($scope, $http, $routeParams, $location) {

                                            			
													   $scope.$watch("optionUrl", function() {
													    	
													    	if ($scope.optionUrl){
																$http.get($scope.optionUrl).success(function(data) {
																	$scope.item = data;
																});		
													    	}
													    	
													    });
                                            				
                                            	
                                            			if ($routeParams.itemId === "add*?#") {
                                            				$scope.screen = {
                                            					"operation" : "add",
                                            					"header" : "Add",
                                            					"button" : "Add"
                                            				};
                                            				$scope.item = {};
                                            			} else {
                                            				$scope.screen = {
                                            					"operation" : "edit",
                                            					"header" : "Edit",
                                            					"button" : "Update"
                                            				};

                                                   			$scope.$watch("url", function() {
                                                   				
                                                   				if ($scope.url){
                                                   					
                                                       				var url = $scope.url  + $routeParams.itemId;
                                                    				
                                                       				$http.get(url).success(function(data) {
                                                       					$scope.item = data;

                                                       				});

                                                   				}
                                                   				
                                                   			});    				
                                            				
                                            			}
                                            			
                                            			$scope.getTheme=function(item){
                                            				var url = $scope.url  + "search?id=" + $scope.item.id;
                                            				
                                               				$http.get(url).success(function(data) {
                                               					$scope.item = data;

                                               				});
                                            			}  
                                            			
                                            			$scope.add = function() {

                                            				var json = JSON.stringify($scope.item);

                                            				var url = $scope.url;
                                            				
                                            				// Save
                                            				$http.post(url, json).success(function(data) {
                                            					$scope.item = data;
                                            					$location.path($scope.path);
                                            				}).success(function(data, status, headers, config) {
                                            			        window.alert("Alert", "Successfully saved");
                                            			    }).error(function(data, status, headers, config) {
                                            					window.alert("Alert", "errors : " + data.errors);
                                            		        });
                                            				
                                            			}

                                            			$scope.edit = function() {

                                            				var json = JSON.stringify($scope.item);

                                            				var url = $scope.url;
                                            				url = url + $scope.item.id;
                                            				
                                            				// Update
                                            				$http.put(url, json).success(function(data) {
                                            					
                                            					$scope.item = data;
                                            					
                                            					$location.path($scope.path);
                                            				}).success(function(data, status, headers, config) {
                                            			        window.alert("Alert", "Successfully saved");
                                            			    }).error(function(data, status, headers, config) {
                                            					window.alert("Alert", "errors : " + data.errors);
                                            		        });
                                            				
                                            			}

                                            			$scope.cancel = function() {
                                            				$location.path($scope.path);
                                            			}

                                            		} ]);   