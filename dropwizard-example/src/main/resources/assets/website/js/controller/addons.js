mainControllers.controller('reportController', [ '$scope', '$timeout', '$location', '$filter', '$http', '$routeParams', '$modal',
function($scope, $timeout, $location, $filter, $http, $routeParams, $modal) {

$scope.openExampleModal = function() {
	
	var ModalInstanceCtrl = function ($scope, $modalInstance) {
		  $scope.close = function () {
		    $modalInstance.dismiss('cancel');
		  };
	};
	
	var modalInstance = $modal.open({
		templateUrl: 'item-design.html',
		controller: ModalInstanceCtrl,
		windowClass : 'app-modal-window'
	});
	
}	
	
	
$scope.setSelectedTab = function(tab) {
    $scope.selectedTab = tab;
    $scope.url = tab.url;
}

$scope.tabClass = function(tab) {
    if ($scope.selectedTab == tab) {
        return "active";
} else {
    return "";
    }
}

// init
$scope.sort = {
    sortingOrder : 'id',
    reverse : false
};

$scope.getData = function(items, query) {
    $scope.filteredItems = $filter('filter')(items,
        query);
    $scope.groupToPages();
};

// calculate page in
// place
$scope.groupToPages = function() {

    $scope.currentPage = 0;
    $scope.pagedItems = [];

    for (var i = 0; i < $scope.filteredItems.length; i++) {
        if (i % $scope.itemsPerPage === 0) {
            $scope.pagedItems[Math.floor(i
                / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
        } else {
            $scope.pagedItems[Math.floor(i
                / $scope.itemsPerPage)]
                .push($scope.filteredItems[i]);
        }
    }
};

$scope.range = function(size) {

    var ret = [];

    for (var i = 0; i < size; i++) {
        ret.push(i);
    }

    return ret;
};

$scope.prevPage = function() {
    if ($scope.currentPage > 0) {
        $scope.currentPage--;
    }
};

$scope.nextPage = function() {
    if ($scope.currentPage < $scope.pagedItems.length - 1) {
        $scope.currentPage++;
    }
};

$scope.setPage = function() {
    $scope.currentPage = this.n;
};

$scope.putToRemoveBag = function(item) {

    var i = $scope.removeItems.indexOf(item);
    if (i >= 0) {
        $scope.removeItems.splice(i, 1);
        item.checked = " ";
} else {
    $scope.removeItems.push(item);
    item.checked = "checked";
    }

};

$scope.deleteAll = function() {
    if (confirm("Are you sure?")) {
        for (var i = 0; i < $scope.removeItems.length; i++) {
            var j = $scope.filteredItems
                .indexOf($scope.removeItems[i]);
            $scope.filteredItems.splice(j, 1);
        }
    }
    $scope.removeItems = [];
    $scope.groupToPages();
};

$scope.editItem = function(item) {

    console.log(item);
    $location.path($scope.selectedTab.path + '/' + item.id);    

}

$scope.changeStatus = function() {
            if ($scope.blackList && $scope.blackList==='changed'){
	                    $scope.blackList='same';
	                    }
            else {
	                    $scope.blackList='changed';
	                    }	                
}

$scope.save = function() {
	                
            $scope.blackList='saved';
}

$scope.openDesignScreen = function() {

	   $location.path($scope.selectedTab.path);

	}

} ]);

