/* App Module */
var mainApp = angular.module('mainApp', [ 'ui.sortable', 'ui.bootstrap.tpls', 'ui.bootstrap.modal', 'ui.select2', 'ui.slider', 'ui.utils', 'ngRoute', 'mainControllers' ]);

mainApp.config([ '$routeProvider', function($routeProvider) {
	
	$routeProvider.when('/home', {
		templateUrl : 'executivereportstup.html',
		controller : 'executiveReportControllerSTUP'
	}).when('/stupruletable', {
		templateUrl : 'stupruletable.html',
		controller : 'tableController'
	}).when('/stuprule/:itemId', {
		templateUrl : 'stupruledetail.html',
		controller : 'detailController'
	}).when('/campaign/search/:campaigntype', {
		templateUrl : 'campaign.html',
		controller : 'campaignController'
	}).when('/scheduler', {
		templateUrl : 'scheduler.html',
		controller : 'schedulerController'
	}).when('/scheduler/:itemId', {
		templateUrl : 'scheduler.html',
		controller : 'schedulerController'
	}).when('/stupcampaignreports', {
		templateUrl : 'stupCampaignReports.html',
		controller : 'reportController'
	}).when('/interactifdesign', {
		templateUrl : 'interactifdesignstup.html',
		controller : 'interactifDesignController'
	}).when('/interactifdesign/:itemId', {
		templateUrl : 'interactifdesign.html',
		controller : 'interactifDesignController'
	}).when('/interactifdesignstuprule/:itemId', {
		templateUrl : 'interactifdesignstuprule.html',
		controller : 'interactifDesignController'
	}).when('/menudesign', {
		templateUrl : 'menudesign.html',
		controller : 'menuDesignController'
	}).when('/menudesign/:itemId', {
		templateUrl : 'menudesign.html',
		controller : 'menuDesignController'
	}).when('/instancedesign', {
		templateUrl : 'instancedesign.html',
		controller : 'instanceDesignController'
	}).when('/instancedesign/:itemId', {
		templateUrl : 'instancedesign.html',
		controller : 'instanceDesignController'
	}).when('/designstable', {
		templateUrl : 'designstable.html',
		controller : 'designsTableController'
	}).when('/stupcode', {
		templateUrl : 'stupCodeConfiguration.html',
		controller : 'detailController'
        }).when('/stupsecuritysettings', {
		templateUrl : 'stupSecuritySettings.html',
		controller : 'detailController'
        }).
        
    otherwise({
        redirectTo: '/home'
    });
} ]);

mainApp.directive('knobSent', function() {
	return {
		require : 'ngModel',
		scope : {
			model : '=ngModel'
		},
		controller : function($scope, $element, $timeout) {
			var el = $($element);
			$scope.$watch('model', function(v) {
				var el = $($element);
				el.val(v).trigger('change');
				console.log("Scope parent total : " + $scope.$parent.total);
				if ($scope.$parent.total) {
					el.trigger('configure', {"max" : $scope.$parent.total});
				}
			});
		},

		link : function($scope, $element, $attrs, $ngModel) {
			var el = $($element);
			el.val($scope.value).knob({
				'change' : function(v) {
					$scope.$apply(function() {
						$ngModel.$setViewValue(v);
					});
				}
			});
		}
	}

});

mainApp.directive('donutChart', function() {

	return {

		// required to make it work as an element
		restrict : 'E',
		template : '<div></div>',
		replace : true,
		// observe and manipulate the DOM
		link : function($scope, element, attrs) {

			var data = $scope[attrs.data];

			// Morris Donut Chart
			Morris.Donut({
				element : element,
				data : data,
				labelColor : '#59839f',
				colors : [ "#59839f", "#9db7c9", "#d7dfec", "#e6e6e7" ],
				formatter : function(y) {
					return "%" + y
				}
			});
		}

	};

});

mainApp
		.directive(
				'lineChart',
				function() {

					return {

						// required to make it work as an element
						restrict : 'E',
						template : '<div></div>',
						replace : true,
						// observe and manipulate the DOM
						link : function($scope, element, attrs) {

							var data = $scope[attrs.data], xkey = $scope[attrs.xkey], ykeys = $scope[attrs.ykeys], labels = $scope[attrs.labels];

							// Morris Line Chart
							Morris.Line({
								element : element,
								data : data,
								xkey : xkey,
								xLabels : 'day',
								ykeys : ykeys,
								labels : labels
							});
						}

					};

				});


var chart = null, opts = {
		series : {
			lines : {
				show : true,
				lineWidth : 1,
				fill : true,
				fillColor : {
					colors : [ {
						opacity : 0.05
					}, {
						opacity : 0.09
					} ]
				}
			},
			points : {
				show : true,
				lineWidth : 2,
				radius : 3
			},
			shadowSize : 0,
			stack : true
		},
		grid : {
			hoverable : true,
			clickable : true,
			tickColor : "#f9f9f9",
			borderWidth : 0
		},
		legend : {
			// show: false
			labelBoxBorderColor : "#fff"
		},
		colors : [ "#a7b5c5", "#30a0eb" ],
		xaxis : {
			ticks : [],
			font : {
				size : 12,
				family : "Open Sans, Arial",
				variant : "small-caps",
				color : "#9da3a9"
			}
		},
		yaxis : {
			ticks : 3,
			tickDecimals : 0,
			font : {
				size : 12,
				color : "#9da3a9"
			}
		}
	};

mainApp.directive('flotChart', function() {
	return {
		restrict : 'E',
		template : '<div></div>',
		replace : true,
		link : function($scope, elem, attrs) {
			
			elem.bind("plothover", function (event, pos, item) {
				
				console.log("item : " + item);
				
    			if (item) {
    				if (previousPoint != item.dataIndex) {
    					previousPoint = item.dataIndex;

    					$("#tooltip").remove();
    					var x = item.datapoint[0].toFixed(0),
    						y = item.datapoint[1].toFixed(0);

    					var month = item.series.xaxis.ticks[item.dataIndex].label;

    					showTooltip(item.pageX, item.pageY,
    								item.series.label + " of " + month + ": " + y);
    				}
    			}
    			else {
    				$("#tooltip").remove();
    				previousPoint = null;
    			}
    		});

			opts.xaxis.ticks = attrs.ngModel.ticks;
			
			$scope.$watch(attrs.ngModel, function(v) {
			
				opts.xaxis.ticks = v.ticks;
				chart = $.plot(elem, v.data, opts);
				elem.show();
			});
			
		}
	};
});

var mainControllers = angular.module('mainControllers', []);

mainApp.directive("customSort", function() {
	return {
		restrict : 'A',
		transclude : true,
		scope : {
			order : '=',
			sort : '='
		},
		template : ' <a ng-click="sort_by(order)">'
				+ '    <span ng-transclude></span>'
				+ '    <i ng-class="selectedCls(order)"></i>' + '</a>',
		link : function(scope) {

			// change sorting order
			scope.sort_by = function(newSortingOrder) {
				var sort = scope.sort;

				if (sort.sortingOrder == newSortingOrder) {
					sort.reverse = !sort.reverse;
				}

				sort.sortingOrder = newSortingOrder;
			};

			scope.selectedCls = function(column) {
				if (column == scope.sort.sortingOrder) {
					return ('icon-chevron-' + ((scope.sort.reverse) ? 'down'
							: 'up'));
				} else {
					return 'icon-sort'
				}
			};
		}// end link
	}
});

mainApp.factory('alert', ['$compile', '$rootScope', '$timeout', '$animate', function($compile, $rootScope, $timeout, $animate) {
	var currentAlert = {};
	return function(title, message) {
		var scope = $rootScope.$new();

		scope.message = message;
		scope.title = title;

		if (currentAlert.close)
			currentAlert.close();

		var alert = $($('#alert-box').html());
		$compile(alert.contents())(scope);

		$('body').append(alert);
		$timeout(function() {
			alert.addClass('in');
		});

		currentAlert.close = scope.close = function() {
			alert.removeClass('in');
			$timeout(function() {
				alert.remove();
			}, 2500);
		}
    }
}]);

//Alert
window.alert = function(title, message) {
  var elem = angular.element(document.querySelector('[ng-controller]'));
  injector = elem.injector();
  var alert = injector.get('alert');
  alert(title, message);
}


//Add / Edit item modal
mainApp.directive('addEditModal', ['$animate', '$timeout', '$compile', '$rootScope', '$document', '$controller', '$window', function($animate, $timeout, $compile, $rootScope, $document, $controller, $window) {
	var currentModal = {};
	$document.click(function() {
		if (currentModal.close) {
			currentModal.close();
		}
	});

	return {
		scope: {
			items: '=',
			options: '@',
			item: '='
		},
		link: function(scope, element, attrs) {
			element.click(function(e) {

				console.log(scope.menus);
				console.log(JSON.stringify(scope.menus));
				console.log(JSON.stringify(scope.items));
				e.stopPropagation();
				if (element.hasClass('active')) {
					return;
				}

				element.addClass('active');

				if (currentModal.remove) {
					currentModal.close();
				}

				// Create modal position
				var template = $(attrs.template  || '#editModal').html();
				currentModal = angular.element(template);

				currentModal.find('.arrow').css({
					'top': element.offset().top-$($window).scrollTop()
				});

				if(attrs.addEditModal != 'static')
					currentModal.css({
						left:element.offset().left+element.width()+10,
						top: element.offset().top-25
					});

				currentModal.click(function(e){
					e.stopPropagation();
				});

				// Scope
				var _scope = $rootScope.$new(true);
				$compile(currentModal)(_scope);
				
				_scope._item = {};
				if (scope.item) {
					_scope._item = angular.copy(scope.item)
					_scope._edit = true;
				}
				if (scope.items) {
					_scope.items = scope.items;
					_scope._edit = false;
				}
				if (scope.options) {
					_scope.options = JSON.parse(scope.options);
					console.log("root modal options : " + scope.options);
					console.log("scope modal modules : " + _scope.options);
				}
				_scope.add = function() {
					if (!_scope._item.title && !_scope._item.function)
						return window.alert('Alert', 'Please select/fill any field!');
					if (!_scope._item.items)
						_scope._item.items = [];
					scope.items.push(_scope._item);
					currentModal.close();
				}

				_scope.edit = function() {
					if (!_scope._item.title && !_scope._item.function)
						return window.alert('Alert', 'Please select/fill any field!');
					
					angular.forEach(_scope._item, function(el, i) {
						if (i.substr(0, 2) != '$$') {
							console.log(i);
							scope.item[i] = _scope._item[i];
						}
					});

					currentModal.close();
				}

				_scope.close = currentModal.close = function() {
					currentModal.remove();
					currentModal = {};
					element.removeClass('active');
				}

				// Controller For Modal
				var ctrlLocals = {};
				ctrlLocals.$scope = _scope;
				$controller(['$scope', function($scope) {

				}], ctrlLocals);


				$document.find('body').append(currentModal);
				$timeout(function(){
					$animate.addClass(currentModal, 'open in');
				});

				e.stopPropagation();
			});
		}
	}
}]);

mainApp.directive('designPreview', ['$animate', '$timeout', '$compile', '$rootScope', '$document', '$window', function($animate, $timeout, $compile, $rootScope, $document, $window) {
	return {
		scope: {
			data: '='
		},
		link: function(scope, element, attrs) {
			element.click(function() {
				var modal = angular.element($('#preview-modal').html());
				var _scope = $rootScope.$new(true);

				// Set scope
				_scope.data = angular.copy(scope.data);
				_scope.items = angular.copy(scope.data);
				_scope.currentItem = _scope.items[0];
				_scope.selectedItems = [];

				_scope.close = function() {
					$animate.removeClass(modal, 'open in', function() {
						$timeout(function() {
							modal.remove();
							_scope.$destroy();
						}, 1500);
					});
				}

				_scope.next = function() {
					var i = _scope.items.indexOf(_scope.currentItem);
					if (i<0) i=0;

					if (_scope.items[i+1]) {

						if (_scope.currentItem.type !== "selectItem")
							_scope.selectedItems.push(_scope.currentItem);

						_scope.currentItem = _scope.items[i+1];
					} else 
						_scope.close();
				}

				_scope.hasPrev = function() {
					return _scope.items.indexOf(_scope.currentItem) > 0 || (_scope.parentItems && _scope.parentItems.length);
				}

				_scope.prev = function() {
					var i = _scope.items.indexOf(_scope.currentItem);
					if (_scope.items[i-1])
						_scope.currentItem = _scope.items[i-1];
					else if(_scope.parentItems) {
						_scope.items = _scope.parentItems;
						_scope.currentItem = _scope.parentItem;
						delete _scope.parentItems;
						delete _scope.parentItem;
					} else 
						_scope.close();
					_scope.selectedItems.pop(_scope.selectedItems.indexOf(_scope.currentItem))
				}

				_scope.selectBranch = function(item) {
                    _scope.parentItems = _scope.items;
                    _scope.parentItem = _scope.currentItem; // to able to go back
                    _scope.selectedItems.push(item);
                    if(item.branch) {
                    	_scope.items = item.items;
                    	_scope.currentItem = item.items[0];
                    } else {
                    	_scope.next();
                    }
				}

				_scope.smsText = function() {
					var text = "";

					for (var i = 0; i < _scope.selectedItems.length; i++) {
						var item = _scope.selectedItems[i];

						if (item.type == "selectOption")
							text += item.options.key.value + " ";
						else if (item.options.question && item.options.question.input)
							text += item.options.question.input + " ";

					}

					return text
				}

				$compile(modal)(_scope);

				$document.find('body').append(modal);
				$timeout(function(){
					$animate.addClass(modal, 'open in');
				});

			});
		}
	}
}]);

// Tree view fix!
mainApp.directive('lastElement', function() {
	return {
		scope: {
			last: '=',
			item: '='
		},
		link: function(scope, element, attrs) {
			scope.$watch(function() {
				if (scope.last)
					if(element.find('ul').height())
						element.css({
							height: 55,
							marginBottom:element.find('ul').height()
						});
			}, true);
		}
	}
});

mainApp.filter('toDash', function() {
	return function(value) {
		if (value)
			return value.replace(/([A-Z])/g, function($1){ return "-"+$1.toLowerCase(); });
		else
			return '';
	}
});

mainApp.directive('uniform',function($timeout){
  return {
    restrict:'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      $(element).uniform({useID: false});
      scope.$watch(function() {return ngModel.$modelValue}, function() {
        $timeout(jQuery.uniform.update, 0);
      });
    }
  };
});

mainApp.directive('filterPattern', ['$timeout', function($timeout) {
	return function(scope, element, attrs) {
		if (attrs.filterPattern) {
			element.keypress(function(e) {
				var re = new RegExp(attrs.filterPattern);
				var ch = String.fromCharCode(e.keyCode);

				if(!ch.match(re))
					e.preventDefault()
			});
		};
	}
}]);

mainApp.directive('maxLen', function() {
	  return {
		    require: 'ngModel',
		    link: function (scope, element, attrs, ngModelCtrl) {
		      function fromUser(text) {
		    	  var maxlength = Number(attrs.maxLen);
		          if (text.length > maxlength) {
		            var transformedInput = text.substring(0, maxlength);
		            ngModelCtrl.$setViewValue(transformedInput);
		            ngModelCtrl.$render();
		            return transformedInput;
		          } 
		          return text;
		      }
		      ngModelCtrl.$parsers.push(fromUser);
		    }
		  }; 
});
