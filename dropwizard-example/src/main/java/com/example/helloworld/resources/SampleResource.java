package com.example.helloworld.resources;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.validator.constraints.NotBlank;

import com.example.helloworld.core.Sample;

@Path("/sample")
@Produces(MediaType.APPLICATION_JSON)
public class SampleResource {

	@POST
	@Path("/{id}")
	public String parameterDemoMethod(String body, 
	                                   @PathParam("id") long id, 
	                                   @QueryParam("foo") String foo, 
	                                   @HeaderParam("X-Auth-Token") String token, 
	                                   @Context HttpServletRequest request) {
	    String response;
	    response  = "id = " + id + "\n";
	    response += "body = " + body + "\n";
	    response += "foo = " + foo + "\n";
	    response += "token = " + token + "\n";
	    response += "ip = " + request.getRemoteAddr() + "\n";
	    return response;
	}
	
	@POST
	@Path("validateblank")
	public String parameterParams(@NotBlank @QueryParam("foo") String foo) {
	    String response = "";
	    response += "foo = " + foo + "\n";
	    return response;
	}
	
	
	@POST
	@Path("validatebean")
	public Sample parameterDemoMethod(@Valid Sample sample) {
	    return sample;
	}
	
	@GET
	@Path("adhocresponse")
	public Response adhocResponse() {
		return Response.status(Status.OK).entity("My ad hoc content").build();
	}
	
	
}
