package tr.com.metamorfoz.web.core.campaign.statistic;

public class ServiceCounter {
	
	private BaseCounter[] counters;
	
	/**
	 * default constructor creates counter of the given size for the counter types.
	 */
	public ServiceCounter() {
		counters = new BaseCounter[CounterType.values().length];
		for (int i = 0; i < counters.length; i++) {
			counters[i] = new BaseCounter();
		}
		
	}

	//===================================================================================================================================================
	// 												GETTER & SETTER
	//===================================================================================================================================================
	public BaseCounter getCounter(CounterType counterType) {
		return counters[counterType.ordinal()];
	}
	
}
