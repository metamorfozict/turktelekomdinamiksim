package tr.com.metamorfoz.web.core.stup.dao;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.stup.domain.StupRule;

public class StupRuleDao extends AbstractDao<StupRule> {

	public StupRuleDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

}
