package tr.com.metamorfoz.web.shared.model.dstk.dstkmodule;

import java.util.List;


public class DstkModuleDtoItem {

	private String title;
	private String type;
	private List<DstkModuleDtoItem> items;
	private DstkModuleDtoOptionsDto options;
	private String $$hashKey;
	private String branch;
	private String destination;
	private String keyword;
	private String imei;
	private String localInfo;
	private Boolean encrypted;   
    private String url;
	
	public DstkModuleDtoItem() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public DstkModuleDtoOptionsDto getOptions() {
		return options;
	}

	public void setOptions(DstkModuleDtoOptionsDto options) {
		this.options = options;
	}

	public List<DstkModuleDtoItem> getItems() {
		return items;
	}

	public void setItems(List<DstkModuleDtoItem> items) {
		this.items = items;
	}

	public String get$$hashKey() {
		return $$hashKey;
	}

	public void set$$hashKey(String $$hashKey) {
		this.$$hashKey = $$hashKey;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Boolean getEncrypted() {
		return encrypted;
	}

	public void setEncrypted(Boolean encrypted) {
		this.encrypted = encrypted;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getLocalInfo() {
		return localInfo;
	}

	public void setLocalInfo(String localInfo) {
		this.localInfo = localInfo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	
}
