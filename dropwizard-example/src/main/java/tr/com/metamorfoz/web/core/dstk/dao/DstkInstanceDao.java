package tr.com.metamorfoz.web.core.dstk.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;

public class DstkInstanceDao extends AbstractDao<DstkInstance> {

	public DstkInstanceDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public DstkInstance findUniqueDstkInstanceByVersion(int instance, String instanceHeader, int instanceVersion) {
		//Assumption:Veritabanında sadece 1 kayıt olduğu kabul edilmiştir.
		DstkInstance dstkInstance = (DstkInstance) currentSession().createCriteria(DstkInstance.class)
													.add(Restrictions.eq("instance", instance))
													.add(Restrictions.eq("instanceHeader", instanceHeader))
													.add(Restrictions.eq("instanceVersion", instanceVersion))
													.uniqueResult();
		return dstkInstance;		
	}
	
	public int findMaxInstanceVersion(String instanceHeader, int instance) {

		String queryString = "select coalesce(max(instanceVersion),0) from DstkInstance where instanceHeader = :instanceHeader and instance = :instance ";
		int maxVersion =  (int) currentSession().createQuery(queryString)
											.setParameter("instanceHeader", instanceHeader)
											.setParameter("instance", instance)
											.uniqueResult();
		return maxVersion;
		
	}
}
