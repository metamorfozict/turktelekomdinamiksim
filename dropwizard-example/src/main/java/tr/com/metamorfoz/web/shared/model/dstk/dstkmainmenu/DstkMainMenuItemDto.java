package tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu;

import java.util.List;

public class DstkMainMenuItemDto {

	private String function;
	private List<DstkMainMenuItemDto> items;
	private String $$hashKey;
	
	public DstkMainMenuItemDto() {
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public List<DstkMainMenuItemDto> getItems() {
		return items;
	}

	public void setItems(List<DstkMainMenuItemDto> items) {
		this.items = items;
	}

	public String get$$hashKey() {
		return $$hashKey;
	}

	public void set$$hashKey(String $$hashKey) {
		this.$$hashKey = $$hashKey;
	}
	
	
}
