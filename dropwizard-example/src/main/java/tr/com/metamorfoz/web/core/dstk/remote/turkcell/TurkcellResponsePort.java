package tr.com.metamorfoz.web.core.dstk.remote.turkcell;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import tr.com.metamorfoz.web.core.campaign.statistic.CounterType;
import tr.com.metamorfoz.web.core.campaign.statistic.Statistic;


@WebService(serviceName = "AsyncRespServer", portName = "AsyncRespServerPort", endpointInterface = "tr.com.turkcellteknoloji.sim.AsyncRespServer", targetNamespace = "http://sim.turkcellteknoloji.com.tr/", wsdlLocation = "WEB-INF/wsdl/NewWebServiceFromWSDL/ServiceBoosterOutBound.wsdl")
public class TurkcellResponsePort {

	private static final Logger logger = Logger.getLogger(TurkcellResponsePort.class);
	
	public int sendServiceBoosterAsyncResponse(java.lang.String status, java.lang.String transactionId, java.lang.String msisdn, java.lang.String variantId) {
        
		//=======================================================================================================================================
		// TODO : All logs will be asynchronous...
		//=======================================================================================================================================
		logger.info(transactionId + TurkcellDialogConstants.FIELD_SEPARATOR + msisdn + TurkcellDialogConstants.FIELD_SEPARATOR + status);
		
		if (TurkcellDialogConstants.SUCCESS.equals(status)) {
			Statistic.incrementCounters(transactionId, CounterType.DELIVERED);
		} else {
			Statistic.incrementCounters(transactionId, CounterType.NON_DELIVERED);
		}
		
		return TurkcellDialogConstants.RETURN_CODE;
		
    }

}
