package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.ota.api.OtaApiService;
import tr.com.metamorfoz.web.shared.model.dstk.DstkThemeDto;
import tr.com.metamorfoz.web.shared.model.ota.OtaTransportKeyDto;

@Path("/api/dstktheme")
@Produces(MediaType.APPLICATION_JSON)
public class DstkThemeResource {

	public DstkThemeResource(OtaApiService otaApiService) {
		super();
	}
	
	@GET
	@UnitOfWork
	public List<DstkThemeDto> findAll(){
		return DstkThemeUtil.readThemeFile(0);
	}
	
	@GET
	@Path("/search")
	@UnitOfWork
	public DstkThemeDto search(@QueryParam("id") int id){
		return DstkThemeUtil.readThemeFile(id).get(0);
	}
	
	@POST
	@UnitOfWork
	public DstkThemeDto create(DstkThemeDto dstkThemeDto) {
		return DstkThemeUtil.readThemeFile(0).get(0);
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public DstkThemeDto find(@PathParam("id") int id) {
		return DstkThemeUtil.readThemeFile(0).get(0);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public DstkThemeDto update(@PathParam("id") int id, DstkThemeDto dstkThemeDto) {
		return DstkThemeUtil.writeThemeFile(id, dstkThemeDto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		return;
	}
	
}
