package tr.com.metamorfoz.web.core.dstk.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkMainMenu;

public class DstkMainMenuDao extends AbstractDao<DstkMainMenu> {

	public DstkMainMenuDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	
	public DstkMainMenu findUniqueDstkMainMenuByVersion(String mainMenuHeader, int mainMenuVersion) {
		//Assumption:Veritabanında sadece 1 kayıt olduğu kabul edilmiştir.
		DstkMainMenu dstkMainMenu = (DstkMainMenu) currentSession().createCriteria(DstkMainMenu.class)
													.add(Restrictions.eq("mainMenuHeader", mainMenuHeader))
													.add(Restrictions.eq("mainMenuVersion", mainMenuVersion))
													.uniqueResult();
		return dstkMainMenu;		
	}
	
	public int findMaxMainMenuVersion(String mainMenuHeader) {

		String queryString = "select coalesce(max(mainMenuVersion),0) from DstkMainMenu where mainMenuHeader = :mainMenuHeader ";
		int maxVersion =  (int) currentSession().createQuery(queryString)
											.setParameter("mainMenuHeader", mainMenuHeader)
											.uniqueResult();
		return maxVersion;
		
	}
}
