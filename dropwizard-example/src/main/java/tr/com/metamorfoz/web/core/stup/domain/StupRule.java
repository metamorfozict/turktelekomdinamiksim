package tr.com.metamorfoz.web.core.stup.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

@Entity
@Table(name="TBL_STUP_RULES")
public class StupRule extends BaseEntity {

	@Column(name="N_BALANCE_BELOW_LIMIT")
	private Integer balanceBelowLimit;

	@Length(max = 50)
	@Column(name="T_LINE_TYPE", length=50)
	private String lineType;
	
	@Column(name="N_LAST_TRANSACTION")
	private Long lastTransaction;	
	
	@Column(name="N_USUAL_AMOUNT")
	private Integer usualAmount;
	
	@Length(max = 30)
	@Column(name="T_MODULE_NAME")
	private String moduleName; 
	  
	@Column(name="N_VALID_FROM")
	private Integer validFrom; 
		  
	@Column(name="N_VALID_TO")
	private Integer validTo; 

	@Column(name="N_MAX_CNT")
	private Integer maxCount; 

	@Column(name="N_REPETITION")
	private Integer repetition; 

	@Column(name="N_DURATION")
	private Long duration; 
	
	public StupRule() {
	}
	
	public void update(Integer balanceBelowLimit
					  , String lineType
					  , Long lastTransaction
					  , Integer usualAmount
					  , String moduleName
					  , Integer validFrom
					  , Integer validTo
					  , Integer maxCount
					  , Integer repetition
					  , Long duration) {
		this.balanceBelowLimit = balanceBelowLimit;
		this.lineType = lineType;
		this.lastTransaction = lastTransaction;
		this.usualAmount = usualAmount;
		this.moduleName = moduleName;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.maxCount = maxCount;
		this.repetition = repetition;
		this.duration = duration;
	}

	public Integer getBalanceBelowLimit() {
		return balanceBelowLimit;
	}

	public void setBalanceBelowLimit(Integer balanceBelowLimit) {
		this.balanceBelowLimit = balanceBelowLimit;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public Long getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(Long lastTransaction) {
		this.lastTransaction = lastTransaction;
	}

	public Integer getUsualAmount() {
		return usualAmount;
	}

	public void setUsualAmount(Integer usualAmount) {
		this.usualAmount = usualAmount;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Integer validFrom) {
		this.validFrom = validFrom;
	}

	public Integer getValidTo() {
		return validTo;
	}

	public void setValidTo(Integer validTo) {
		this.validTo = validTo;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public Integer getRepetition() {
		return repetition;
	}

	public void setRepetition(Integer repetition) {
		this.repetition = repetition;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}
	
}
