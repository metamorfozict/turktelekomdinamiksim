package tr.com.metamorfoz.web.shared.model.dstk.dstkinstance;

import java.util.List;

public class DstkInstanceJsonContentItemDto {

	private Boolean active;
	private String title;
	private String function;
	private List<DstkInstanceJsonContentItemDto> items;
	private String $$hashKey;
	
	public DstkInstanceJsonContentItemDto() {
		super();
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public List<DstkInstanceJsonContentItemDto> getItems() {
		return items;
	}

	public void setItems(List<DstkInstanceJsonContentItemDto> items) {
		this.items = items;
	}

	public String get$$hashKey() {
		return $$hashKey;
	}

	public void set$$hashKey(String $$hashKey) {
		this.$$hashKey = $$hashKey;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
    	
}
