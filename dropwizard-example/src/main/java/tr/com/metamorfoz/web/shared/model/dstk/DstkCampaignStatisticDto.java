package tr.com.metamorfoz.web.shared.model.dstk;

import java.util.Date;

import javax.persistence.Column;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class DstkCampaignStatisticDto {

	@NotBlank
	@Length(max = 128)
	private String serviceId;

	private Date startTime;
	
	private Date endTime;

	private Integer status;
	private Integer total;
	private Integer sent;
	private Integer delivered;
	private Integer blackListed;
	private Integer nonDelivered;
	private Integer accepted;

	@NotBlank
	@Length(max = 300)
	private String description;

	@NotBlank
	@Length(max = 50)
	@Column(length = 50)
	private String type;

	public DstkCampaignStatisticDto() {
	}
	
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Integer getDelivered() {
		return delivered;
	}

	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}

	public Integer getBlackListed() {
		return blackListed;
	}

	public void setBlackListed(Integer blackListed) {
		this.blackListed = blackListed;
	}

	public Integer getAccepted() {
		return accepted;
	}

	public void setAccepted(Integer accepted) {
		this.accepted = accepted;
	}

	public String getDesciption() {
		return description;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getNonDelivered() {
		return nonDelivered;
	}

	public void setNonDelivered(Integer nonDelivered) {
		this.nonDelivered = nonDelivered;
	}

	//TODO:Convert it to retrieve data from Codes Table
	public String getStatusName() {
		return getStatusName(status);
	}
	

	//TODO:Convert it to retrieve data from Codes Table
	public static String getStatusName(int statusId) {
		
		switch (statusId) {
		case 1:
			return "ACTIVE";
		case 2:
			return "PLANNED";
		case 3:
			return "COMPLETED";
		default:
			return "UNKOWN_STATUS";
		}
		
	}
	
	//TODO:Convert it to retrieve data from Codes Table
	public static int getStatusId(String statusName) {
		
		switch (statusName) {
		case "ACTIVE":
			return 1;
		case "PLANNED":
			return 2;
		case "COMPLETED":
			return 3;
		default:
			return 0;
		}
		
	}
	
	
	
}
