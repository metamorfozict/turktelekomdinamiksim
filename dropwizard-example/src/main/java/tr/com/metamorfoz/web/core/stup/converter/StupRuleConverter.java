package tr.com.metamorfoz.web.core.stup.converter;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.stup.domain.StupRule;
import tr.com.metamorfoz.web.shared.model.stup.STUPConstants;
import tr.com.metamorfoz.web.shared.model.stup.StupRuleDto;

public class StupRuleConverter extends AbstractEntityConverter<StupRule, StupRuleDto>  {

	@Override
	public StupRule createEntity(final StupRuleDto dto) {
		StupRule entity = new StupRule();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(final StupRule entity, StupRuleDto dto) {
		entity.setBalanceBelowLimit(dto.getBalanceBelowLimit());
		entity.setValidFrom(dto.getValidFrom());
		entity.setValidTo(dto.getValidTo());
		entity.setDuration(dto.getDuration());
		entity.setLastTransaction(dto.getLastTransaction());
		entity.setLineType(dto.getLineType());
		entity.setMaxCount(dto.getMaxCount());
		entity.setModuleName(dto.getModuleName());
		entity.setRepetition(dto.getRepetition());
		entity.setUsualAmount(dto.getUsualAmount());
	}

	@Override
	public StupRuleDto convertToDto(final StupRule entity) {
		StupRuleDto ruleDto = new StupRuleDto(entity.getId()
											 , entity.getBalanceBelowLimit()
											 , entity.getLineType()
											 , entity.getLastTransaction()
											 , entity.getUsualAmount()
											 , entity.getModuleName()
											 , entity.getValidFrom()
											 , entity.getValidTo()
											 , entity.getMaxCount()
											 , entity.getRepetition()
											 , entity.getDuration());
		return ruleDto;
	}




}
