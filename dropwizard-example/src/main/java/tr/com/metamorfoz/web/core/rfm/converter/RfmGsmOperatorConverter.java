package tr.com.metamorfoz.web.core.rfm.converter;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;
import tr.com.metamorfoz.web.shared.model.rfm.RfmGsmOperatorDto;

public class RfmGsmOperatorConverter extends AbstractEntityConverter<RfmGsmOperator, RfmGsmOperatorDto>  {

	@Override
	public RfmGsmOperator createEntity(final RfmGsmOperatorDto dto) {
		RfmGsmOperator entity = new RfmGsmOperator();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(final RfmGsmOperator entity, RfmGsmOperatorDto dto) {
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
	}

	@Override
	public RfmGsmOperatorDto convertToDto(final RfmGsmOperator entity) {
		RfmGsmOperatorDto operatorDto = new RfmGsmOperatorDto(entity.getId(), entity.getCode(), entity.getName(), entity.getDescription());
		return operatorDto;
	}




}
