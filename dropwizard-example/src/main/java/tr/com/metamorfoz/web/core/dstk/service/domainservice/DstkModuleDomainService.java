package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import java.util.List;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.dao.DstkModuleDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;

public class DstkModuleDomainService extends AbstractDomainCrudService<DstkModule, DstkModuleDao> {

	public DstkModuleDomainService(DstkModuleDao dao) {
		super(dao);
	}

	public DstkModule findUniqueDstkModuleByName(String moduleName) {
		return getDao().findUniqueDstkModuleByName(moduleName);
	}
	
	public DstkModule findUniqueDstkModuleByModuleId(Integer moduleId) {
		return getDao().findUniqueDstkModuleByModuleId(moduleId);
	}
	
	public List<DstkModule> findUniqueDstkModuleByType(Integer type) {
		return getDao().findUniqueDstkModuleByType(type);
	}
	
}
