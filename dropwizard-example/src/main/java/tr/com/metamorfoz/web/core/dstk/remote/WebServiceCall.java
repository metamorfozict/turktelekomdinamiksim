package tr.com.metamorfoz.web.core.dstk.remote;

import org.apache.log4j.Logger;

import tr.com.metamorfoz.common.pool.SyncConnectionPool;
import tr.com.metamorfoz.dstk.remote.turkcell.ServiceBoosterInboundInput;
import tr.com.metamorfoz.dstk.remote.turkcell.ServiceBoosterInboundOutput;
import tr.com.metamorfoz.dstk.remote.turkcell.TSOSENDSERVICEREQ;
import tr.com.metamorfoz.web.core.campaign.statistic.CounterType;
import tr.com.metamorfoz.web.core.campaign.statistic.Statistic;
import tr.com.metamorfoz.web.core.dstk.remote.turkcell.ClientInterfacePortConnection;
import tr.com.metamorfoz.web.core.dstk.remote.turkcell.TurkcellDialogConstants;

public abstract class WebServiceCall implements Runnable {

	//=======================================================================================================
	// Logger definition.
	//=======================================================================================================
	private static final Logger logger = Logger.getLogger(WebServiceCall.class);
	
	//=======================================================================================================
	// Member function.
	//=======================================================================================================
	private SyncConnectionPool syncConnectionPool;
	
	
	/**
	 * the default constructor.
	 * 
	 * @param					SyncConnectionPool				the synchronized connection pool.
	 */
	public WebServiceCall(SyncConnectionPool syncConnectionPool) {
		this.syncConnectionPool = syncConnectionPool;
	}
	
	/**
	 * this function calls the service provider
	 * 
	 */
	public String call(ServiceBoosterInboundInput requestParams) {
		
		ClientInterfacePortConnection clientInterfacePortConnection = null;
		String 						  result						= "NOK"; 
		
		try {
			
			//========================================================================================================
			// TODO : log connection getting...
			//========================================================================================================
			clientInterfacePortConnection = (ClientInterfacePortConnection) this.syncConnectionPool.getConnection(0);
			
			//========================================================================================================
			// TODO : log service parameters, service times etc.
			//========================================================================================================
			ServiceBoosterInboundOutput status = ((TSOSENDSERVICEREQ) clientInterfacePortConnection.getWsClientInterface()).serviceBoosterInbound(requestParams);
			result = status.getTSOresult().getStatusCode();
			
			logger.info(this + " status : " + result);
			
			if (TurkcellDialogConstants.SUCCESS.equals(result)) {
				//========================================================================================================
				// TODO : Insert or Update the campaign statistics table in DB.
				//========================================================================================================
				Statistic.addToStatistics(requestParams.getTransactionId());
				Statistic.incrementCounters(requestParams.getTransactionId(), CounterType.SENT, requestParams.getMsisdnList().getMSISDN().size());
			}
		
		} catch (Throwable t) {
			logger.error(this.toString(), t);
		} finally {
			if (clientInterfacePortConnection != null) {
				this.syncConnectionPool.returnConnection(clientInterfacePortConnection);
			}
		}
		
		return result;
		
	}

}
