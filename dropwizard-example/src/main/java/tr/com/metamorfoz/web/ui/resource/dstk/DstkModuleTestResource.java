package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkModuleApiService;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.utility.CopyOfDSTKTest_64K;
import tr.com.metamorfoz.web.utility.DSTKTest;
import tr.com.metamorfoz.web.utility.DSTKTest_512_150210;
import tr.com.metamorfoz.web.utility.DSTKTest_Azerfon;
import tr.com.metamorfoz.web.utility.DSTKTest_HTC;
import tr.com.metamorfoz.web.utility.DSTKTest_KEM;
import tr.com.metamorfoz.web.utility.DSTKTest_NO3;
import tr.com.metamorfoz.web.utility.DSTKTest_NO4;
import tr.com.metamorfoz.web.utility.DSTKTest_NO5;
import tr.com.metamorfoz.web.utility.DSTKTest_SM5;
import tr.com.metamorfoz.web.utility.DSTKTest_SMS;
import tr.com.metamorfoz.web.utility.DSTKTest_SN5;

@Path("/api/dstkmoduletest")
@Produces(MediaType.APPLICATION_JSON)
public class DstkModuleTestResource {
	
	private DstkModuleApiService dstkModuleApiService;
	
	public DstkModuleTestResource(DstkModuleApiService dstkModuleApiService) {
		super();
		this.dstkModuleApiService = dstkModuleApiService;
	}
	
	
	@GET
	@Path("{modulename}")
	@UnitOfWork
	public DstkModule find(@PathParam("modulename") String modulename) {
		DstkModule dstkModule = dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByName(modulename);
		
		System.out.println(modulename);
		
		if (modulename.startsWith("S64")) {
			System.out.println(modulename + "-1-64K");
			CopyOfDSTKTest_64K.sendScenario(dstkModule);
		} else if (modulename.startsWith("S512")) {
			System.out.println(modulename + "-2-512K");
			DSTKTest_512_150210.sendScenario(dstkModule);
		} else if (modulename.startsWith("HTC")) {
			System.out.println(modulename + "-HTC");
			DSTKTest_HTC.sendScenario(dstkModule);
		} else if (modulename.startsWith("SMS")) {
			System.out.println(modulename + "-SMS");
			DSTKTest_SMS.sendScenario(dstkModule);
		} else if (modulename.startsWith("NO4")) {
			System.out.println(modulename + "-NO4");
			DSTKTest_NO4.sendScenario(dstkModule);
		} else if (modulename.startsWith("NO5")) {
			System.out.println(modulename + "-NO5");
			DSTKTest_NO5.sendScenario(dstkModule);
		} else if (modulename.startsWith("SM5")) {
			System.out.println(modulename + "-SM5");
			DSTKTest_SM5.sendScenario(dstkModule);
		// bizdeki Avea ya ait S5	
		} else if (modulename.startsWith("SN5")) {
			System.out.println(modulename + "-SN5");
			DSTKTest_SN5.sendScenario(dstkModule);
		}  else if (modulename.startsWith("KEM")) {
			System.out.println(modulename + "-KEM");
			DSTKTest_KEM.sendScenario(dstkModule);
		} else if (modulename.startsWith("NO3")) {
			System.out.println(modulename + "-NO3");
			DSTKTest_NO3.sendScenario(dstkModule);
		} else if (modulename.startsWith("AZ")) {
			System.out.println(modulename + "-AZ");
			DSTKTest_Azerfon.sendScenario(dstkModule);
		} else {
			System.out.println(modulename + "-3-128K");
			DSTKTest.sendScenario(dstkModule);
		}
		
		return dstkModule;
	}

}
