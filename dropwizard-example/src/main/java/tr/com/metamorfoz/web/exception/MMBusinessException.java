package tr.com.metamorfoz.web.exception;

public class MMBusinessException extends MMException {

	public MMBusinessException(String message, Throwable e) {
		super(message, e);
	}

	public MMBusinessException(String message) {
		super(message);
	}


	
}
