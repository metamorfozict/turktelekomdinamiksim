package tr.com.metamorfoz.web.core.dstk.api;

import java.util.List;

import tr.com.metamorfoz.web.core.common.api.AbstractApiDtoCrudService;
import tr.com.metamorfoz.web.core.common.converter.EntityConverterUtil;
import tr.com.metamorfoz.web.core.dstk.converter.DstkCampaignStatisticConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkCampaignStatisticDomainService;
import tr.com.metamorfoz.web.shared.model.dstk.DstkCampaignStatisticDto;

public class DstkCampaignStatisticApiDtoService extends AbstractApiDtoCrudService<DstkCampaignStatistic, DstkCampaignStatisticDto, DstkCampaignStatisticDomainService, DstkCampaignStatisticConverter> {

	public DstkCampaignStatisticApiDtoService(DstkCampaignStatisticDomainService dstkCampaignStatisticDomainService, DstkCampaignStatisticConverter dstkCampaignStatisticConverter) {
		super(dstkCampaignStatisticDomainService, dstkCampaignStatisticConverter);
	}

	/**
	 * Retrieves the campaign statistics.
	 * 
	 * @param 					statusName					the status of the campaign.
	 * @param 					type
	 * @return					List<DstkCampaignStatisticDto>
	 */
	public List<DstkCampaignStatisticDto> search(String statusName, String type) {
		int statusId = DstkCampaignStatisticDto.getStatusId(statusName); 
		List<DstkCampaignStatistic> entityList= getDomainCrudService().search(statusId, type);
		return EntityConverterUtil.convertToDtoList(getConverter(), entityList);
	}

}
