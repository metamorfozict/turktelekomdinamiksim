package tr.com.metamorfoz.web.ui.exception.mapper;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tr.com.metamorfoz.web.ui.exception.dto.ErrorDto;
import tr.com.metamorfoz.web.ui.exception.dto.FieldErrorDto;
import tr.com.metamorfoz.web.utility.ExceptionUtil;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

	private final static Logger logger = LoggerFactory.getLogger(ConstraintViolationExceptionMapper.class); 

    @Override
    public Response toResponse(ConstraintViolationException e) {
    	
    	ErrorDto errorDto = new ErrorDto();
    	errorDto.setMessage(e.getMessage());
    	
    	Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
    	
    	List<FieldErrorDto> fieldErrorDTOs = ExceptionUtil.populateFieldErrors(constraintViolations);
    	errorDto.setFieldErrors(fieldErrorDTOs);
    	
    	logger.error("JpaConstraintViolationExceptionMapper Caught", e);
        return Response.status(Status.BAD_REQUEST).entity(errorDto).build();
    }
    

}
