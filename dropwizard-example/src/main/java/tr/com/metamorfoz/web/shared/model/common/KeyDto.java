package tr.com.metamorfoz.web.shared.model.common;

public class KeyDto {

	private String value;
	
	public KeyDto() {
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
