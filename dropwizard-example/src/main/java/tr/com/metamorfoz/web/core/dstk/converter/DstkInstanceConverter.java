package tr.com.metamorfoz.web.core.dstk.converter;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceDto;

public class DstkInstanceConverter extends AbstractEntityConverter<DstkInstance, DstkInstanceDto> {


	@Override
	public DstkInstance createEntity(final DstkInstanceDto dto) {
		DstkInstance entity = new DstkInstance();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(final DstkInstance entity, DstkInstanceDto dto) {
		entity.setAppId(dto.getAppId());
//		entity.setContent(content);
		entity.setInstance(dto.getInstance());
		entity.setInstanceHeader(dto.getInstanceHeader());
		entity.setInstanceVersion(dto.getInstanceVersion());
		entity.setIntrBlock(dto.getIntrBlock());
//		entity.setJson(json);
		entity.setProfile(dto.getProfile());
		entity.setTar(dto.getTar());
	}

	@Override
	public DstkInstanceDto convertToDto(final DstkInstance entity) {
		DstkInstanceDto dto = new DstkInstanceDto(entity.getId(), entity.getInstanceHeader()
												, entity.getInstance(), entity.getAppId()
												, entity.getIntrBlock(), entity.getProfile()
												, entity.getTar(), entity.getInstanceVersion()
												, entity.getModificationTime(), entity.getContent(), entity.getJson());
		return dto;
	}


}
