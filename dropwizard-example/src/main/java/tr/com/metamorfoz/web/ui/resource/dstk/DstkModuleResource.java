package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkModuleApiService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkModuleConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDtoSaveRequest;

@Path("/api/dstkmodule")
@Produces(MediaType.APPLICATION_JSON)
public class DstkModuleResource {
	
	private DstkModuleApiService dstkModuleApiService;
	
	public DstkModuleResource(DstkModuleApiService dstkModuleApiService) {
		super();
		this.dstkModuleApiService = dstkModuleApiService;
	}
	
	@GET
	@UnitOfWork
	public List<DstkModuleDto> findAll(){
		return dstkModuleApiService.findAll();
	}

	@GET
	@Path("/searchpushmodule")
	@UnitOfWork
	public List<DstkModule> searchPushModule(){
		return dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByType(0);
	}
	
	@GET
	@Path("/searchmenumodule")
	@UnitOfWork
	public List<DstkModule> searchMenuModule(){
		return dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByType(1);
	}
	
	@GET
	@Path("/searchterminalmodule")
	@UnitOfWork
	public List<DstkModule> search(){
		return dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByType(2);
	}

	@GET
	@Path("/searchstupmodule")
	@UnitOfWork
	public List<DstkModule> searchStupModule(){
		return dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByType(3);
	}
	
	@POST
	@UnitOfWork
	public void create(@Valid DstkModuleDtoSaveRequest request) {
		dstkModuleApiService.save(request, "A");
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public DstkModuleDto find(@PathParam("id") int id) {
		return dstkModuleApiService.findSafely(id);
	}

	@PUT
	@Path("{id}")
	@UnitOfWork
	public void update(@PathParam("id") int id, @Valid DstkModuleDtoSaveRequest request) {
		dstkModuleApiService.save(request, "E");
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		dstkModuleApiService.deleteAll(ids);
	}
	
}
