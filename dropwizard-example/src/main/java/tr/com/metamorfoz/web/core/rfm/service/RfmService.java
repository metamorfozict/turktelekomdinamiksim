package tr.com.metamorfoz.web.core.rfm.service;

import tr.com.metamorfoz.web.core.rfm.service.domainservice.RfmGsmOperatorDomainService;

public class RfmService  {

	private RfmGsmOperatorDomainService rfmGsmOperatorDomainService;

	public RfmService(RfmGsmOperatorDomainService rfmGsmOperatorDomainService) {
		super();
		this.rfmGsmOperatorDomainService = rfmGsmOperatorDomainService;
	}

	public RfmGsmOperatorDomainService getRfmGsmOperatorDomainService() {
		return rfmGsmOperatorDomainService;
	}

	public void setRfmGsmOperatorDomainService(RfmGsmOperatorDomainService rfmGsmOperatorDomainService) {
		this.rfmGsmOperatorDomainService = rfmGsmOperatorDomainService;
	}

}