package tr.com.metamorfoz.web.shared.model.ota;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class OtaGsmOperatorDto  {

	private Integer id;
	
	@NotBlank
	@Length(max = 128)
	private String code;
	
	@Length(max = 128)
	private String name;

	@Length(max = 128)
	private String description;

	public OtaGsmOperatorDto() {
		super();
	}

	public OtaGsmOperatorDto(Integer id, String code, String name,
			String description) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
}
