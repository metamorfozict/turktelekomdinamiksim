package tr.com.metamorfoz.web.utility;

import org.apache.log4j.Logger;

import tr.com.metamorfoz.otasocketinterface.ClientConnectionManager;
import tr.com.metamorfoz.otasocketinterface.SocketConnectionEventHandler;
import tr.com.metamorfoz.otasocketinterface.messages.APDUExecutionResult;
import tr.com.metamorfoz.otasocketinterface.messages.APDUSubmitRequestList;
import tr.com.metamorfoz.otasocketinterface.messages.APDUSubmitResponse;
import tr.com.metamorfoz.otasocketinterface.messages.APDUSubmitResponseList;
import tr.com.metamorfoz.otasocketinterface.messages.CSCPVASMessage;
import tr.com.metamorfoz.otasocketinterface.messages.HeartBeatMessage;
import tr.com.metamorfoz.otasocketinterface.test.StatisticsPrintingTask.Counter;


/**
 * Default handler for socket connection events. 
 * Prints a log message when a connection event is fired.
 * Extends this class if you need special handling of connection events.
 * @author TCOBUK
 *
 */
public class ClientSocketConnectionEventHandler extends SocketConnectionEventHandler{

	private static Logger logger = Logger.getLogger(ClientSocketConnectionEventHandler.class);

	private ClientConnectionManager ccm;

	protected ClientSocketConnectionEventHandler(){
	
	}
	
	public void setCcm(ClientConnectionManager ccm){
		this.ccm = ccm;
	}
	
	/**
	 * Called when a message is received.
	 * @param message: incoming message
	 */
	@Override
	public void messageReceived(CSCPVASMessage messageObject){

		if (messageObject instanceof APDUSubmitRequestList){
			APDUSubmitRequestList apduSubmitRequestList = (APDUSubmitRequestList)messageObject;
			logger.error("APDUSubmitRequestList message received. apduSubmitRequestList: " + apduSubmitRequestList);

			Counter.NumOfInSubmitReqs.incrementCounters(apduSubmitRequestList.getApduSubmitRequestList().size());
			Counter.NumOfInSubmitReqLists.incrementCounters();

		} else if (messageObject instanceof APDUSubmitResponseList){
			APDUSubmitResponseList apduSubmitResponseList = (APDUSubmitResponseList)messageObject;

			Counter.NumOfInSubmitRespLists.incrementCounters();

			for (APDUSubmitResponse apduSubmitResponse : apduSubmitResponseList.getApduSubmitResponseList()){
				Counter.NumOfInSubmitResps.incrementCounters();
				if (apduSubmitResponse.isAck()){
					Counter.NumOfInSubmitResps_Ack.incrementCounters();
				} else {
					Counter.NumOfInSubmitResps_Nack.incrementCounters();
				}
			}

		} else if (messageObject instanceof APDUExecutionResult){
			APDUExecutionResult apduExecutionResult = (APDUExecutionResult)messageObject;
			
			logger.info("apduExecutionResult received. apduExecutionResult: " + apduExecutionResult);
			

			switch (apduExecutionResult.getEventType()){
			case DELIVERY_NOTIF:
				Counter.NumOfInExecutionRes_DELIVERY_NOTIF.incrementCounters();
				break;
			case EXPIRED:
				Counter.NumOfInExecutionRes_EXPIRED.incrementCounters();
				break;
			case WAITING:
				Counter.NumOfInExecutionRes_WAITING.incrementCounters();
				break;
			case ACK:
				Counter.NumOfInExecutionRes_ACK.incrementCounters();
				break;
			case NACK:
				Counter.NumOfInExecutionRes_NACK.incrementCounters();
				break;
			case STATUS_RETRY:
				Counter.NumOfInExecutionRes_STATUS_RETRY.incrementCounters();
				break;
			case STATUS_INT_ERROR:
				Counter.NumOfInExecutionRes_STATUS_INT_ERROR.incrementCounters();
				break;
			case STATUS_TRN_COLLIDE:
				Counter.NumOfInExecutionRes_STATUS_TRN_COLLIDE.incrementCounters();
				break;
			case CONNECTION_OPEN:
				break;
			case CONNECTION_CLOSE:
				Counter.NumOfInExecutionRes_CONNECTION_CLOSE.incrementCounters();
				break;
			case CONNECTION_RESET:
				Counter.NumOfInExecutionRes_CONNECTION_RESET.incrementCounters();
				break;
			case CONNECTION_ERROR:
				Counter.NumOfInExecutionRes_CONNECTION_ERROR.incrementCounters();
				break;
			case STATUS_OK:
				Counter.NumOfInExecutionRes_STATUS_OK.incrementCounters();
				break;
			case STATUS_ERROR:
				Counter.NumOfInExecutionRes_STATUS_ERROR.incrementCounters();
				break;
			case MO:
				Counter.NumOfInExecutionRes_MO.incrementCounters();
				break;
			default:
				Counter.NumOfInExecutionRes_Invalid.incrementCounters();
			}

		} else if (messageObject instanceof HeartBeatMessage) {
			Counter.NumOfInHeartBeatMsgs.incrementCounters();

		} else {
			logger.error("Unexpected message received. messageObject: " + messageObject);
		}
	}
}