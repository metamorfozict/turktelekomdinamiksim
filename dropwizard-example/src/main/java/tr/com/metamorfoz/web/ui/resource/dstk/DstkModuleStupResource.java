package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkModuleApiService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkModuleConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDto;
import tr.com.metamorfoz.web.utility.DSTKTest;

@Path("/api/dstkmodulestup")
@Produces(MediaType.APPLICATION_JSON)
public class DstkModuleStupResource {
	
	private DstkModuleApiService dstkModuleApiService;
	
	public DstkModuleStupResource(DstkModuleApiService dstkModuleApiService) {
		super();
		this.dstkModuleApiService = dstkModuleApiService;
	}
	
	
	@GET
	@Path("{modulename}")
	@UnitOfWork
	public DstkModuleDto find(@PathParam("modulename") String modulename) {
		DstkModule dstkModule = dstkModuleApiService.getDstkModuleDomainService().findUniqueDstkModuleByName(modulename);
		return new DstkModuleConverter().convertToDto(dstkModule);
	}
	
	
}
