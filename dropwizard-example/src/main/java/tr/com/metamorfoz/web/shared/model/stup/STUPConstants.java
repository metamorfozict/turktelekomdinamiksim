package tr.com.metamorfoz.web.shared.model.stup;

public class STUPConstants {
	
	public static final long 	DAY_IN_MILLISECONDS  = 24 * 60 * 60 * 1000;
	public static final long 	WEEK_IN_MILLISECONDS = 7 * 24 * 60 * 60 * 1000;
	
	public static final String	KEYWORD_ONCE_A_DAY	 = "Once-A-Day";
	public static final String	KEYWORD_TWICE_A_DAY	 = "Twice-A-Day";
	public static final String	KEYWORD_ONCE_A_WEEK	 = "Once-A-Week";
	public static final String	KEYWORD_TWICE_A_WEEK = "Once-A-Day";
	
	public static final String 	RULE_NEGLECT 	  	 = "ANY";
	public static final String 	RULE_SELF_TOPUP 	 = "OWN";
	
}
