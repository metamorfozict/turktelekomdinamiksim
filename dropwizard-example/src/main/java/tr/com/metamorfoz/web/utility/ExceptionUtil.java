package tr.com.metamorfoz.web.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import tr.com.metamorfoz.web.ui.exception.dto.FieldErrorDto;

public class ExceptionUtil {

	public static List<FieldErrorDto> populateFieldErrors(Set<ConstraintViolation<?>> constraintViolations) {

		List<FieldErrorDto> fieldErrors = new ArrayList<FieldErrorDto>();
		StringBuilder errorMessage = new StringBuilder("");
		
		for (ConstraintViolation<?> constraintViolation : constraintViolations) {
			
//			String localizedErrorMsg = localizeErrorMessage(errorMessage.toString());
			
			fieldErrors.add(new FieldErrorDto(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
			errorMessage.delete(0, errorMessage.capacity());
		}
		return fieldErrors;
	}

}
