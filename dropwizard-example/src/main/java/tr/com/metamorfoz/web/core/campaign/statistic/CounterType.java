package tr.com.metamorfoz.web.core.campaign.statistic;


public enum CounterType {
	
	SENT
	, DELIVERED
	, BLACK_LISTED
	, NON_DELIVERED
	
}
