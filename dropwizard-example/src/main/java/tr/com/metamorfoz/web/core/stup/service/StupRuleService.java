package tr.com.metamorfoz.web.core.stup.service;

import tr.com.metamorfoz.web.core.stup.service.domainservice.StupRuleDomainService;

public class StupRuleService  {

	private StupRuleDomainService stupRuleDomainService;

	public StupRuleService(StupRuleDomainService stupRuleDomainService) {
		super();
		this.stupRuleDomainService = stupRuleDomainService;
	}

	public StupRuleDomainService getStupRuleDomainService() {
		return stupRuleDomainService;
	}

	public void setStupRuleDomainService(StupRuleDomainService stupRuleDomainService) {
		this.stupRuleDomainService = stupRuleDomainService;
	}

}