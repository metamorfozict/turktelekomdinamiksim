package tr.com.metamorfoz.web.core.rfm.api;

import java.util.List;

import tr.com.metamorfoz.web.core.common.converter.EntityConverterUtil;
import tr.com.metamorfoz.web.core.rfm.converter.RfmGsmOperatorConverter;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;
import tr.com.metamorfoz.web.core.rfm.service.RfmService;
import tr.com.metamorfoz.web.shared.model.rfm.RfmGsmOperatorDto;


public class RfmApiService {

	private RfmService rfmService;
	
	private RfmGsmOperatorConverter rfmGsmOperatorDtoConverter = new RfmGsmOperatorConverter();
	
	public RfmApiService(RfmService rfmService) {
		super();
		this.rfmService = rfmService;
	}

	public RfmGsmOperatorDto createGsmOperatorDto(RfmGsmOperatorDto rfmGsmOperatorDto) {
		//Assumption:Validation should be done at ui.
		RfmGsmOperator rfmGsmOperator = rfmGsmOperatorDtoConverter.createEntity(rfmGsmOperatorDto);
		rfmGsmOperator = rfmService.getRfmGsmOperatorDomainService().save(rfmGsmOperator);
		RfmGsmOperatorDto operatorDto = rfmGsmOperatorDtoConverter.convertToDto(rfmGsmOperator);
		return operatorDto;
	}

	public RfmGsmOperatorDto updateOperatorDto(int id, RfmGsmOperatorDto rfmGsmOperatorDto) {
		RfmGsmOperator rfmGsmOperator = rfmService.getRfmGsmOperatorDomainService().findSafely(id);
		rfmGsmOperatorDtoConverter.updateEntity(rfmGsmOperator, rfmGsmOperatorDto);
//		transportKey.update(transportKeyDto.getNo(), transportKeyDto.getValue(), transportKeyDto.getAlgorithmName(), transportKeyDto.getAlgorithmMode());
		rfmGsmOperator = rfmService.getRfmGsmOperatorDomainService().save(rfmGsmOperator);
		return rfmGsmOperatorDtoConverter.convertToDto(rfmGsmOperator);
	}
	
	public List<RfmGsmOperatorDto> findAllGsmOperatorDto(){
		List<RfmGsmOperator> gsmOperators = rfmService.getRfmGsmOperatorDomainService().findAll();
		return EntityConverterUtil.convertToDtoList(rfmGsmOperatorDtoConverter, gsmOperators);
	}

	public RfmGsmOperatorDto findGsmOperatorDtoSafely(int id)   {
		RfmGsmOperator rfmGsmOperator = rfmService.getRfmGsmOperatorDomainService().find(id);
		return rfmGsmOperatorDtoConverter.convertToDto(rfmGsmOperator);
	}
	
	public void deleteGsmOperatorDto(int id) {
		RfmGsmOperator rfmGsmOperator = rfmService.getRfmGsmOperatorDomainService().findSafely(id);
		rfmService.getRfmGsmOperatorDomainService().delete(rfmGsmOperator);
	}
	
	public int deleteAllGsmOperatorDto(List<Integer> ids) {
		return rfmService.getRfmGsmOperatorDomainService().deleteAll(ids);
	}
	

}
