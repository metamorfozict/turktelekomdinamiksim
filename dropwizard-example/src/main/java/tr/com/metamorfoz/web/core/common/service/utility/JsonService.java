package tr.com.metamorfoz.web.core.common.service.utility;

import java.io.ByteArrayOutputStream;

import tr.com.metamorfoz.web.exception.MMConversionException;
import tr.com.metamorfoz.web.exception.MMException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonService {

	private ObjectMapper objectMapper;

	public JsonService(ObjectMapper objectMapper) {
		super();
		this.objectMapper = objectMapper;
	}
	
	public <T> String toJson(T t)  {
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); 
		
		try {
			
			try {
				objectMapper.writeValue(outputStream, t);
			} catch (Exception e) {
				throw new MMConversionException("ByteArray'den Json nesnesine dönüştürülürken bir hata oluştu", e);
			} finally {
				outputStream.close();
			}
			
		} catch (MMException e) {
			throw e;
		} catch (Exception e) {
			throw new MMConversionException("IO Exception", e);
		}
		
		String json = new String(outputStream.toByteArray());
		return json;
		
	}
	
//	private T toPojo() {
//		
//	}
	

	
}
