package tr.com.metamorfoz.web.ui.exception.dto;

import java.util.List;

public class ErrorDto {
		
	private String message;
	private String technicalMessage;
	private List<FieldErrorDto> fieldErrors;
	
	public ErrorDto() {
	}
	
	public ErrorDto(String message) {
		super();
		this.message = message;
	}

	public ErrorDto(String message, String technicalMessage) {
		super();
		this.message = message;
		this.technicalMessage = technicalMessage;
	}

	public ErrorDto(String message, String technicalMessage, List<FieldErrorDto> fieldErrors) {
		super();
		this.message = message;
		this.technicalMessage = technicalMessage;
		this.fieldErrors = fieldErrors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<FieldErrorDto> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldErrorDto> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public String getTechnicalMessage() {
		return technicalMessage;
	}

	public void setTechnicalMessage(String technicalMessage) {
		this.technicalMessage = technicalMessage;
	}

}
