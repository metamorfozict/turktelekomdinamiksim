package tr.com.metamorfoz.web.shared.model.dstk.dstkinstance;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class DstkInstanceDtoSaveRequest {

	@NotNull
	private Integer instance;

	@NotBlank
	@Length(max = 100)
	private String instanceHeader;

	private Integer instanceVersion;

	private DstkInstanceJsonContentDto menuContent;

	public DstkInstanceDtoSaveRequest() {
		super();
	}
	
	public Integer getInstance() {
		return instance;
	}

	public void setInstance(Integer instance) {
		this.instance = instance;
	}

	public String getInstanceHeader() {
		return instanceHeader;
	}

	public void setInstanceHeader(String instanceHeader) {
		this.instanceHeader = instanceHeader;
	}

	public Integer getInstanceVersion() {
		return instanceVersion;
	}

	public void setInstanceVersion(Integer instanceVersion) {
		this.instanceVersion = instanceVersion;
	}

	public DstkInstanceJsonContentDto getMenuContent() {
		return menuContent;
	}

	public void setMenuContent(DstkInstanceJsonContentDto menuContent) {
		this.menuContent = menuContent;
	}



	
	
}
