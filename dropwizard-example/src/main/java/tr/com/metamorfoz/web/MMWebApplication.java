package tr.com.metamorfoz.web;

import java.util.Timer;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.common.pool.SyncConnectionPool;
import tr.com.metamorfoz.common.socket.statistic.StatisticTimerTask;
import tr.com.metamorfoz.web.core.campaign.statistic.CounterType;
import tr.com.metamorfoz.web.core.campaign.statistic.Statistic;
import tr.com.metamorfoz.web.core.common.service.utility.JsonService;
import tr.com.metamorfoz.web.core.dstk.api.DstkCampaignStatisticApiService;
import tr.com.metamorfoz.web.core.dstk.api.DstkInstanceApiService;
import tr.com.metamorfoz.web.core.dstk.api.DstkMainMenuApiService;
import tr.com.metamorfoz.web.core.dstk.api.DstkModuleApiService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkCampaignStatisticConverter;
import tr.com.metamorfoz.web.core.dstk.converter.DstkInstanceConverter;
import tr.com.metamorfoz.web.core.dstk.converter.DstkMainMenuConverter;
import tr.com.metamorfoz.web.core.dstk.converter.DstkModuleConverter;
import tr.com.metamorfoz.web.core.dstk.dao.DstkCampaignStatisticDao;
import tr.com.metamorfoz.web.core.dstk.dao.DstkInstanceDao;
import tr.com.metamorfoz.web.core.dstk.dao.DstkMainMenuDao;
import tr.com.metamorfoz.web.core.dstk.dao.DstkModuleDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkMainMenu;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkCampaignStatisticDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkInstanceDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkMainMenuDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkModuleDomainService;
import tr.com.metamorfoz.web.core.ota.api.OtaApiService;
import tr.com.metamorfoz.web.core.ota.dao.OtaSimProfileDao;
import tr.com.metamorfoz.web.core.ota.dao.OtaTransportKeyDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaSimProfile;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.core.ota.service.OtaService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaSimProfileDomainService;
import tr.com.metamorfoz.web.core.ota.service.domainservice.OtaTransportKeyDomainService;
import tr.com.metamorfoz.web.core.rfm.api.RfmApiService;
import tr.com.metamorfoz.web.core.rfm.dao.RfmGsmOperatorDao;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;
import tr.com.metamorfoz.web.core.rfm.service.RfmService;
import tr.com.metamorfoz.web.core.rfm.service.domainservice.RfmGsmOperatorDomainService;
import tr.com.metamorfoz.web.core.stup.api.StupRuleApiService;
import tr.com.metamorfoz.web.core.stup.dao.StupRuleDao;
import tr.com.metamorfoz.web.core.stup.domain.StupRule;
import tr.com.metamorfoz.web.core.stup.service.StupRuleService;
import tr.com.metamorfoz.web.core.stup.service.domainservice.StupRuleDomainService;
import tr.com.metamorfoz.web.ui.exception.mapper.ConstraintViolationExceptionMapper;
import tr.com.metamorfoz.web.ui.exception.mapper.MMRecordNotFoundExceptionMapper;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkCampaignStatisticResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkInstanceResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkMainMenuResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkModuleResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkModuleStupResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkModuleTestResource;
import tr.com.metamorfoz.web.ui.resource.dstk.DstkThemeResource;
import tr.com.metamorfoz.web.ui.resource.ota.OtaTransportKeyResource;
import tr.com.metamorfoz.web.ui.resource.rfm.RfmGsmOperatorResource;
import tr.com.metamorfoz.web.ui.resource.standart.ValidationResource;
import tr.com.metamorfoz.web.ui.resource.stup.StupRuleResource;

import com.example.helloworld.cli.RenderCommand;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MMWebApplication extends Application<MMWebConfiguration> {
	
    public static void main(String[] args) throws Exception {
        new MMWebApplication().run(args);
    }

    private final HibernateBundle<MMWebConfiguration> hibernateBundle = new HibernateBundle<MMWebConfiguration>(DstkCampaignStatistic.class
    																										   , DstkInstance.class
    																										   , DstkModule.class
    																										   , DstkMainMenu.class
    																										   , RfmGsmOperator.class
    																										   , OtaSimProfile.class
    																										   , OtaTransportKey.class
    																										   , StupRule.class) {
    	
                @Override
                public DataSourceFactory getDataSourceFactory(MMWebConfiguration configuration) {
                	DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
                	dataSourceFactory.setAutoCommitByDefault(true);
                    return dataSourceFactory;
                }
                
               
            };

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<MMWebConfiguration> bootstrap) {
        bootstrap.addCommand(new RenderCommand());
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new MigrationsBundle<MMWebConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(MMWebConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
//        bootstrap.addBundle(new ViewBundle());
    }

    @Override
    public void run(MMWebConfiguration configuration, Environment environment) throws ClassNotFoundException {   	
       
    	ObjectMapper objectMapper = environment.getObjectMapper();
    	
    	configureJersey(environment);
    	configureJackson(objectMapper);
    	configureExceptionMappers(environment);
    	
    	SessionFactory sessionFactory = hibernateBundle.getSessionFactory();

    	/***********   Utility Services *****************/
    	
    	final JsonService jsonService = new JsonService(objectMapper);
    	
    	/***********   DSTK  *****************/
        final DstkInstanceDao dstkInstanceDao = new DstkInstanceDao(sessionFactory);
        final DstkModuleDao dstkModuleDao = new DstkModuleDao(sessionFactory);
        final DstkCampaignStatisticDao dstkCampaignStatisticDao = new DstkCampaignStatisticDao(sessionFactory);
      
		final DstkInstanceDomainService dstkInstanceDomainService = new DstkInstanceDomainService(dstkInstanceDao);
		final DstkModuleDomainService dstkModuleDomainService = new DstkModuleDomainService(dstkModuleDao);
		
        final DstkInstanceConverter converter = new DstkInstanceConverter();
		final DstkInstanceApiService dstkInstanceApiDtoService = new DstkInstanceApiService(dstkInstanceDomainService, converter, dstkModuleDomainService, jsonService);

		final DstkCampaignStatisticDomainService dstkCampaignStatisticDomainService = new DstkCampaignStatisticDomainService(dstkCampaignStatisticDao);
		final DstkCampaignStatisticConverter dstkCampaignStatisticConverter = new DstkCampaignStatisticConverter();
		final DstkCampaignStatisticApiService dstkCampaignStatisticApiDtoService = new DstkCampaignStatisticApiService(dstkCampaignStatisticDomainService, dstkCampaignStatisticConverter);
		final DstkModuleConverter dstkModuleConverter = new DstkModuleConverter();
		final DstkModuleApiService dstkModuleApiService = new DstkModuleApiService(dstkModuleDomainService, dstkModuleConverter, jsonService);
		
		final DstkMainMenuDao dstkMainMenuDao = new DstkMainMenuDao(sessionFactory);
		final DstkMainMenuDomainService dstkMainMenuDomainService = new DstkMainMenuDomainService(dstkMainMenuDao);
		final DstkMainMenuConverter dstkMainMenuConverter = new DstkMainMenuConverter();
		final DstkMainMenuApiService dstkMainMenuApiService = new DstkMainMenuApiService(dstkMainMenuDomainService, dstkMainMenuConverter, jsonService);
		
		
    	/***********   DSTK  RESOURCES *****************/
		environment.jersey().register(new DstkThemeResource(null));
		environment.jersey().register(new DstkInstanceResource(dstkInstanceApiDtoService));
		environment.jersey().register(new DstkModuleResource(dstkModuleApiService));
		environment.jersey().register(new DstkModuleTestResource(dstkModuleApiService));
		environment.jersey().register(new DstkModuleStupResource(dstkModuleApiService));
		environment.jersey().register(new DstkMainMenuResource(dstkMainMenuApiService));
		
		
    	/***********   OTA   *****************/
    	final OtaSimProfileDao simProfileDao = new OtaSimProfileDao(sessionFactory);
    	
    	final OtaTransportKeyDao transportKeyDao = new OtaTransportKeyDao(sessionFactory);
    	final OtaTransportKeyDomainService otaTransportKeyDomainService = new OtaTransportKeyDomainService(transportKeyDao);
    	final OtaSimProfileDomainService otaSimProfileDomainService = new OtaSimProfileDomainService(simProfileDao);
    	
    	final OtaService otaDomainService = new OtaService(dstkInstanceDomainService
    													 , dstkModuleDomainService
    													 , otaSimProfileDomainService
    													 , otaTransportKeyDomainService);
    	
    	final OtaApiService otaApiService = new OtaApiService(otaDomainService);
    	environment.jersey().register(new OtaTransportKeyResource(otaApiService));
    	
    	/***********   RFM   *****************/
    	final RfmGsmOperatorDao rfmGsmOperatorDao = new RfmGsmOperatorDao(sessionFactory);
    	final RfmGsmOperatorDomainService rfmGsmOperatorDomainService = new RfmGsmOperatorDomainService(rfmGsmOperatorDao);
    	final RfmService rfmService = new RfmService(rfmGsmOperatorDomainService);
    	final RfmApiService rfmApiService = new RfmApiService(rfmService);
    	
    	environment.jersey().register(new RfmGsmOperatorResource(rfmApiService));
    	
    	/***********   OTA RESOURCES *****************/				
		
		environment.jersey().register(new DstkCampaignStatisticResource(dstkCampaignStatisticApiDtoService));
		
		/***********   SIMPLE TOPUP RESOURCES *****************/	
		final StupRuleDao stupRuleDao = new StupRuleDao(sessionFactory);
    	final StupRuleDomainService stupRuleDomainService = new StupRuleDomainService(stupRuleDao);
    	final StupRuleService stupRuleService = new StupRuleService(stupRuleDomainService);
    	final StupRuleApiService stupRuleApiService = new StupRuleApiService(stupRuleService);
    	
    	environment.jersey().register(new StupRuleResource(stupRuleApiService));
		
		
		/***********   OTHER RESOURCES *****************/
    	//========================================================================================================
		// TODO : Change to configuration values...
		//========================================================================================================
		final SyncConnectionPool turkcellDialogService = new SyncConnectionPool("tr.com.metamorfoz.web.core.dstk.remote.turkcell.ClientInterfacePortConnection" // connection pool class name
																					  , 1 	 // Initial pool size
																					  , 1    // Max pool size
																					  , 180  // Response port timeout
																					  , 1800 // Reap timeout
																					  , 1800 // Port validity check period
																					  , null
																				  , new String[] {"tr.com.turkcell.sdp.wsdl.generated.TSOSENDSERVICEREQ_Service" // service class name
																								 , "URL" 									   // url
																								 , "http://sdp.turkcell.com.tr/wsdl/generated" // target name space
																								 , "TSO_SEND_SERVICE_REQ"                      // function name
																					  				 , "getTSOSENDSERVICEREQImplPort"		   // port name 
																					  				 , null
																					  				 , "1"}
																				   , 0
																				   , null);
		
		//========================================================================================================
		// TODO : Add active campaigns to statistics LRU cache...
		//        Statistic.addToStatistics(serviceId);
		//        Statistic.incrementCounters(5000, CounterType.DELIVERED);
		//========================================================================================================
		
		//========================================================================================================
		// TODO : Timer for Statistics Object, get delay from configuration.
		//========================================================================================================		
		// new Timer().schedule(new StatisticTimerTask(), 3600000);
		
        environment.jersey().register(new ValidationResource());
        
        
    }


	private void configureJersey(Environment environment) {
		
	}

	private void configureExceptionMappers(Environment environment) {
		 environment.jersey().register(new ConstraintViolationExceptionMapper());
		 environment.jersey().register(new MMRecordNotFoundExceptionMapper());
//		 environment.jersey().register(new GenericExceptionMapper());
		 
		 
	}

	private void configureJackson(ObjectMapper objectMapper) {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}
	
}
