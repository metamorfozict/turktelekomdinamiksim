package tr.com.metamorfoz.web.shared.model.dstk.dstkmodule;

import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class DstkModuleDtoSaveRequest {

	private Integer id;
	
	@Length(max = 30)
	@Column(length=30)
	private String moduleName;
	
	@NotNull
	private Integer type;
	
	private Integer moduleId;
	private List<DstkModuleDtoItem> items;
	
	public DstkModuleDtoSaveRequest() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public List<DstkModuleDtoItem> getItems() {
		return items;
	}
	public void setItems(List<DstkModuleDtoItem> items) {
		this.items = items;
	}
	
	
	
}
