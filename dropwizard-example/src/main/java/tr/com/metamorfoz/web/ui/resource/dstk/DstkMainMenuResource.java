package tr.com.metamorfoz.web.ui.resource.dstk;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.dstk.api.DstkMainMenuApiService;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu.DstkMainMenuDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu.DstkMainMenuDtoSaveRequest;

@Path("/api/dstkmainmenu")
@Produces(MediaType.APPLICATION_JSON)
public class DstkMainMenuResource {

	DstkMainMenuApiService dstkMainMenuApiService;
	
	public DstkMainMenuResource(DstkMainMenuApiService dstkMainMenuApiService) {
		super();
		this.dstkMainMenuApiService = dstkMainMenuApiService;
	}

	@GET
	@UnitOfWork
	public List<DstkMainMenuDto> findAll(){
		return dstkMainMenuApiService.findAll();
	}

	@POST
	@UnitOfWork
	public DstkMainMenuDto create(@NotNull @Valid DstkMainMenuDtoSaveRequest request) {
		return dstkMainMenuApiService.saveOrUpdate(null, request);
	}

	@PUT
	@Path("{id}")
	@UnitOfWork
	public DstkMainMenuDto update(@PathParam("id") int id, @Valid DstkMainMenuDtoSaveRequest request) {
		return dstkMainMenuApiService.saveOrUpdate(id, request);
	}

	@GET
	@Path("{id}")
	@UnitOfWork
	public DstkMainMenuDto find(@PathParam("id") int id) {
		return dstkMainMenuApiService.findSafely(id);
	}
	
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		dstkMainMenuApiService.deleteAll(ids);
	}
	
}