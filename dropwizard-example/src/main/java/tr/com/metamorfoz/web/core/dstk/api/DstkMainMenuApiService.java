package tr.com.metamorfoz.web.core.dstk.api;

import java.util.Date;

import tr.com.metamorfoz.web.core.common.api.AbstractApiDtoCrudService;
import tr.com.metamorfoz.web.core.common.service.utility.JsonService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkMainMenuConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkMainMenu;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkMainMenuDomainService;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu.DstkMainMenuDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu.DstkMainMenuDtoSaveRequest;

public class DstkMainMenuApiService extends AbstractApiDtoCrudService<DstkMainMenu, DstkMainMenuDto, DstkMainMenuDomainService, DstkMainMenuConverter> {
	
	private JsonService jsonService;

	public DstkMainMenuApiService(DstkMainMenuDomainService dstkMainMenuDomainService, DstkMainMenuConverter dstkMainMenuConverter, JsonService jsonService) {
		super(dstkMainMenuDomainService, dstkMainMenuConverter);
		this.jsonService = jsonService;
	}

	public DstkMainMenuDto saveOrUpdate(Integer id, DstkMainMenuDtoSaveRequest request) {
		
		DstkMainMenu dstkMainMenu;
		if (id != null) {
			dstkMainMenu = getDomainCrudService().findSafely(id);
		} else {
			dstkMainMenu = new DstkMainMenu();
			dstkMainMenu.setCreationTime(new Date());
		}
		
		dstkMainMenu.setMainMenuHeader(request.getTitle());
		dstkMainMenu = getDomainCrudService().save(dstkMainMenu);
		String json = jsonService.toJson(request);
		dstkMainMenu.setJson(json);
		return getConverter().convertToDto(dstkMainMenu);
	}


	
	
}
