package tr.com.metamorfoz.web.ui.resource.stup;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.caching.CacheControl;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.stup.api.StupRuleApiService;
import tr.com.metamorfoz.web.shared.model.stup.StupRuleDto;

import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;

@Path("/api/stuprule")
@Produces(MediaType.APPLICATION_JSON)
public class StupRuleResource {

	private StupRuleApiService stupRuleApiService;
	
	public StupRuleResource(StupRuleApiService stupRuleApiService) {
		super();
		this.stupRuleApiService = stupRuleApiService;
	}
	
	@GET
	@UnitOfWork
	public List<StupRuleDto> findAll(){
		return stupRuleApiService.findAllStupRuleDto();
	}
	
	@POST
	@UnitOfWork
	public StupRuleDto create(StupRuleDto stupRuleDto) {
		return stupRuleApiService.createStupRuleDto(stupRuleDto);
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public StupRuleDto find(@PathParam("id") int id) {
		return stupRuleApiService.findStupRuleDtoSafely(id);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public StupRuleDto update(@PathParam("id") int id, StupRuleDto stupRuleDto) {
		return stupRuleApiService.updateRuleDto(id, stupRuleDto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		stupRuleApiService.deleteAllStupRuleDto(ids);
	}
	
}
