package tr.com.metamorfoz.web.core.rfm.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

@Entity
@Table(name="TBL_OTASS_GSM_OPERATORS")
public class RfmGsmOperator extends BaseEntity {

	@Length(max = 15)	
	@Column(name="T_CODE", length=15)
	private String code;

	@Length(max = 50)
	@Column(name="T_NAME", length=50)
	private String name;
	
	@Length(max = 120)	
	@Column(name="T_DESCRIPTION", length=120)
	private String description;	
	
	public RfmGsmOperator() {
	}
	
	public void update(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
