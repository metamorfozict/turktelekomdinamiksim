package tr.com.metamorfoz.web.core.dstk.remote.turkcell;

import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import tr.com.metamorfoz.common.Message;
import tr.com.metamorfoz.common.pool.AbstractConnection;
import tr.com.metamorfoz.common.pool.ConnectionPool;

public class ClientInterfacePortConnection extends AbstractConnection {

	//==============================================================================================
	// Logger definition.
	//==============================================================================================
	private static final Logger logger = Logger.getLogger(ClientInterfacePortConnection.class);
	
	//==============================================================================================
	// members.
	//==============================================================================================
	private Object wsClientInterface = null;
	
	/**
	 * This helper function closes the proxy.
	 */
	public void closeConnection() {
		this.wsClientInterface = null;
	}

	/**
	 * This helper function returns statistics.
	 */
	public String getStatistics() {
		return "0";
	}

	/**
	 * This function encapsulates the retry mechanism specific to the message.
	 * 
	 * @param					pool					the pool where this connection belongs to.
	 * @param					connectionOrder			the rank of the connection.
	 * @return					boolean					the return value indicating if the communication channel 
	 * 													is up or down.										
	 */
	public boolean initConnection(ConnectionPool pool, int connectionOrder) {
		
		try {
			
			Object classToInvoke = this.getClass().getClassLoader().loadClass(pool.getConnectionParameters()[0]).getConstructor(URL.class, QName.class)
																							  .newInstance(new URL(pool.getConnectionParameters()[1])
																										  , new QName(pool.getConnectionParameters()[2]
																											         , pool.getConnectionParameters()[3]));
			
			this.wsClientInterface = classToInvoke.getClass().getDeclaredMethod(pool.getConnectionParameters()[4]).invoke(classToInvoke);
			
			this.pool	   = pool;
			this.inuse 	   = false;
			this.timestamp = 0;
			
			return true;
			
		} catch (Throwable t) {
			
			closeConnection();
			logger.error("Cannot open connection to address " + pool.getConnectionParameters()[1] + " , qname : " 
						 + pool.getConnectionParameters()[2] + " and function name : " + pool.getConnectionParameters()[3] + " , ", t);
			
		}
		
		return false;
		
	}

	/**
	 * This helper is not applicable for this protocol.
	 */
	public Message releaseOperationTrn(Integer arg0) {
		return null;
	}

	/**
	 * This helper function controls the sanity of the connection.
	 * 
	 * @return					boolean					returns true if connection is valid, false o/w.	
	 */
	public boolean sendHeartBeat() {
		return true;
	}

	/**
	 * This helper function controls the sanity of the connection.
	 * 
	 * @return					boolean					returns true if connection is valid, false o/w.	
	 */
	public boolean validate() {
		return true;
	}

	/**
	 * This helper function returns the connection class interface.
	 * 
	 * @return					Object			the client web service interface.
	 */
	public Object getWsClientInterface() {
		return this.wsClientInterface;
	}

}
