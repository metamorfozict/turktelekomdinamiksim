package tr.com.metamorfoz.web.ui.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tr.com.metamorfoz.web.ui.exception.dto.ErrorDto;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<RuntimeException>  {

	private static final Logger logger = LoggerFactory.getLogger(GenericExceptionMapper.class);

	@Override
	public Response toResponse(RuntimeException runtime) {

		ErrorDto errorDto = new ErrorDto(runtime.getMessage());
		Response defaultResponse = Response.serverError().entity(errorDto).build();
		
		// Use the default
		logger.error(runtime.getMessage(), runtime);
		return defaultResponse;

	}

	
	


}
