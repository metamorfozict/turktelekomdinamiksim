package tr.com.metamorfoz.web.core.rfm.dao;

import org.hibernate.SessionFactory;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.rfm.domain.RfmGsmOperator;

public class RfmGsmOperatorDao extends AbstractDao<RfmGsmOperator> {

	public RfmGsmOperatorDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

}
