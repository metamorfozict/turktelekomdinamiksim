package tr.com.metamorfoz.web.core.stup.service.domainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.stup.dao.StupRuleDao;
import tr.com.metamorfoz.web.core.stup.domain.StupRule;

public class StupRuleDomainService extends AbstractDomainCrudService<StupRule, StupRuleDao>{

	public StupRuleDomainService(StupRuleDao dao) {
		super(dao);
	}

}
