package tr.com.metamorfoz.web.core.dstk.api;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import tr.com.metamorfoz.dstk.encoder.widget.DSTKService;
import tr.com.metamorfoz.dstk.encoder.widget.EncodeUtil;
import tr.com.metamorfoz.dstk.encoder.widget.model.CallService;
import tr.com.metamorfoz.dstk.encoder.widget.model.InputTextService;
import tr.com.metamorfoz.dstk.encoder.widget.model.InputTextService.TEXT_TYPE;
import tr.com.metamorfoz.dstk.encoder.widget.model.LabelService;
import tr.com.metamorfoz.dstk.encoder.widget.model.LaunchBrowserService;
import tr.com.metamorfoz.dstk.encoder.widget.model.MenuService;
import tr.com.metamorfoz.dstk.encoder.widget.model.MenuServiceItem;
import tr.com.metamorfoz.dstk.encoder.widget.model.SMSKeyword;
import tr.com.metamorfoz.dstk.encoder.widget.model.SendSMSService;
import tr.com.metamorfoz.dstk.encoder.widget.model.Service;
import tr.com.metamorfoz.dstk.encoder.widget.model.ServiceBearerType;
import tr.com.metamorfoz.dstk.encoder.widget.model.ServiceFlow;
import tr.com.metamorfoz.web.core.common.api.AbstractApiDtoCrudService;
import tr.com.metamorfoz.web.core.common.service.utility.JsonService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkModuleConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkModuleDomainService;
import tr.com.metamorfoz.web.exception.MMBusinessException;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDtoItem;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDtoSaveRequest;
import tr.com.metamorfoz.web.utility.StringUtil;

public class DstkModuleApiService extends AbstractApiDtoCrudService<DstkModule, DstkModuleDto, DstkModuleDomainService, DstkModuleConverter> {

	DstkModuleDomainService dstkModuleDomainService;
	private JsonService jsonService;
	
	public DstkModuleApiService(DstkModuleDomainService dstkModuleDomainService, DstkModuleConverter dstkModuleConverter, JsonService jsonService) {
		super(dstkModuleDomainService, dstkModuleConverter);
		this.dstkModuleDomainService = dstkModuleDomainService;
		this.jsonService = jsonService;
	}
	
	public DstkModuleDomainService getDstkModuleDomainService() {
		return dstkModuleDomainService;
	}

	public DstkModuleDtoSaveRequest save(DstkModuleDtoSaveRequest request, String operation) {
			   
        try {
        
	        //======================================================================================================================================
	        // Parse service name and service flow JSON data.
	        //======================================================================================================================================

	        ServiceFlow serviceFlow = new ServiceFlow();
			
			serviceFlow.setFlowBearer(ServiceBearerType.SMS);
			serviceFlow.setServiceDescription("");
	        
	        //======================================================================================================================================
	        // Parse the service.
	        //======================================================================================================================================
	        parseServiceFromJsonData(serviceFlow, null, new LinkedList<String>(), request.getItems());
	        
	        serviceFlow.dispatchKeywords   (serviceFlow.getServiceList(), 0, 0, null);
	        serviceFlow.updateSelecItemKeys(serviceFlow.getServiceList());
			
			DSTKService dstkService = EncodeUtil.composeService(0, 3333, request.getModuleName(), true, false, serviceFlow);
			
			//======================================================================================================================================
	        // Store the service.
	        //======================================================================================================================================
	        
	        DstkModule module = new DstkModule();
	        module.setType(request.getType());
	        module.setModuleName(request.getModuleName());
	        module.setType(request.getType());
	        module.setJson(jsonService.toJson(request.getItems()));
	        module.setDescription(request.getModuleName());
	        			
	        module.setHexSize			   (dstkService.getHexSize());
	        module.setNbCommands		   (dstkService.getNbCommands());
	        module.setOffsets			   (dstkService.getOffset());
	        module.setTerminalModuleOffsets(dstkService.getTerminalModulesOffset());
	        module.setTerminalModules	   (dstkService.getTerminalModules());
	        module.setByteCode			   (dstkService.getByteCodeData());
			
	        
	        if ("E".equals(operation)) {
	        	// module = dstkModuleDomainService.findSafely(request.getId());
	        	module.setId(request.getId());
	        	module = dstkModuleDomainService.save(module);
	        } else {
	        	module.setCreationTime(new Date());
	        	if (request.getType() == 0) { // Push service
	        		module.setModuleId(13107);
	        	} else {
	        		
	        		int i = 1;
	        		// find an empty location and save
	        		for (; i < 16300; i++) {
	        			try {dstkModuleDomainService.findUniqueDstkModuleByModuleId(i);} catch(Throwable th) {break;}
	        		}
	        		
	        	}
	        	
	        	module = dstkModuleDomainService.save(module);
	        	
	        }
        
        } catch (Throwable t) {
        	throw new MMBusinessException("Problem while processing request, reason : " + t.getMessage(), t);
        }

        return request;
        
	}
	
	
	/**
	 * traverses the DSTK service flow object.
	 * 
	 * objects :
	 * 		- optional : playTone
	 * 		- displayText
	 * 		- getInput
	 * 		- selectItem
	 * 		- sendSms
	 * 		- setupCall
	 * 		- openBrowser
	 * 
	 * TODO : change SYSO with system loggers.
	 */
	private static void parseServiceFromJsonData(ServiceFlow serviceFlow
												, MenuServiceItem menuServiceItem
												, LinkedList<String> smsPosition
												, List<DstkModuleDtoItem> items) {
		
		//===================================================================================================
		// Parse arrays, flow services by recalling parse function.
		//===================================================================================================
	    	
    	for (DstkModuleDtoItem currentObject : items) {
    		
    			
    		    //===================================================================================================
    			// Parse flow item e.g. displayText, getInput, etc. and add to service flow.
    			//===================================================================================================
    			
    	    	Service service = null;
    	    	
            	//===================================================================================================
        		// Fill attributes of the current service according its type.
        		//===================================================================================================
    	    	String 	   type   		 = currentObject.getType();
    	    	
    	    	if ("playTone".equals(type)) {
    	    		serviceFlow.setPlayTone(true);
    	    		
    	    	} else if ("selectItem".equals(type)) {
    	    		
    	    		service = new MenuService();
    	    		
    	    		if (currentObject.getOptions() != null && currentObject.getOptions().getQuestion() != null && !StringUtil.isBlank(currentObject.getOptions().getQuestion().getValue())) {
    	    			((MenuService) service).setHeader(currentObject.getOptions().getQuestion().getValue());
        	    		System.out.println("Setting Question Value : " + currentObject.getOptions().getQuestion().getValue());
    	    		}
    	    		
    	    		
    	    		System.out.println("Adding Menu : " + ((MenuService) service).getHeader());
    	    		
    	    		boolean firstKeyword = true;
    	    		
    	    		for (DstkModuleDtoItem selectOption : currentObject.getItems()) {
    	    			
    	    			MenuServiceItem item = new MenuServiceItem();
    	    			item.setValue(selectOption.getOptions().getValue().getValue());
    	    			String key = selectOption.getOptions().getKey().getValue();
    	    			
    	    			System.out.println("Menu : " + ((MenuService) service).getHeader() + " adding item : " + item.getValue());
    	    			
    	    			if (key != null && !"".equals(key) && (selectOption.getBranch() == null || !(Boolean.parseBoolean(selectOption.getBranch())))) {
    	    				SMSKeyword smsKeyword = new SMSKeyword();
    	    				smsKeyword.setKeyWord(key);
    	    				item.setMountedService(smsKeyword);
    	    				if (firstKeyword) {
    	    					addNextItem(true, smsPosition);
    	    					firstKeyword = false;
    	    				}
    	    				System.out.println("Menu : " + ((MenuService) service).getHeader() + " item : " + item.getValue() + " adding keyword : " + smsKeyword.getKeyWord());
    	    			} else {
    	    				System.out.println("Menu : " + ((MenuService) service).getHeader() + " item : " + item.getValue() + " adding new service...");
    	    				parseServiceFromJsonData(serviceFlow, item, smsPosition, selectOption.getItems());
    	    			}
    	    			
    	    			((MenuService) service).addMenuItem(item);
    	    			
    	    		}
    	    		
    	    		
        		} else if ("displayText".equals(type)) {
        			
        			service = new LabelService();
        			if (currentObject.getOptions() != null && currentObject.getOptions().getQuestion()!= null && !StringUtil.isBlank(currentObject.getOptions().getQuestion().getValue())) {
        				((LabelService) service).setLabelText(currentObject.getOptions().getQuestion().getValue());
        				System.out.println("Adding display text with label : " + ((LabelService) service).getLabelText());
        			}
        			
        		} else if ("getInput".equals(type)) {
        			
        			service = new InputTextService();
        			((InputTextService) service).setLabel(currentObject.getOptions().getQuestion().getValue());
        			
        			String textType = "ALPHANUMERIC";
        			
        			if (currentObject.getOptions().getSelect() != null && currentObject.getOptions().getSelect().getValue() != null) {
        				textType = currentObject.getOptions().getSelect().getValue();
        			}
        			
        			((InputTextService) service).setTextType(TEXT_TYPE.valueOf(textType));
        			((InputTextService) service).setMinLength(currentObject.getOptions().getRange().get(0));
        			((InputTextService) service).setMaxLength(currentObject.getOptions().getRange().get(1));
        			addNextItem(false, smsPosition);
        			System.out.println("Adding get input  with label : " + ((InputTextService) service).getLabel() 
        							  + " type : " + ((InputTextService) service).getTextType() + " min : " + ((InputTextService) service).getMinLength()
        							  + " max : " + ((InputTextService) service).getMaxLength());
        			
        		} else if ("sendSms".equals(type)) {
        			
        			String alphaIdentifier = "Sending Message...";
        			
        			if ((currentObject.getTitle() != null) && (!"".equals(currentObject.getTitle()))) {
        				alphaIdentifier = currentObject.getTitle();
        			}
        			
        			service = new SendSMSService(alphaIdentifier);
        			
        			String staticKeyword = "";
        			
        			if ((currentObject.getKeyword() != null) && (!"".equals(currentObject.getKeyword().toString()))) {
        				staticKeyword = currentObject.getKeyword();
        			}
        			
        			((SendSMSService) service).setKeyword(staticKeyword + convertToSMSPositionString(smsPosition));
        			((SendSMSService) service).setVasId(currentObject.getDestination());
        			
        			System.out.println("Adding SMS with alpha id : " + ((SendSMSService) service).getAlphaId() + " keyword : " + ((SendSMSService) service).getKeyword()
        							  + " VAS id : " + ((SendSMSService) service).getVasId());
        			
        		} else if ("setupCall".equals(type)) {
        			
        			service = new CallService();
        			if (!StringUtil.isBlank(currentObject.getDestination())) {
        				((CallService) service).setMsisdn(currentObject.getDestination());
        				System.out.println("Adding setup call with destination : " + ((CallService) service).getMsisdn());
        			}
        			
        		} else if ("openBrowser".equals(type)) {
        			
        			service = new LaunchBrowserService();
        			if (!StringUtil.isBlank(currentObject.getUrl())) {
        				((LaunchBrowserService) service).setUrl(currentObject.getUrl());
        				System.out.println("Adding browser call with url : " + ((LaunchBrowserService) service).getUrl());
        			}
        			
        		}
    	    	
        		//===================================================================================================
        		// Add to service flow, or add as a mounted service to the menu item.
        		//===================================================================================================
        		if (service != null) {
    	    		if (menuServiceItem != null) {
    					menuServiceItem.setMountedService(service);
    					
    					System.out.println("Adding service : " + service.getClass() + " to item : " + menuServiceItem.getValue());
    					
    				} else {
    					serviceFlow.addService(service);
    					
    					System.out.println("Adding service : " + service.getClass() + " to flow");
    					
    				}
        		}
    	            

    		}
    		
	}
	        
	    
	
	/**
	 * adds the next item to the SMS position.
	 * 
	 * @param 					isSelectItem					whether "select item" or "get input".
	 * @param 					smsPosition						the smsPosition.
	 */
	private static void addNextItem(boolean isSelectItem, LinkedList<String> smsPosition) {
		
		String prefix = isSelectItem ? "SI" : "GI";
		
		//===================================================================================================
		// Count current number of items. [<SI1>] [<GI1>]
		//===================================================================================================
		int itemNo  = 1;
		int indexOf = -1;
		for (String position : smsPosition) {
			
			indexOf = position.indexOf(prefix);
			if (indexOf > -1) {
				itemNo++;
			}
			
		}
		
		smsPosition.add("[<" + prefix + itemNo + ">]");
		
	}
	
	/**
	 * composes the SMS keyword from SMS positions.
	 * 
	 * @param 					smsPosition					the SMS positions.
	 * @return					String						the SMS keyword.
	 */
	public static String convertToSMSPositionString(LinkedList<String> smsPosition) {
		
		String result = "";
		
		for(String position : smsPosition) {
			result += position + " ";
		}
		
		if (result.endsWith(" ")) {
			result = result.substring(0, result.length() - 1);
		}
		
		return result;
		
	}
	
	
}
