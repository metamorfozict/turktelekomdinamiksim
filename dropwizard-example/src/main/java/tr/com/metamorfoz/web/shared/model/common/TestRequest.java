package tr.com.metamorfoz.web.shared.model.common;

import org.hibernate.validator.constraints.NotBlank;

public class TestRequest extends BaseRequest {

	@NotBlank
	private String name;
	
	@NotBlank
	private String surname;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	@Override
	public String toString() {
		return "TestRequest [name=" + name + ", surname=" + surname + "]";
	}
}
