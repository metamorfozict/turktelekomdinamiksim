package tr.com.metamorfoz.web.utility;

import java.util.List;
import java.util.Timer;
import java.util.UUID;

import tr.com.metamorfoz.common.CommonUtil;
import tr.com.metamorfoz.dstk.encoder.command.EncoderException;
import tr.com.metamorfoz.dstk.encoder.command.SecureDataGeneratorV1;
import tr.com.metamorfoz.dstk.encoder.model.EngineVersion;
import tr.com.metamorfoz.dstk.encoder.model.ModulePosition;
import tr.com.metamorfoz.dstk.encoder.model.ModuleState;
import tr.com.metamorfoz.dstk.encoder.model.Position;
import tr.com.metamorfoz.dstk.encoder.model.ProfileConfig;
import tr.com.metamorfoz.dstk.encoder.model.ServiceDesigner;
import tr.com.metamorfoz.dstk.encoder.model.impl.ModuleImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceDesignerImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceProviderImpl;
import tr.com.metamorfoz.dstk.encoder.util.ConvType;
import tr.com.metamorfoz.otasocketinterface.ClientConnectionManager;
import tr.com.metamorfoz.otasocketinterface.ConnectionManager;
import tr.com.metamorfoz.otasocketinterface.messages.APDUSubmitRequest;
import tr.com.metamorfoz.otasocketinterface.messages.APDUSubmitRequestList;
import tr.com.metamorfoz.otasocketinterface.test.StatisticsPrintingTask;
import tr.com.metamorfoz.otasocketinterface.test.StatisticsPrintingTask2;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;

public class DSTKTest_Azerfon {

	private static final String SERVICE_ID = "SIMGOSERVICE_VP"; //"SIMGOSERVICE_TEST";//"NFC_TEST_TR";"SIMGOSERVICE_TEST"
	// private static final String SERVICE_ID = "PLT1037";
	// private static final String SERVER_SOCKET_ADDRESSES = "10.210.227.52:4321";//"10.210.227.52:4321";
	// METAMORFOZ IP
	private static final String SERVER_SOCKET_ADDRESSES = "127.0.0.1:5432";
	// private static final String SERVER_SOCKET_ADDRESSES = "10.248.67.191:5432";
	// Local IP
	// private static final String SERVER_SOCKET_ADDRESSES = "127.0.0.1:5432";
	//private static final String SERVER_SOCKET_ADDRESSES = "10.10.5.77:5434";
	private static final int MAX_IN_MESSAGE_QUEUE_SIZE = 100;
	private static final int MAX_OUT_MESSAGE_QUEUE_SIZE = 100;
	private static final int MAX_IN_MESSAGE_SPEED = 200;
	private static final int MAX_OUT_MESSAGE_SPEED = 100;
	private static final int HEART_BEAT_PERIOD = 10000;
	private static final int SOCKET_STREAM_BUFFER_SIZE = 10000;
	private static final int INACTIVE_SERVICE_CLOSING_INTERVAL = 100000;
	private static final int MAX_SERVICE_INACTIVITY_PERIOD = 100000;
	private static final int NUMBER_OF_IN_MESSAGE_CALL_BACK_THREADS = 1;
	private static final byte SERIALIZATION_MODE = ConnectionManager.CUSTOM_SERIALIZATION;

	private static final String CONNECTION_MANAGER_STORAGE_FILE = "C:\\connectionManagerStorageFile.out";

	private static final int STATISCTICS_PRINTING_PERIOD = 500000000;
	private static final int APDU_COMMAND_EXPIRY_PERIOD = 18000000;

	private static Timer statisticsTimer = new Timer(true);

	public static void sendScenario(DstkModule module) {
		
		try {

			ClientSocketConnectionEventHandler clientSocketConnectionEventHandler = new ClientSocketConnectionEventHandler();
			ClientConnectionManager ccm = new ClientConnectionManager(SERVER_SOCKET_ADDRESSES, MAX_IN_MESSAGE_QUEUE_SIZE, MAX_OUT_MESSAGE_QUEUE_SIZE, MAX_IN_MESSAGE_SPEED, MAX_OUT_MESSAGE_SPEED, HEART_BEAT_PERIOD, SOCKET_STREAM_BUFFER_SIZE, clientSocketConnectionEventHandler, INACTIVE_SERVICE_CLOSING_INTERVAL, MAX_SERVICE_INACTIVITY_PERIOD, NUMBER_OF_IN_MESSAGE_CALL_BACK_THREADS, SERIALIZATION_MODE, CONNECTION_MANAGER_STORAGE_FILE);
			ccm.start();

			StatisticsPrintingTask task1 = new StatisticsPrintingTask();
			StatisticsPrintingTask2 task2 = new StatisticsPrintingTask2(ccm);
			
			statisticsTimer.scheduleAtFixedRate(task1, STATISCTICS_PRINTING_PERIOD, STATISCTICS_PRINTING_PERIOD);
			statisticsTimer.scheduleAtFixedRate(task2, STATISCTICS_PRINTING_PERIOD, STATISCTICS_PRINTING_PERIOD);

			APDUSubmitRequestList apduSubmitRequestList = null;
			APDUSubmitRequest apduSubmitRequest = null;
			
//			String[] contentStr = new String[] {"690102F373"
//												 , "7A0213FD733333060000000A00F2013401520163017453FE733333017400000754616E6974696D003381F4DC496E7465726E6574746520677576656E6C6920616C69737665726973207961706D616B20566F6461666F6E65204365702043757A64616E20696C6520636F"
//												 , "BF0167FE7333330174004B6B206B6F6C61792E20557374656C696B20626972636F6B2066696E616E73616C2069736C656D69206465207A61686D657473697A6365207961706162696C697273696E697A2E2042696C67696C6572696E697A6920676972696E2C53616E61"
//					 						 	 , "BA0167FE733333017400AA6C20436570204E616B6974206B617274696E697A69207961726174696E2C73657276697369206B756C6C616E6D6179612068656D656E206261736C6179696EFC0542737672201CF801852C53616E616C20436570204E616B6974204B617274"
//												 , "610167FE733333017401092074616C6562696E697A20696C6574696C69796F722E2E2E86000C01000C910935230180500000FFFFFF0000F4145443204B696D6C696B204E756D6172616E697A3A0B0BFD01201D3301F4074164696E697A3A020FFD01201E1C01F40A536F"
//												 , "D50214FE73333301740168796164696E697A3A020F4D1D03F43333"};
			
			String parameter = "02BC0303111A005532340A05500000"; // 13880C091A15015232340A05500000 09C40C081A15015232340A05500000
			                    
			
			ProfileConfig config = new ProfileConfig(parameter);
			
			ModuleImpl moduleImpl = new ModuleImpl(Long.parseLong("13107") 											  
												  , "1234567"
												  , EngineVersion.V1
												  , ModuleState.ACTIVATED												  
												  , false 																  
												  , Integer.parseInt(module.getNbCommands(), 16)																	  
												  , Integer.parseInt(module.getHexSize(), 16)															  
												  , ConvType.hexStringToByteArray(module.getOffsets())					  
												  , ConvType.hexStringToByteArray(module.getByteCode())
												  , (ServiceDesigner) new ServiceDesignerImpl("0000000000000000"
														  				   					 , new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}
														  				   					 , new ServiceProviderImpl("XXX", "YYY"))
												  , "Test");

			
			
			SecureDataGeneratorV1 secureDataGeneratorV1 = new SecureDataGeneratorV1();
			secureDataGeneratorV1.initialize(106 /* MT Max Size */, 121 /* MO Max Size */, config, false);
			
			try {
				
				Position position = new ModulePosition(3, 2);
				
				// add module
				secureDataGeneratorV1.addInstallModuleCommand(position, moduleImpl);
				
				// trig module
				secureDataGeneratorV1.addStartModuleCommand(moduleImpl);
				
			} catch (EncoderException encexc) {
				encexc.printStackTrace();
			}
			
			List<byte[]> secureDataList = secureDataGeneratorV1.generateSecureDataList();
			String[] contentStr = new String[secureDataList.size() + 1];
			contentStr[0] = "000102F30A"; // 12-9 5 690102F373, 12-8 5 5E0102F368 
			
			for (int i = 0; i < secureDataList.size(); i++) {
				
				contentStr[i + 1] = CommonUtil.byteArrayToHexString(secureDataList.get(i));
				
			}
			
			for (int j = 0; j < contentStr.length; j++) { 
				
				apduSubmitRequestList = new APDUSubmitRequestList(SERVICE_ID);
								  
				byte[] content = CommonUtil.hexStringToByteArray(contentStr[j]);
				
				String msisdn = "5522305025";// TURKIYE 5360692164 5323495805  "0994502310364";

				apduSubmitRequest = new APDUSubmitRequest(
						UUID.randomUUID().toString(),
						msisdn,
						System.currentTimeMillis() + APDU_COMMAND_EXPIRY_PERIOD,
						"200101", // 300102 200101
						content
				);

				apduSubmitRequestList.addAPDUSubmitRequest(apduSubmitRequest);

				ccm.insertOutMessage(apduSubmitRequestList);
				
				System.out.println("Part No : " + j + " Content : " + apduSubmitRequest);
				
				try{Thread.sleep(15000);} catch(Throwable t) {}
				
			}
			
			try {
				task1.cancel();
				task2.cancel();
			} catch (Throwable t) {
				t.printStackTrace();
			}
			ccm.shutdown();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		byte b = (byte) 0;
		byte[] c = new byte[]{(byte) 0x01, (byte) 0x02, (byte) 0xF3, (byte) 0x0A};
		
		
		for (int i = 0; i < c.length; i++) {
			b += (c[i] & 0x00FF);
		}
		
		System.out.println(b);
		
		b = (byte) 0;
		c = new byte[]{ (byte) 0x00, (byte) 0x01, (byte) 0x10, (byte) 0xD1
					  , (byte) 0x01, (byte) 0x01, (byte) 0xFF, (byte) 0x00, (byte) 0x03
					  , (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
					  , (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
		
		
		for (int i = 1; i < c.length; i++) {
			b += (c[i] & 0x00FF);
		}
		c[0] = b;
		System.out.println(CommonUtil.byteArrayToHexString(c));
		
		
	}
	
}