package tr.com.metamorfoz.web.exception;


public class RecordNotFoundException extends MMException {

	private static final long serialVersionUID = 4489665539786381076L;
	
	public RecordNotFoundException(String message) {
		super(message);
	}
	
}