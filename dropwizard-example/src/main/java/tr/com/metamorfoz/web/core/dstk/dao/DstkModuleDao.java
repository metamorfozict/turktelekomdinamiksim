package tr.com.metamorfoz.web.core.dstk.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;

public class DstkModuleDao extends AbstractDao<DstkModule> {

	public DstkModuleDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public DstkModule findUniqueDstkModuleByName(String moduleName) {
		//Assumption:Veritabanında sadece 1 kayıt olduğu kabul edilmiştir.
		DstkModule dstkModule = (DstkModule) currentSession().createCriteria(DstkModule.class)
													.add(Restrictions.eq("moduleName", moduleName))
													.uniqueResult();
		return dstkModule;
	}
	
	public DstkModule findUniqueDstkModuleByModuleId(Integer moduleId) {
		DstkModule dstkModule = (DstkModule) currentSession().createCriteria(DstkModule.class)
													.add(Restrictions.eq("moduleId", moduleId))
													.uniqueResult();
		return dstkModule;
	}
	
	@SuppressWarnings("unchecked")
	public List<DstkModule> findUniqueDstkModuleByType(Integer type) {
		return (List<DstkModule>) currentSession().createCriteria(DstkModule.class)
												  .add(Restrictions.eq("type", type))
												  .list();
	}
	
}

