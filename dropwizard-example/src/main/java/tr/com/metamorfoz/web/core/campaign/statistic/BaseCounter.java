package tr.com.metamorfoz.web.core.campaign.statistic;

import java.util.concurrent.atomic.AtomicLong;

public class BaseCounter {

	private AtomicLong	periodicValue;
	private AtomicLong	cumulativeValue;

	/**
	 * default constructor.
	 */
	public BaseCounter() {
		this.periodicValue   = new AtomicLong(0L);
		this.cumulativeValue = new AtomicLong(0L);
	}

	/**
	 * increments both periodic and total counter by one.
	 */
	public void incrementCounters() {
		this.periodicValue.getAndIncrement();
		this.cumulativeValue.getAndIncrement();
	}

	/**
	 * increments both counters by a given specified delta value.
	 * 
	 * @param					delta					the delta value.
	 */
	public void incrementCounters(long delta) {
		this.periodicValue.getAndAdd(delta);
		this.cumulativeValue.getAndAdd(delta);
	}

	/**
	 * resets both counter.
	 */
	public void resetCounters() {
		this.periodicValue.getAndSet(0L);
		this.cumulativeValue.getAndSet(0L);
	}

	/**
	 * resets only periodic value.
	 */
	public void resetPeriodicValue() {
		this.periodicValue.getAndSet(0L);
	}

	
	//===================================================================================================================================================
	// 												GETTER & SETTER
	//===================================================================================================================================================
	public AtomicLong getPeriodicValue() {
		return periodicValue;
	}

	public AtomicLong getCumulativeValue() {
		return cumulativeValue;
	}
	
}
