package tr.com.metamorfoz.web.ui.resource.standart;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tr.com.metamorfoz.web.exception.MMRecordNotFoundException;
import tr.com.metamorfoz.web.shared.model.common.TestRequest;
import tr.com.metamorfoz.web.shared.model.dstk.DstkCampaignStatisticDto;

@Path("api/validation")
@Produces(MediaType.APPLICATION_JSON)
public class ValidationResource {

	@POST
	@Path("hello") 
	public String hello(@NotNull @QueryParam("name") String name, 
			 			@NotNull @QueryParam("surname") String surname) {
		return name + " " + surname;
	}
	
	@POST
	@Path("hello2") 
	@NotNull
	public String hello2(@Valid TestRequest testRequest) {
		return testRequest.toString();
	}
	
	@POST
	@Path("hello3") 
	public Response hello3() {
	
		TestRequest testRequest = new TestRequest();
		testRequest.setName("test");
				
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "User Name exists. Try another user name");
		map.put("testRequest", testRequest);
		return Response.status(Status.BAD_REQUEST).entity(map).build();

	}
	
	@POST
	@Path("test_record_not_found_exception") 
	public Response test_record_not_found_exception() {
		throw new MMRecordNotFoundException("Record not found.");
	}

	
	@POST
	@Path("test_constraint_violation_exception") 
	public void test_constraint_violation_exception(@Valid DstkCampaignStatisticDto dstkCampaignStatisticDto) {
		
	}

	@POST
	@Path("test_raw_json") 
	public String test_raw_json(String rawjson) {
		return rawjson;
	}
	
	//Json örneği.
//	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
//		DstkCampaignStatisticDto dto = new DstkCampaignStatisticDto();
//		dto.setDescription("desc");
//		dto.setStatus(1);
//		
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.writeValue(new File("c:\\deneme\\user.json"), dto);
//	}
	
}
	
	
