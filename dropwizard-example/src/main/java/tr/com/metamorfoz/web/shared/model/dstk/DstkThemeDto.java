package tr.com.metamorfoz.web.shared.model.dstk;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class DstkThemeDto  {

	private Integer id;
	
	@NotBlank
	@Length(max = 4000)
	private String theme;

	public DstkThemeDto() {
	}
	
	public DstkThemeDto(Integer id, String theme) {
		super();
		this.id = id;
		this.theme = theme;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	
	
}