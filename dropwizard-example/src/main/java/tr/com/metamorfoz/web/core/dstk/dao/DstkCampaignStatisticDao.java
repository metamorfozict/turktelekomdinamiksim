package tr.com.metamorfoz.web.core.dstk.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tr.com.metamorfoz.web.core.common.dao.AbstractDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;

public class DstkCampaignStatisticDao extends AbstractDao<DstkCampaignStatistic> {

	public DstkCampaignStatisticDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	public List<DstkCampaignStatistic> search(int status, String type) {
		
		if (status == 0) {
			
			return currentSession().createCriteria(DstkCampaignStatistic.class)
								   .add(Restrictions.eq("type", type))
								   .list();
			
		}
		
		if ("all".equals(type)) {
			
			return currentSession().createCriteria(DstkCampaignStatistic.class)
								   .add(Restrictions.eq("status", status))
								   .list();
			
		}
		
		return currentSession().createCriteria(DstkCampaignStatistic.class)
						.add(Restrictions.eq("status", status))
						.add(Restrictions.eq("type", type))
						.list();
	}

	
}
