package tr.com.metamorfoz.web.core.stup.api;

import java.util.List;

import tr.com.metamorfoz.web.core.common.converter.EntityConverterUtil;
import tr.com.metamorfoz.web.core.stup.converter.StupRuleConverter;
import tr.com.metamorfoz.web.core.stup.domain.StupRule;
import tr.com.metamorfoz.web.core.stup.service.StupRuleService;
import tr.com.metamorfoz.web.shared.model.stup.StupRuleDto;


public class StupRuleApiService {

	private StupRuleService stupRuleService;
	
	private StupRuleConverter stupRuleDtoConverter = new StupRuleConverter();
	
	public StupRuleApiService(StupRuleService stupRuleService) {
		super();
		this.stupRuleService = stupRuleService;
	}

	public StupRuleDto createStupRuleDto(StupRuleDto stupRuleDto) {
		//Assumption:Validation should be done at ui.
		StupRule stupRule = stupRuleDtoConverter.createEntity(stupRuleDto);
		stupRule = stupRuleService.getStupRuleDomainService().save(stupRule);
		StupRuleDto stupRDto = stupRuleDtoConverter.convertToDto(stupRule);
		return stupRDto;
	}

	public StupRuleDto updateRuleDto(int id, StupRuleDto stupRuleDto) {
		StupRule stupRule = stupRuleService.getStupRuleDomainService().findSafely(id);
		stupRuleDtoConverter.updateEntity(stupRule, stupRuleDto);
//		transportKey.update(transportKeyDto.getNo(), transportKeyDto.getValue(), transportKeyDto.getAlgorithmName(), transportKeyDto.getAlgorithmMode());
		stupRule = stupRuleService.getStupRuleDomainService().save(stupRule);
		return stupRuleDtoConverter.convertToDto(stupRule);
	}
	
	public List<StupRuleDto> findAllStupRuleDto(){
		List<StupRule> stupRules = stupRuleService.getStupRuleDomainService().findAll();
		return EntityConverterUtil.convertToDtoList(stupRuleDtoConverter, stupRules);
	}

	public StupRuleDto findStupRuleDtoSafely(int id)   {
		StupRule stupRule = stupRuleService.getStupRuleDomainService().find(id);
		return stupRuleDtoConverter.convertToDto(stupRule);
	}
	
	public void deleteGsmOperatorDto(int id) {
		StupRule stupRule = stupRuleService.getStupRuleDomainService().findSafely(id);
		stupRuleService.getStupRuleDomainService().delete(stupRule);
	}
	
	public int deleteAllStupRuleDto(List<Integer> ids) {
		return stupRuleService.getStupRuleDomainService().deleteAll(ids);
	}
	

}
