package tr.com.metamorfoz.web.core.dstk.api;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import tr.com.metamorfoz.dstk.encoder.command.EncoderException;
import tr.com.metamorfoz.dstk.encoder.command.SecureDataGeneratorV1;
import tr.com.metamorfoz.dstk.encoder.model.DCS;
import tr.com.metamorfoz.dstk.encoder.model.EngineVersion;
import tr.com.metamorfoz.dstk.encoder.model.MenuPosition;
import tr.com.metamorfoz.dstk.encoder.model.ModulePosition;
import tr.com.metamorfoz.dstk.encoder.model.ModuleState;
import tr.com.metamorfoz.dstk.encoder.model.Position;
import tr.com.metamorfoz.dstk.encoder.model.ProfileConfig;
import tr.com.metamorfoz.dstk.encoder.model.ServiceDesigner;
import tr.com.metamorfoz.dstk.encoder.model.TerminalModulePosition;
import tr.com.metamorfoz.dstk.encoder.model.impl.ModuleImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceDesignerImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceProviderImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.TerminalModuleImpl;
import tr.com.metamorfoz.dstk.encoder.util.ConvType;
import tr.com.metamorfoz.web.core.common.api.AbstractApiDtoCrudService;
import tr.com.metamorfoz.web.core.common.service.utility.JsonService;
import tr.com.metamorfoz.web.core.dstk.converter.DstkInstanceConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkInstanceDomainService;
import tr.com.metamorfoz.web.core.dstk.service.domainservice.DstkModuleDomainService;
import tr.com.metamorfoz.web.core.ota.constants.OtaCampaignConstants;
import tr.com.metamorfoz.web.exception.MMRecordNotFoundException;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceDtoSaveRequest;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceJsonContentDto;
import tr.com.metamorfoz.web.shared.model.dstk.dstkinstance.DstkInstanceJsonContentItemDto;
import tr.com.metamorfoz.web.utility.CommonUtil;

public class DstkInstanceApiService extends AbstractApiDtoCrudService<DstkInstance, DstkInstanceDto, DstkInstanceDomainService, DstkInstanceConverter> {

	private DstkModuleDomainService dstkModuleDomainService;
	private JsonService jsonService;
	
	public DstkInstanceApiService(DstkInstanceDomainService dstkInstanceDomainService, DstkInstanceConverter dstkInstanceConverter
								, DstkModuleDomainService dstkModuleDomainService, JsonService jsonService) {
		super(dstkInstanceDomainService, dstkInstanceConverter);
		this.dstkModuleDomainService = dstkModuleDomainService;
		this.jsonService = jsonService;
	}

	public DstkInstanceDto save(Integer id, DstkInstanceDtoSaveRequest request, String operation) {
		
		DstkInstance dstkInstance = null;
		
		if (id != null) {
			dstkInstance = getDomainCrudService().findSafely(id);
		} else {
			dstkInstance = getDomainCrudService().findUniqueDstkInstanceByVersion(1, "DEFAULT", 1);
		}
		//TODO:Veritabanında kayıt olmadığı için geçici olarak kapalı, normalde açık olmalı.
		if (dstkInstance == null) {
			throw new MMRecordNotFoundException("Record not found for, instance:" + request.getInstance() + " instanceHeader:" + request.getInstanceHeader() + " instanceVersion:" + request.getInstanceVersion());
		}

		try {
			
			LinkedList<String> result = parseMenuFromJsonData(OtaCampaignConstants.DSTK_MOBILE_ERR_DISP
															, dstkInstance.getProfile()
															, dstkInstance.getIntrBlock()
															, request.getMenuContent());
			
			dstkInstance.setInstanceHeader(request.getMenuContent().getTitle());
			String content = "";
	        
	        for (String apdu : result) {
	        	content += apdu + OtaCampaignConstants.DSTK_COMMAND_SEPARATOR;
	        }
	        
	        content = content.substring(0, content.length() - 1);
	        
	        dstkInstance.setJson(jsonService.toJson(request.getMenuContent()));
        	dstkInstance.setContent(content);
	        
	        //======================================================================================================================================
	        // Update/insert DB according the operation type.
	        //======================================================================================================================================
	        if ("E".equals(operation)) {
	        	
	        	dstkInstance.setId(id);
	        	dstkInstance.setModificationTime(new Date());
	        	getDomainCrudService().save(dstkInstance);
	        	
	        } else {
	        	DstkInstance dstkInstanceToSave = new DstkInstance();
	        	
	        	dstkInstanceToSave.setCreationTime(new Date());
	        	dstkInstanceToSave.setAppId(dstkInstance.getAppId());
	        	dstkInstanceToSave.setContent(dstkInstance.getContent());
	        	dstkInstanceToSave.setInstance(dstkInstance.getInstance());
	        	dstkInstanceToSave.setInstanceHeader(dstkInstance.getInstanceHeader());
	        	dstkInstanceToSave.setInstanceVersion(dstkInstance.getInstanceVersion());
	        	dstkInstanceToSave.setIntrBlock(dstkInstance.getIntrBlock());
	        	dstkInstanceToSave.setJson(dstkInstance.getJson());
	        	dstkInstanceToSave.setModificationTime(new Date());
	        	dstkInstanceToSave.setProfile(dstkInstance.getProfile());
	        	dstkInstanceToSave.setTar(dstkInstance.getTar());
	        	
	        	getDomainCrudService().save(dstkInstanceToSave);
	        	
	        }

	        
			
		} catch (EncoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * traverses the DSTK menu flow object.
	 * 
	 * @param				object						the JSON object
	 * @param				List of APDUs				the list of APDUs
	 * 
	 * TODO : change SYSO with system loggers.
	 * @throws EncoderException 
	 */
	private LinkedList<String> parseMenuFromJsonData(String errorMessage
													, String applicationSpecificData
													, String instrospectionBlock
													, DstkInstanceJsonContentDto contentDto) throws EncoderException {
		
		LinkedList<String> result 		   = new LinkedList<String>();
		LinkedList<String> terminalModules = new LinkedList<String>(); 
		
		//===================================================================================================
		// Parse arrays, flow services by recalling parse function.
		//===================================================================================================
		String applicationName = contentDto.getTitle();
	    
	    System.out.println("applicationName:" + applicationName);
	    
	    if (applicationName == null || "".equals("applicationName")) {
	    	return null;
	    }
	    
	    //===================================================================================================
	    // Add static content. 
	    //===================================================================================================
	    ProfileConfig config = new ProfileConfig(applicationSpecificData);
	    
	    SecureDataGeneratorV1 secureDataGeneratorV1 = new SecureDataGeneratorV1();
		secureDataGeneratorV1.initialize(OtaCampaignConstants.DSTK_MAX_CNTNT_LEN_MT
										, OtaCampaignConstants.DSTK_MAX_CNTNT_LEN_MO
										, config
										, false);
	    
		secureDataGeneratorV1.addResetCommand();
		secureDataGeneratorV1.addSetDisplayErrorMobileCommand(errorMessage, DCS.SMS_ALPHABET);
		secureDataGeneratorV1.addSetLicenseKeyCommand(new byte[]{(byte) 0x00});
		secureDataGeneratorV1.addSetApplicationNameCommand(applicationName, DCS.SMS_ALPHABET);
		secureDataGeneratorV1.addSetIntrospectionBloc0348Command(CommonUtil.hexStringToByteArray(instrospectionBlock));
		
	    //===================================================================================================
	    // parse menu items. 
	    //===================================================================================================
		List<DstkInstanceJsonContentItemDto> menuItems    = contentDto.getItems();
		int 	  firstLvlIdx  = 1;
		int 	  scndLvlIdx   = 1;
		
		for (DstkInstanceJsonContentItemDto menuItem : menuItems) {
			
			//===================================================================================================
		    // ASSUMPTION : menu item can only have one menu deeper.
			//				check the next level to understand whether module or menu item.
		    //===================================================================================================
			boolean   isModule = true;
			List<DstkInstanceJsonContentItemDto> submenus = menuItem.getItems(); 
			System.out.println("submenus:" + submenus);
			
			if ( !submenus.isEmpty() ) {
				isModule = false;
			}
			
			//===================================================================================================
		    // get module info from DB. 
		    //===================================================================================================
			DstkModule module = null;
			
			if (isModule) {
				
				String moduleName = menuItem.getFunction();
				module = dstkModuleDomainService.findUniqueDstkModuleByName(moduleName);

				//===================================================================================================
			    // add module. 
			    //===================================================================================================
				ModuleImpl moduleImpl = new ModuleImpl(module.getId()											  
													  , module.getDescription()
													  , EngineVersion.V1
													  , ModuleState.ACTIVATED												  
													  , false 																  
													  , Integer.parseInt(module.getNbCommands())																	  
													  , Integer.parseInt(module.getHexSize(), 16) 																  
													  , ConvType.hexStringToByteArray(module.getOffsets())		
													  //TODO:Buradaki blob byte Array'e dönüştürülecek.
													  , ConvType.hexStringToByteArray(module.getByteCode().toString())
													  , (ServiceDesigner) new ServiceDesignerImpl("0000000000000000"
															  				   					 , new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}
															  				   					 , new ServiceProviderImpl("XXX", "YYY"))
													  , "Test");
				
				Position position = new ModulePosition(firstLvlIdx);
				
				secureDataGeneratorV1.addInitialisationModuleCommand(position, moduleImpl);
				secureDataGeneratorV1.addInstallModuleCommand		(position, moduleImpl);
				
				//===================================================================================================
			    // add terminal modules. 
			    //===================================================================================================
				if (module.getTerminalModules() != null && !"".equals(module.getTerminalModules())) {
					int idx = 0;
					int len = module.getTerminalModules().length();
					
					for (; idx < len; idx = idx + 4) {
						terminalModules.add(module.getTerminalModules().substring(idx, idx + 4));
					}
				}
				
			} else {
				
				//===================================================================================================
			    // add header. 
			    //===================================================================================================
				String title = menuItem.getTitle();
				secureDataGeneratorV1.addSetMenuCommand(new MenuPosition(firstLvlIdx), title, DCS.SMS_ALPHABET);
				
				//===================================================================================================
			    // process sub menus. 
			    //===================================================================================================
				scndLvlIdx  = 1;
				
				for (DstkInstanceJsonContentItemDto submenu : submenus) {
					
					//===================================================================================================
				    // get menu item from DB.
				    //===================================================================================================
					String moduleName = submenu.getFunction();
					module = dstkModuleDomainService.findUniqueDstkModuleByName(moduleName);
					
					ModuleImpl moduleImpl = new ModuleImpl(module.getId()											  
														  , module.getDescription()
														  , EngineVersion.V1
														  , ModuleState.ACTIVATED												  
														  , false 																  
														  , Integer.parseInt(module.getNbCommands())																	  
														  , Integer.parseInt(module.getHexSize(), 16) 																  
														  , ConvType.hexStringToByteArray(module.getOffsets())		
														  //TODO:Buradaki blob byte Array'e dönüştürülecek.
														  , ConvType.hexStringToByteArray(module.getByteCode().toString())
														  , (ServiceDesigner) new ServiceDesignerImpl("0000000000000000"
																  				   					 , new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}
																  				   					 , new ServiceProviderImpl("XXX", "YYY"))
														  , "Test");
					
					
					Position position = new ModulePosition(firstLvlIdx, scndLvlIdx);
					
					secureDataGeneratorV1.addInstallModuleCommand(position, moduleImpl);	
					
					//===================================================================================================
				    // add terminal modules. 
				    //===================================================================================================
					if (module.getTerminalModules() != null && !"".equals(module.getTerminalModules())) {
						int idx = 0;
						int len = module.getTerminalModules().length();
						
						for (; idx < len; idx = idx + 4) {
							terminalModules.add(module.getTerminalModules().substring(idx, idx + 4));
						}
					}
					
					scndLvlIdx++;
				}
				
			}
			
			firstLvlIdx++;
			
			
		}
	    
		//===================================================================================================
	    // process terminal modules.
	    //===================================================================================================
		LinkedList<String> processedModules = new LinkedList<String>();
		int 			   trmnlMdlIdx 		= 1;
		boolean 		   processed 		= false;
		for (String terminalModule : terminalModules) {
			
			processed = false;
			
			for (String processedModule : processedModules) {
				if (processedModule.equals(terminalModule)) {
					processed = true;
					break;
				}
			}
			
			if (!processed) {
				
				processedModules.add(terminalModule);
				
				DstkModule module = dstkModuleDomainService.findUniqueDstkModuleByName(terminalModule);				
				
				TerminalModuleImpl moduleImpl = new TerminalModuleImpl(module.getId()											  
																	  , module.getDescription()
																	  , EngineVersion.V1
																	  , ModuleState.ACTIVATED												  
																	  , false 																  
																	  , Integer.parseInt(module.getNbCommands())																	  
																	  , Integer.parseInt(module.getHexSize(), 16) 																  
																	  , ConvType.hexStringToByteArray(module.getOffsets())	
																	  //TODO:Buradaki blob byte Array'e dönüştürülecek.
																	  , ConvType.hexStringToByteArray(module.getByteCode().toString())
																	  , (ServiceDesigner) new ServiceDesignerImpl("0000000000000000"
																			  				   					 , new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}
																			  				   					 , new ServiceProviderImpl("XXX", "YYY"))
																	  , "Test");	

				TerminalModulePosition position = new TerminalModulePosition(trmnlMdlIdx);

				secureDataGeneratorV1.addInitialisationModuleCommand(position, moduleImpl);
				secureDataGeneratorV1.addInstallModuleCommand		(position, moduleImpl);
				
				trmnlMdlIdx++;
				
			}
			
		}
		
		//===================================================================================================
	    // convert to string list.
	    //===================================================================================================
		for (byte[] apdu : secureDataGeneratorV1.generateSecureDataList()) {
			result.add(CommonUtil.byteArrayToHexString(apdu));
			System.out.println(CommonUtil.byteArrayToHexString(apdu));
		}
		
		return result;

	}
	


}
