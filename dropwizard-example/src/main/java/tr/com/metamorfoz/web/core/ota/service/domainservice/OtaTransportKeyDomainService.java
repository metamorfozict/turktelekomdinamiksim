package tr.com.metamorfoz.web.core.ota.service.domainservice;

import java.util.List;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.ota.dao.OtaTransportKeyDao;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;

public class OtaTransportKeyDomainService extends AbstractDomainCrudService<OtaTransportKey, OtaTransportKeyDao>{

	public OtaTransportKeyDomainService(OtaTransportKeyDao dao) {
		super(dao);
	}

	public List<OtaTransportKey> search(String status) {
		return getDao().search(status);
	}

	
}
