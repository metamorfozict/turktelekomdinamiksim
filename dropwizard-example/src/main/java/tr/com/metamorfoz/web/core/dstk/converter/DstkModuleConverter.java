package tr.com.metamorfoz.web.core.dstk.converter;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmodule.DstkModuleDto;

public class DstkModuleConverter extends AbstractEntityConverter<DstkModule, DstkModuleDto> {


	@Override
	public DstkModule createEntity(final DstkModuleDto dto) {
		DstkModule entity = new DstkModule();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(final DstkModule entity, DstkModuleDto dto) {
		entity.setByteCode(dto.getByteCode());
		entity.setDescription(dto.getDescription());
		entity.setHexSize(dto.getHexSize());
		entity.setId(dto.getId());
		entity.setModuleId(dto.getModuleId());
		entity.setModuleName(dto.getModuleName());
		entity.setNbCommands(dto.getNumOfCommands());
		entity.setOffsets(dto.getOffsets());
		entity.setTerminalModuleOffsets(dto.getTerminalModuleOffsets());
		entity.setTerminalModules(dto.getTerminalModules());
		entity.setType(dto.getType());
	}

	@Override
	public DstkModuleDto convertToDto(final DstkModule entity) {
		DstkModuleDto dto = new DstkModuleDto(entity.getId(), entity.getModuleId(), entity.getModuleName(), entity.getByteCode(), entity.getTerminalModules()
											 , entity.getTerminalModuleOffsets(), entity.getOffsets(), entity.getHexSize(), entity.getNbCommands()
											 , entity.getJson(), entity.getDescription(), entity.getType(), entity.getCreationTime(), entity.getModificationTime());
		return dto;
	}

}
