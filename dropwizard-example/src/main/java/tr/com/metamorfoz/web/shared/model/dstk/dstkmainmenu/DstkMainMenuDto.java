package tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;


public class DstkMainMenuDto {
	
	private Integer id;

	@NotBlank
	@Length(max = 25)
	private String mainMenuHeader;
	
	private String json;
	
	@NotNull
	private Integer mainMenuVersion;
	
	private Date modificationTime;

	public DstkMainMenuDto() {
		super();
	}

	public String getMainMenuHeader() {
		return mainMenuHeader;
	}

	public void setMainMenuHeader(String mainMenuHeader) {
		this.mainMenuHeader = mainMenuHeader;
	}


	public Integer getMainMenuVersion() {
		return mainMenuVersion;
	}

	public void setMainMenuVersion(Integer mainMenuVersion) {
		this.mainMenuVersion = mainMenuVersion;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
	public String getName() {
		return this.mainMenuHeader;
	}
	
	public String getStrType() {
		return ""+ this.mainMenuVersion;
	}
	
	public Date getModificationTime() {
		return this.modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}