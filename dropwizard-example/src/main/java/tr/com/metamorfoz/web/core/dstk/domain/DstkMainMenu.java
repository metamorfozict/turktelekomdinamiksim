package tr.com.metamorfoz.web.core.dstk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

@Entity
@Table(name="TBL_OTASS_DSTK_MAIN_MENUS")
public class DstkMainMenu extends BaseEntity {

	@NotBlank
	@Length(max = 25)
	@Column(name="T_PK_MAIN_MENU", length=25)
	private String mainMenuHeader;
	
	@Lob
	@Column(name="T_JSON")
	private String json;
	
	@NotNull
	@Column(name="N_PK_VERSION")
	private Integer mainMenuVersion;

	public DstkMainMenu() {
		super();
	}

	public String getMainMenuHeader() {
		return mainMenuHeader;
	}

	public void setMainMenuHeader(String mainMenuHeader) {
		this.mainMenuHeader = mainMenuHeader;
	}


	public Integer getMainMenuVersion() {
		return mainMenuVersion;
	}

	public void setMainMenuVersion(Integer mainMenuVersion) {
		this.mainMenuVersion = mainMenuVersion;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}


	
}
