package tr.com.metamorfoz.web.ui.resource.rfm;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import tr.com.metamorfoz.web.core.rfm.api.RfmApiService;
import tr.com.metamorfoz.web.shared.model.rfm.RfmGsmOperatorDto;

@Path("/api/gsmoperator")
@Produces(MediaType.APPLICATION_JSON)
public class RfmGsmOperatorResource {

	private RfmApiService rfmApiService;
	
	public RfmGsmOperatorResource(RfmApiService rfmApiService) {
		super();
		this.rfmApiService = rfmApiService;
	}
	
	@GET
	@UnitOfWork
	public List<RfmGsmOperatorDto> findAll(){
		return rfmApiService.findAllGsmOperatorDto();
	}
	
	@POST
	@UnitOfWork
	public RfmGsmOperatorDto create(RfmGsmOperatorDto rfmGsmOperatorDto) {
		return rfmApiService.createGsmOperatorDto(rfmGsmOperatorDto);
	}
	
	@GET
	@Path("{id}")
	@UnitOfWork
	public RfmGsmOperatorDto find(@PathParam("id") int id) {
		return rfmApiService.findGsmOperatorDtoSafely(id);
	}
	
	@PUT
	@Path("{id}")
	@UnitOfWork
	public RfmGsmOperatorDto update(@PathParam("id") int id, RfmGsmOperatorDto rfmGsmOperatorDto) {
		return rfmApiService.updateOperatorDto(id, rfmGsmOperatorDto);
	}
	
	@DELETE
	@UnitOfWork
	public void delete(@QueryParam("ids") List<Integer> ids) {
		rfmApiService.deleteAllGsmOperatorDto(ids);
	}
	
}
