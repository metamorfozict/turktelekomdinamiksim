package tr.com.metamorfoz.web.core.dstk.domain;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="TBL_OTASS_CAMPAIGN_STATISTICS")
public class DstkCampaignStatistic { // extends BaseEntity

	@Id
	@NotBlank
	@Length(max = 128)
	@Column(name="T_PK_SERVICEID", length = 128)
	private String serviceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="D_START_TIME")
	private Date startTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="D_END_TIME")
	private Date endTime;

	@Column(name="C_STATUS")
	private Integer status;
	
	@Column(name="N_TOTAL")
	private Integer total;
	
	@Column(name="N_SENT")
	private Integer sent;
	
	@Column(name="N_DELIVERED")
	private Integer delivered;
	
	@Column(name="N_BLACK_LISTED")
	private Integer blackListed;
	
	@Column(name="N_NON_DELIVERED")
	private Integer nonDelivered;
	
	@Column(name="N_ACCEPTED")
	private Integer accepted;

	@NotBlank
	@Length(max = 300)
	@Column(name="T_DESC", length = 300)
	private String description;

	@Column(name="B_DETAIL")
	private Blob detail;

	@NotBlank
	@Length(max = 50)
	@Column(name="T_TYPE", length = 50)
	private String type;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Integer getDelivered() {
		return delivered;
	}

	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}

	public Integer getBlackListed() {
		return blackListed;
	}

	public void setBlackListed(Integer blackListed) {
		this.blackListed = blackListed;
	}

	public Integer getAccepted() {
		return accepted;
	}

	public void setAccepted(Integer accepted) {
		this.accepted = accepted;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blob getDetail() {
		return detail;
	}

	public void setDetail(Blob detail) {
		this.detail = detail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getNonDelivered() {
		return nonDelivered;
	}

	public void setNonDelivered(Integer nonDelivered) {
		this.nonDelivered = nonDelivered;
	}
	
}
