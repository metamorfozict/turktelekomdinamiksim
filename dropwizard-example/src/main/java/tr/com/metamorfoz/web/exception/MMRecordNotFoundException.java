package tr.com.metamorfoz.web.exception;


public class MMRecordNotFoundException extends MMException {

	private static final long serialVersionUID = 4489665539786381076L;
	
	public MMRecordNotFoundException(String message) {
		super(message);
	}
	
}