package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.helloworld.cli.RenderCommand;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.dao.DstkInstanceDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkInstance;

public class DstkInstanceDomainService extends AbstractDomainCrudService<DstkInstance, DstkInstanceDao> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RenderCommand.class);
	
	public DstkInstanceDomainService(DstkInstanceDao dao) {
		super(dao);
	}

	public DstkInstance findUniqueDstkInstanceByVersion(int instance, String instanceHeader, int instanceVersion) {
		return 	getDao().findUniqueDstkInstanceByVersion(instance, instanceHeader, instanceVersion);
	}
	
	@Override
	public DstkInstance save(DstkInstance dstkInstance) {
		if (dstkInstance.getId() == null) {
			int maxVersion = findMaxInstanceVersion(dstkInstance.getInstanceHeader(), dstkInstance.getInstance());
			dstkInstance.setInstanceVersion(maxVersion + 1);
		}
		return super.save(dstkInstance);
	}

	public int findMaxInstanceVersion(String instanceHeader, int instance) {
		int result = 0;
		
		try {
			result = getDao().findMaxInstanceVersion(instanceHeader, instance);
		} catch (Throwable t) {
			LOGGER.info("New Instance previous version Not found : " + instanceHeader);
		}
		
		return result;
	}
	

	
}
