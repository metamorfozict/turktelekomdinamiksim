package tr.com.metamorfoz.web.core.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@MappedSuperclass
public abstract class BaseEntity {

	
	@GenericGenerator(name = "generator", strategy = "sequence-identity", parameters = @Parameter(name = "sequence", value = "SEQ_OTASS_GENERIC"))
    @Id
    @GeneratedValue(generator = "generator")
	@Column(name="N_ID")
	private Integer id;
	
	@Column(name="D_ENT_DATE")
	private Date creationTime;
	
	@Column(name="D_MOD_DATE")
	private Date modificationTime = new Date();
	
	@PreUpdate
	public void preUpdate() {
	    modificationTime = new Date();
	}
	  
	@PrePersist
	public void prePersist() {
	    Date now = new Date();
	    creationTime = now;
	    modificationTime = now;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}


}    