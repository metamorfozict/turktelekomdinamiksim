package tr.com.metamorfoz.web.shared.model.dstk.dstkmodule;

import java.util.List;

import tr.com.metamorfoz.web.shared.model.common.KeyDto;

public class DstkModuleDtoOptionsDto {

	private KeyDto question;
	private List<Integer> range;
	private KeyDto key;
	private KeyDto value;
	private KeyDto select;
	
	public DstkModuleDtoOptionsDto() {
	}
	
	public KeyDto getQuestion() {
		return question;
	}
	public void setQuestion(KeyDto question) {
		this.question = question;
	}
	public List<Integer> getRange() {
		return range;
	}
	public void setRange(List<Integer> range) {
		this.range = range;
	}
	public KeyDto getKey() {
		return key;
	}
	public void setKey(KeyDto key) {
		this.key = key;
	}
	public KeyDto getValue() {
		return value;
	}
	public void setValue(KeyDto value) {
		this.value = value;
	}

	public KeyDto getSelect() {
		return select;
	}

	public void setSelect(KeyDto select) {
		this.select = select;
	}

	

}
