package tr.com.metamorfoz.web.core.campaign.statistic;

public class Statistic {

	//===============================================================================================
	// Keeps statistics of all services for a single port.
	//===============================================================================================
	private static CampaignStatisticCache<String, ServiceCounter> statistics = new CampaignStatisticCache<String, ServiceCounter>(StatisticConstants.CACHE_SIZE);

	/**
	 * adds new service to statistics list.
	 * 
	 * @param 					serviceId					the service identifier.
	 */
	public static void addToStatistics(String serviceId) {
		statistics.put(serviceId, new ServiceCounter());
	}
	
	/**
	 * increments counters by one for the given serviceId and counter type.
	 * 
	 * @param 					serviceId					the service identifier.
	 * @param 					counterType					the counter type.
	 */
	public static void incrementCounters(String serviceId, CounterType counterType) {
		 statistics.get(serviceId).getCounter(counterType).incrementCounters();
	 }
	 
	/**
	 * increments the counters by the given value delta for the given service identifier and counter type.
	 * 
	 * @param 					serviceId					the service identifier.
	 * @param 					counterType					the counter type.
	 * @param 					delta						the delta value to increment.
	 */
	 public static void incrementCounters(String serviceId, CounterType counterType, long delta) {
		 statistics.get(serviceId).getCounter(counterType).incrementCounters(delta);
	 }
	
	 /**
	  * resets the counter value for the given service identifier and the counter type.
	  * 
	  * @param 					serviceId					the service identifier.
	  * @param 					counterType					the counter type.
	  */
	 public static void resetCounters(String serviceId, CounterType counterType) {
		 statistics.get(serviceId).getCounter(counterType).resetCounters();
	 }
	 
	 /**
	  * resets the periodic value for the given service identifier and the counter type.
	  * 
	  * @param 					serviceId					the service identifier.
	  * @param 					counterType					the counter type.
	  */
	 public static void resetPeriodicValue(String serviceId, CounterType counterType) {
		 statistics.get(serviceId).getCounter(counterType).resetPeriodicValue();
	 }
	 
	 /**
	  * resets periodic value for all proceeding campaigns.
	  */
	 public static void resetPeriodicValues() {
		 
		 for (ServiceCounter serviceCounter : statistics.map.values()) {
			 for (CounterType counterType : CounterType.values()) {
				 serviceCounter.getCounter(counterType).resetPeriodicValue();
			 }
		 }
		 
	 }
	 
	 /**
	  * updates statistics to DB.
	  * 
	  */
	 public static void updateStatistics() {
		 
		 //==========================================================================================
		 // TODO : refactor with hibernate
		 //==========================================================================================
		 for (String serviceId : statistics.map.keySet()) {
			 // Main.getCampaignServer().getDaoCampaign().updateStatistic(serviceId, statistics.get(serviceId));
		 }
		 
	 }
	
}