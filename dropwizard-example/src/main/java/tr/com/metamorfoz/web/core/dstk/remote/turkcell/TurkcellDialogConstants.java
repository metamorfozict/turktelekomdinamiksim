package tr.com.metamorfoz.web.core.dstk.remote.turkcell;

public class TurkcellDialogConstants {

	public static final String SUCCESS 	  		= "OK";
	public static final String FIELD_SEPARATOR  = "||"; 
	public static final int	   RETURN_CODE		= 0;
	
}
