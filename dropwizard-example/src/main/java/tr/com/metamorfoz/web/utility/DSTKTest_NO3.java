package tr.com.metamorfoz.web.utility;

import java.util.List;

import tr.com.metamorfoz.common.CommonUtil;
import tr.com.metamorfoz.dstk.encoder.command.EncoderException;
import tr.com.metamorfoz.dstk.encoder.command.SecureDataGeneratorV1;
import tr.com.metamorfoz.dstk.encoder.model.EngineVersion;
import tr.com.metamorfoz.dstk.encoder.model.ModulePosition;
import tr.com.metamorfoz.dstk.encoder.model.ModuleState;
import tr.com.metamorfoz.dstk.encoder.model.Position;
import tr.com.metamorfoz.dstk.encoder.model.ProfileConfig;
import tr.com.metamorfoz.dstk.encoder.model.ServiceDesigner;
import tr.com.metamorfoz.dstk.encoder.model.impl.ModuleImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceDesignerImpl;
import tr.com.metamorfoz.dstk.encoder.model.impl.ServiceProviderImpl;
import tr.com.metamorfoz.dstk.encoder.util.ConvType;
import tr.com.metamorfoz.gcm.App;
import tr.com.metamorfoz.web.core.dstk.domain.DstkModule;

public class DSTKTest_NO3 {

	public static void sendScenario(DstkModule module) {
		
		try {
			
			String parameter = "09C40C081A15015232340A05500000"; // 13880C091A15015232340A05500000 09C40C081A15015232340A05500000
			
			ProfileConfig config = new ProfileConfig(parameter);
			
			ModuleImpl moduleImpl = new ModuleImpl(Long.parseLong("13107") 											  
												  , "1234567"
												  , EngineVersion.V1
												  , ModuleState.ACTIVATED												  
												  , false 																  
												  , Integer.parseInt(module.getNbCommands(), 16)																	  
												  , Integer.parseInt(module.getHexSize(), 16)															  
												  , ConvType.hexStringToByteArray(module.getOffsets())					  
												  , ConvType.hexStringToByteArray(module.getByteCode())
												  , (ServiceDesigner) new ServiceDesignerImpl("0000000000000000"
														  				   					 , new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}
														  				   					 , new ServiceProviderImpl("XXX", "YYY"))
												  , "Test");

			
			
			SecureDataGeneratorV1 secureDataGeneratorV1 = new SecureDataGeneratorV1();
			secureDataGeneratorV1.initialize(106 /* MT Max Size */, 121 /* MO Max Size */, config, false);
			
			try {
				
				Position position = new ModulePosition(12, 5);
				
				// add module
				secureDataGeneratorV1.addInstallModuleCommand(position, moduleImpl);
				
				// trig module
				secureDataGeneratorV1.addStartModuleCommand(moduleImpl);
				
			} catch (EncoderException encexc) {
				encexc.printStackTrace();
			}
			
			List<byte[]> secureDataList = secureDataGeneratorV1.generateSecureDataList();
			String[] contentStr = new String[secureDataList.size() + 1];
			contentStr[0] = "5E0102F368"; // 12-9 5 690102F373, 12-8 5 5E0102F368 
			System.out.println("contentStr[0] ->> " + contentStr[0]);
			for (int i = 0; i < secureDataList.size(); i++) {
				
				contentStr[i + 1] = CommonUtil.byteArrayToHexString(secureDataList.get(i));
				System.out.println("contentStr[" + (i + 1) + "] ->> " + contentStr[i + 1]);
			}
									
			//App.sendMessage(contentStr, "APA91bF_y3dHwQbJt1ub0OII4NzuNQP6x3tcl09dZCAF8SBE8KX55vDMm4Fq8DRRXyi50T5nMp4JrfPp4TVb9qjOdJ_zS9rfp9ku-SBr82c6WBKLpUMa34Br1GPifG5Ewx_nc3QeBz7z");
			App.sendMessage(contentStr, "APA91bHF8C4dYsMogIxOXlg-2OQkeqcUBA-jFMNO-4V5qCwMb5Jo1aOogPaO-NJfiRdJSqP7nqlb1Ud3xRPMfL86J1isehyRmKYVXREGReFVM0vSvJv3TQ2liy0GYQlTdlQAgc6DBFL-");	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}