package tr.com.metamorfoz.web.ui.resource.dstk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import freemarker.core._RegexBuiltins.replace_reBI;
import tr.com.metamorfoz.gcm.App;
import tr.com.metamorfoz.web.shared.model.dstk.DstkThemeDto;

public class DstkThemeUtil {
	
	//===========================================================================================
	// Logger definition.
	//===========================================================================================
	private static final Logger logger = Logger.getLogger(DstkThemeUtil.class);
	
	public static List<DstkThemeDto> readThemeFile(int id) {
		
		String path = App.PATH_THEMES.get("" + id);
		
		if (id == 0) {
			path = App.PATH_THEME;
		}
		
		// theme -B-
    	
    	Properties prop = new Properties();
    	InputStream input = null;
    	String themeId = "";
    	
    	try {
     
    		input = new FileInputStream(path);
     
    		// load a properties file
    		prop.clear();
    		prop.load(input);
     
    		themeId = prop.getProperty("id");
    			  
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	// theme -E-
		
		List<DstkThemeDto> result = new LinkedList<DstkThemeDto>();
		
		try {
			result.add(new DstkThemeDto(Integer.parseInt(themeId), new String(Files.readAllBytes(Paths.get(App.PATH_THEMES.get(themeId))))));
		} catch (Exception exc) {
			logger.error(exc);
		}
		
		return result;
		
	}
	
	private static void copyFile(File source, File dest) throws IOException {
		Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}
	
	public static DstkThemeDto writeThemeFile(int id, DstkThemeDto dstkThemeDto) {
		
		
		try {
			
			//===================================================================================================================
			// Find the file to be updated.
			//===================================================================================================================
			String fileName = App.PATH_THEMES.get("" + id);
			
			//===================================================================================================================
			// Update the file.
			//===================================================================================================================
			Files.write(Paths.get(fileName), dstkThemeDto.getTheme().getBytes(), new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING });
			
			//===================================================================================================================
			// Copy as default theme.
			//===================================================================================================================
			copyFile(new File(fileName), new File(App.PATH_THEME));
			
		} catch (Exception exc) {
			logger.error(exc);
		}
		
		return dstkThemeDto;
		
	}

}
