package tr.com.metamorfoz.web.core.campaign.statistic;

import java.util.TimerTask;

public class StatisticWriterTask extends TimerTask {

	/**
	 * updates the statistics value 
	 */
	public void run() {

		//==================================================================================
		// all statistics initialized in campaign task. update current value to DB.
		//==================================================================================
		Statistic.updateStatistics();
		
		//==================================================================================
		// Reset statistics.
		//==================================================================================
		Statistic.resetPeriodicValues();
		
	}

}