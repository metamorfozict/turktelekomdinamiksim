package tr.com.metamorfoz.web.core.common.converter;


public interface EntityConverter<E, D> {

	public E createEntity(final D dto);
	public void updateEntity(final E entity, D dto);
	public D convertToDto(final E entity);
	
}
