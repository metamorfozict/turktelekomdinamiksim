package tr.com.metamorfoz.web.shared.model.dstk.dstkmodule;

import java.util.Date;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class DstkModuleDto {

	private Integer id;
	
	private Integer moduleId;
	
	@NotBlank
	@Length(max = 30)
	private String moduleName;
	
	private String byteCode;
	
	private String terminalModules;
	private String terminalModuleOffsets;
	private String offsets;
	private String hexSize;
	private String numOfCommands;
	private String json;
	private String description;
	private Integer type;
	private Date creationTime;
	private Date modificationTime;
	
	public DstkModuleDto(Integer id, Integer moduleId, String moduleName, String byteCode, String terminalModules,
			String terminalModuleOffsets, String offsets, String hexSize, String numOfCommands, String json, String description,
			Integer type, Date creationTime, Date modificationTime) {
		super();
		this.id = id;
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.byteCode = byteCode;
		this.terminalModules = terminalModules;
		this.terminalModuleOffsets = terminalModuleOffsets;
		this.offsets = offsets;
		this.hexSize = hexSize;
		this.numOfCommands = numOfCommands;
		this.json = json;
		this.description = description;
		this.type = type;
		this.creationTime = creationTime;
		this.modificationTime = modificationTime;
	}
	
	public DstkModuleDto() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getByteCode() {
		return byteCode;
	}

	public void setByteCode(String byteCode) {
		this.byteCode = byteCode;
	}

	public String getTerminalModules() {
		return terminalModules;
	}

	public void setTerminalModules(String terminalModules) {
		this.terminalModules = terminalModules;
	}

	public String getTerminalModuleOffsets() {
		return terminalModuleOffsets;
	}

	public void setTerminalModuleOffsets(String terminalModuleOffsets) {
		this.terminalModuleOffsets = terminalModuleOffsets;
	}

	public String getOffsets() {
		return offsets;
	}

	public void setOffsets(String offsets) {
		this.offsets = offsets;
	}

	public String getHexSize() {
		return hexSize;
	}

	public void setHexSize(String hexSize) {
		this.hexSize = hexSize;
	}

	public String getNumOfCommands() {
		return numOfCommands;
	}

	public void setNumOfCommands(String numOfCommands) {
		this.numOfCommands = numOfCommands;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	public String getName() {
		return this.moduleName;
	}
	
	public String getStrType() {
		switch (this.type) {
			case 0 :
				return "PUSH";
			case 1 :
				return "MENU";
			case 2 :
				return "COMMON  MODULE";
			default :
				return "UNKNOWN TYPE";
		}
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}
	
}
