package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.dao.DstkMainMenuDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkMainMenu;

public class DstkMainMenuDomainService extends AbstractDomainCrudService<DstkMainMenu, DstkMainMenuDao> {

	public DstkMainMenuDomainService(DstkMainMenuDao dstkMainMenuDao) {
		super(dstkMainMenuDao);
	}
	
	public DstkMainMenu findUniqueDstkMainMenuByVersion(String mainMenuHeader, int mainMenuVersion) {
		return getDao().findUniqueDstkMainMenuByVersion(mainMenuHeader, mainMenuVersion);
	}
	
	@Override
	public DstkMainMenu save(DstkMainMenu dstkMainMenu) {
		if (dstkMainMenu.getId() == null) {
			int maxVersion = findMaxMainMenuVersion(dstkMainMenu.getMainMenuHeader());
			dstkMainMenu.setMainMenuVersion(maxVersion + 1);
		}
		return super.save(dstkMainMenu);
	}

	public int findMaxMainMenuVersion(String mainMenuHeader) {
		return getDao().findMaxMainMenuVersion(mainMenuHeader);
	}
	
}
