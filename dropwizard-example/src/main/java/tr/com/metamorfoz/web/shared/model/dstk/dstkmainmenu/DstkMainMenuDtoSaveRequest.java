package tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu;

import java.util.List;

import javax.persistence.Column;

import org.hibernate.validator.constraints.NotBlank;

public class DstkMainMenuDtoSaveRequest {

	private Integer id;
	
	@NotBlank
	@Column(length=25)
	private String title;
	
	private List<DstkMainMenuItemDto> items;
	
	public DstkMainMenuDtoSaveRequest() {
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<DstkMainMenuItemDto> getItems() {
		return items;
	}
	public void setItems(List<DstkMainMenuItemDto> items) {
		this.items = items;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
}
