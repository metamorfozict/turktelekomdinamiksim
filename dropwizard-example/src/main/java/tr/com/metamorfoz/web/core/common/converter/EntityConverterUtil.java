package tr.com.metamorfoz.web.core.common.converter;

import java.util.ArrayList;
import java.util.List;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;

public class EntityConverterUtil {

	public static <D, E> List<D> convertToDtoList(EntityConverter<E, D> converter, List<E> entityList) {
		List<D> dtoList = new ArrayList<D>();
		for (E entity : entityList) {
			D dto = converter.convertToDto(entity);
			dtoList.add(dto);
		} 
		return dtoList;
	}
	
}
