package tr.com.metamorfoz.web.core.dstk.converter;

import java.util.Date;

import tr.com.metamorfoz.web.core.common.converter.AbstractEntityConverter;
import tr.com.metamorfoz.web.core.dstk.domain.DstkMainMenu;
import tr.com.metamorfoz.web.shared.model.dstk.dstkmainmenu.DstkMainMenuDto;

public class DstkMainMenuConverter extends AbstractEntityConverter<DstkMainMenu, DstkMainMenuDto> {

	@Override
	public DstkMainMenu createEntity(DstkMainMenuDto dto) {
		DstkMainMenu entity = new DstkMainMenu();
		updateEntity(entity, dto);
		return entity;
	}

	@Override
	public void updateEntity(DstkMainMenu entity, DstkMainMenuDto dto) {
		entity.setMainMenuHeader(dto.getMainMenuHeader());
		entity.setMainMenuVersion(dto.getMainMenuVersion());
		entity.setJson(dto.getJson());
		entity.setCreationTime(new Date());
	}

	@Override
	public DstkMainMenuDto convertToDto(DstkMainMenu entity) {
		DstkMainMenuDto dto = new DstkMainMenuDto();
		dto.setId(entity.getId());
		dto.setMainMenuHeader(entity.getMainMenuHeader());
		dto.setMainMenuVersion(entity.getMainMenuVersion());
		dto.setJson(entity.getJson());
		dto.setModificationTime(entity.getModificationTime());
		return dto;
	}

}
