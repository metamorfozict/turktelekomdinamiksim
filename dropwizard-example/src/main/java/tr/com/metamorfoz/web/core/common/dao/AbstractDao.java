package tr.com.metamorfoz.web.core.common.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

public class AbstractDao<T> extends AbstractDAO<T> {

	public AbstractDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

    public T uniqueResult(Criteria criteria)  {
        return super.uniqueResult(criteria);
    }

    public T uniqueResult(Query query)  {
        return super.uniqueResult(query);
    }

    public List<T> list(Criteria criteria)  {
    	return super.list(criteria);
    }

    public List<T> list(Query query)  {
        return super.list(query);
    }

    public T get(Serializable id) {
        return super.get(id);
    }

    public T persist(T entity)  {
        return super.persist(entity);
    }
    
    public <T> T initialize(T proxy)  {
       return super.initialize(proxy);
    }

    public List<T> list()  {
    	return currentSession().createCriteria(getEntityClass()).list();
    }

    public void delete(T entity) {
    	currentSession().delete(entity);
    }
    
	public int deleteAll(List<Integer> ids) {
		String queryString ="delete from " + getEntityClass().getSimpleName() + " where id in(:ids)";
		Query query = currentSession().createQuery(queryString)
										.setParameterList("ids", ids);
		return query.executeUpdate();
	}

	
}
