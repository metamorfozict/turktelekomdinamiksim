package tr.com.metamorfoz.web.shared.model.stup;


import org.hibernate.validator.constraints.Length;

public class StupRuleDto  {

	private Integer id;
	
	private Integer balanceBelowLimit;
	private String balanceBelowLimitStr;

	@Length(max = 50)
	private String lineType;
	
	private Long lastTransaction;	
	private String lastTransactionStr;
	
	private Integer usualAmount;
	private String usualAmountStr;
	
	private String moduleName; 
	  
	private Integer validFrom; 
		  
	private Integer validTo; 

	private Integer maxCount; 
	private String maxCountStr;

	private Integer repetition;
	private String repetitionInterval;

	private Long duration; 
	
	public StupRuleDto() {
	}
	
	public StupRuleDto(Integer id
					  , Integer balanceBelowLimit
					  , String lineType
					  , Long lastTransaction
					  , Integer usualAmount
					  , String moduleName
					  , Integer validFrom
					  , Integer validTo
					  , Integer maxCount
					  , Integer repetition
					  , Long duration) {
		this.id = id;
		this.balanceBelowLimit = balanceBelowLimit;
		this.lineType = lineType;
		this.lastTransaction = lastTransaction;
		this.usualAmount = usualAmount;
		this.moduleName = moduleName;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.maxCount = maxCount;
		this.repetition = repetition;
		this.duration = duration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBalanceBelowLimit() {
		return balanceBelowLimit;
	}

	public String getBalanceBelowLimitStr() {
		if (this.balanceBelowLimit == 0) {
			return "ANY";
		}
		
		return "" + this.balanceBelowLimit;
		
	}
	
	public void setBalanceBelowLimit(Integer balanceBelowLimit) {
		this.balanceBelowLimit = balanceBelowLimit;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public Long getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(Long lastTransaction) {
		this.lastTransaction = lastTransaction;
	}

	public String getLastTransactionStr() {
		if (this.lastTransaction == 0) {
			return "ANY";
		}
		
		return "" + (this.lastTransaction / STUPConstants.WEEK_IN_MILLISECONDS);
		
	}
	
	public Integer getUsualAmount() {
		return usualAmount;
	}

	public void setUsualAmount(Integer usualAmount) {
		this.usualAmount = usualAmount;
	}
	
	public String getUsualAmountStr() {
		
		if (this.usualAmount == 0) {
			return "ANY";
		}
		
		return "" + this.usualAmount;
		
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Integer validFrom) {
		this.validFrom = validFrom;
	}

	public Integer getValidTo() {
		return validTo;
	}

	public void setValidTo(Integer validTo) {
		this.validTo = validTo;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public String getMaxCountStr() {
		
		if (this.maxCount == 0) {
			return "ANY";
		}
		
		return "" + this.maxCount;
		
	}
	
	public Integer getRepetition() {
		return repetition;
	}

	public void setRepetition(Integer repetition) {
		this.repetition = repetition;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}
	
	public void setRepetitionInterval(String repetitionInterval) {
		
		if (("Once-A-Day").equals(repetitionInterval)) {
			this.repetition = 1;
			this.duration = STUPConstants.DAY_IN_MILLISECONDS;
		} else if (("Twice-A-Day").equals(repetitionInterval)) {
			this.repetition = 2;
			this.duration = STUPConstants.DAY_IN_MILLISECONDS;
		} else if (("Once-A-Week").equals(repetitionInterval)) {
			this.repetition = 1;
			this.duration = STUPConstants.WEEK_IN_MILLISECONDS;
		} else if (("Twice-A-Week").equals(repetitionInterval)) {
			this.repetition = 2;
			this.duration = STUPConstants.WEEK_IN_MILLISECONDS;
		} else {
			this.repetition = 0;
			this.duration = 0L;
		}
		
	}
	
	public String getRepetitionInterval() {
		
		if ((this.repetition == 1) && (this.duration == STUPConstants.DAY_IN_MILLISECONDS)) {
			return STUPConstants.KEYWORD_ONCE_A_DAY;
		} else if ((this.repetition == 2) && (this.duration == STUPConstants.DAY_IN_MILLISECONDS)) {
			return STUPConstants.KEYWORD_TWICE_A_DAY;
		} else if ((this.repetition == 1) && (this.duration == STUPConstants.WEEK_IN_MILLISECONDS)) {
			return STUPConstants.KEYWORD_ONCE_A_WEEK;
		} else if ((this.repetition == 2) && (this.duration == STUPConstants.WEEK_IN_MILLISECONDS)) {
			return STUPConstants.KEYWORD_TWICE_A_WEEK;
		}
		
		return "ANY";
		
	}
	
}