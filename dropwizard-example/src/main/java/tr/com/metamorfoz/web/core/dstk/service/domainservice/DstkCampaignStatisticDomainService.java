package tr.com.metamorfoz.web.core.dstk.service.domainservice;

import java.util.List;

import tr.com.metamorfoz.web.core.common.service.AbstractDomainCrudService;
import tr.com.metamorfoz.web.core.dstk.dao.DstkCampaignStatisticDao;
import tr.com.metamorfoz.web.core.dstk.domain.DstkCampaignStatistic;

public class DstkCampaignStatisticDomainService extends AbstractDomainCrudService<DstkCampaignStatistic, DstkCampaignStatisticDao> {

	public DstkCampaignStatisticDomainService(DstkCampaignStatisticDao dstkCampaignStatisticDao) {
		super(dstkCampaignStatisticDao);
	}

	public List<DstkCampaignStatistic> search(int status, String type) {
		return getDao().search(status, type);
	};


}
