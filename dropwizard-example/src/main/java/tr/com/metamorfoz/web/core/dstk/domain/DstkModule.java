package tr.com.metamorfoz.web.core.dstk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.utility.ObjectUtil;

@Entity
@Table(name="TBL_OTASS_DSTK_MODULES")
public class DstkModule extends BaseEntity {

	private static final Integer DEFAULT_VALUE_FOR_TYPE = null;
	
	@Column(name="N_PK_MODULEID")
	private Integer moduleId;
	
	@Length(max = 250)
	@Column(name="T_DESC", length=250)
	private String description;
	
	//TODO:Burada Enum kullanmak nasıl olur?
	@Column(name="C_TYPE")
	private Integer type;
	
	@Lob
	@Column(name="C_JSON")
	private String json;
	
	@Length(max = 10)
	@Column(name="T_NB_COMMANDS", length=10)
	private String nbCommands;
	
	@Length(max = 10)
	@Column(name="T_HEX_SIZE", length=10)
	private String hexSize;
	
	@Length(max = 500)
	@Column(name="T_OFFSETS", length=500)
	private String offsets;
	
	@Length(max = 1000)
	@Column(name="T_TERMINAL_MODULES", length=1000)
	private String terminalModules;
	
	@Length(max = 1000)
	@Column(name="T_TERMINAL_MODULE_OFFSETS", length=1000)
	private String terminalModuleOffsets;
	
	@Column(name="C_BYTE_CODE")
	private String byteCode;
	
	@Length(max = 30)
	@Column(name="T_MODULE_NAME", length=30)
	private String moduleName;

	public DstkModule() {
		super();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNbCommands() {
		return nbCommands;
	}

	public void setNbCommands(String nbCommands) {
		this.nbCommands = nbCommands;
	}

	public String getHexSize() {
		return hexSize;
	}

	public void setHexSize(String hexSize) {
		this.hexSize = hexSize;
	}

	public String getOffsets() {
		return offsets;
	}

	public void setOffsets(String offsets) {
		this.offsets = offsets;
	}

	public String getTerminalModules() {
		return terminalModules;
	}

	public void setTerminalModules(String terminalModules) {
		this.terminalModules = terminalModules;
	}

	public String getTerminalModuleOffsets() {
		return terminalModuleOffsets;
	}

	public void setTerminalModuleOffsets(String terminalModuleOffsets) {
		this.terminalModuleOffsets = terminalModuleOffsets;
	}

	
	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getByteCode() {
		return byteCode;
	}

	public void setByteCode(String byteCode) {
		this.byteCode = byteCode;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Integer getType() {
		return (Integer) ObjectUtil.defaultIfNull(type, DEFAULT_VALUE_FOR_TYPE);
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
}
