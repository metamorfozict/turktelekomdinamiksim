package tr.com.metamorfoz.web.core.common.service;

import java.util.List;

import tr.com.metamorfoz.web.core.common.domain.BaseEntity;
import tr.com.metamorfoz.web.exception.MMRecordNotFoundException;

public interface DomainCrudService<E> {

	public abstract List<E> findAll();

	public abstract E find(int id);

	public abstract E findSafely(int id) throws MMRecordNotFoundException;

	public abstract E save(E t);

	public abstract void delete(E t);

	public abstract int deleteAll(List<Integer> ids);

}