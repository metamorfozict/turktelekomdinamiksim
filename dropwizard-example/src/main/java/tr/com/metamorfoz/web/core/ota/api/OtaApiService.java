package tr.com.metamorfoz.web.core.ota.api;

import java.util.List;

import tr.com.metamorfoz.web.core.common.converter.EntityConverterUtil;
import tr.com.metamorfoz.web.core.ota.converter.OtaTransportKeyConverter;
import tr.com.metamorfoz.web.core.ota.domain.OtaTransportKey;
import tr.com.metamorfoz.web.core.ota.service.OtaService;
import tr.com.metamorfoz.web.shared.model.ota.OtaTransportKeyDto;

public class OtaApiService {
	
	private OtaService otaService;
	
	private OtaTransportKeyConverter transportKeyDtoConverter = new OtaTransportKeyConverter();
	
	public OtaApiService(OtaService otaService) {
		super();
		this.otaService = otaService;
	}

	public OtaTransportKeyDto createTransportKeyDto(OtaTransportKeyDto transportKeyDto) {
		//Assumption:Validation should be done at ui.
		OtaTransportKey transportKey = transportKeyDtoConverter.createEntity(transportKeyDto);
		transportKey = otaService.getTransportKeyDomainService().save(transportKey);
		OtaTransportKeyDto keyDto = transportKeyDtoConverter.convertToDto(transportKey);
		return keyDto;
	}

	public OtaTransportKeyDto updateTransportKeyDto(int id, OtaTransportKeyDto transportKeyDto) {
		OtaTransportKey transportKey = otaService.getTransportKeyDomainService().findSafely(id);
		transportKeyDtoConverter.updateEntity(transportKey, transportKeyDto);
//		transportKey.update(transportKeyDto.getNo(), transportKeyDto.getValue(), transportKeyDto.getAlgorithmName(), transportKeyDto.getAlgorithmMode());
		transportKey = otaService.getTransportKeyDomainService().save(transportKey);
		return transportKeyDtoConverter.convertToDto(transportKey);
	}
	
	public List<OtaTransportKeyDto> findAllTransportKeyDto(){
		List<OtaTransportKey> transportKeys = otaService.getTransportKeyDomainService().findAll();
		return EntityConverterUtil.convertToDtoList(transportKeyDtoConverter, transportKeys);
	}

	public OtaTransportKeyDto findTransportKeyDtoSafely(int id)   {
		OtaTransportKey transportKey = otaService.getTransportKeyDomainService().find(id);
		return transportKeyDtoConverter.convertToDto(transportKey);
	}
	
	public void deleteTransportKeyDto(int id) {
		OtaTransportKey transportKey = otaService.getTransportKeyDomainService().findSafely(id);
		otaService.getTransportKeyDomainService().delete(transportKey);
	}
	
	public int deleteAllTransportKeyDto(List<Integer> ids) {
		return otaService.getTransportKeyDomainService().deleteAll(ids);
	}
	

	public List<OtaTransportKeyDto> searchTransportKeyDto(String status) {
		List<OtaTransportKey> transportKeys = otaService.getTransportKeyDomainService().search(status);
		return EntityConverterUtil.convertToDtoList(transportKeyDtoConverter, transportKeys);
	}

		
}
