package tr.com.metamorfoz.gcm;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class App 
{
	
	//==================================================================================================================================
	// Local resources
	//==================================================================================================================================
	public static final String 							  PATH_THEME  = "C:\\customers\\Lama\\ui.properties";
	public static final String 							  PATH_DEPOT  = "C:\\customers\\Lama\\themes\\";
	
	//==================================================================================================================================
	// Production resources
	//==================================================================================================================================
	// public static final String 							  PATH_THEME  = "/opt/processes/GUI/resources/ui.properties";
	// public static final String 							  PATH_DEPOT  = "/opt/processes/GUI/resources/themes/";
	
	public static final ConcurrentHashMap<String, String> PATH_THEMES = new ConcurrentHashMap<String, String>();
	
	static {
		
		PATH_THEMES.put("1", PATH_DEPOT + "ui-akbank.properties");
		PATH_THEMES.put("2", PATH_DEPOT + "ui-avea.properties");
		PATH_THEMES.put("3", PATH_DEPOT + "ui-garanti.properties");
		PATH_THEMES.put("4", PATH_DEPOT + "ui-turkcell.properties");
		PATH_THEMES.put("5", PATH_DEPOT + "ui-vodafone.properties");
		
	}
	
    public static void main( String[] args )
    {
        System.out.println( "Sending POST to GCM" );
        
        String apiKey = "AIzaSyAsMn49fxxMKL7H2guNNo0TPrDSGY7ivq4";
        Content content = createContent();
        
        POST2GCM.post(apiKey, content);
    }
    
    public static Content createContent(){
		
		Content c = new Content();
		
		String contentStr = "5E0102F368;36030FFD6833330400000003000E0018003139FE6833330031000000003381F403414141FC01201BFF00F403424242010A4E3301850343434386000C01000B815023435908F50000FFFFFF0003F43333";
		
		c.addRegId("APA91bGHk1vmRNrpdMyo-imbCaA88-nZMC_ZHm24kSee_D_Yzz4LIrMFmaOYSPwYX0rNBToZmqFHYfjb811R-jbFQfIRwH7tChkmYlpIUCYPYxw_y0tQFQp7caqFPOYbN_81UBy28SGg");
		c.createData("Test Title", contentStr);
		
		return c;
	}
    
    public static Content createContent(String[] messageArr, String mobileId) {
    	
    	// theme -B-
    	
    	Properties prop = new Properties();
    	InputStream input = null;
    	String theme = "";
    	
    	try {
     
    		input = new FileInputStream(PATH_THEME);
     
    		// load a properties file
    		prop.clear();
    		prop.load(input);
     
    		theme = prop.getProperty("logoPath") + ","
    			  + prop.getProperty("buttonColor") + ","
    			  + prop.getProperty("editTextDefaultColor") + ","
    			  + prop.getProperty("editTextFocusedColor") + ","
    			  + prop.getProperty("selectItemColor") + ","
    			  + prop.getProperty("textColor") + ","
    			  + prop.getProperty("errorTextColor") + ","
    			  + prop.getProperty("backgroundColor");
    			  
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}

    	
    	// theme -E-
		
		Content c = new Content();
		
		// String contentStr = "5E0102F368;36030FFD6833330400000003000E0018003139FE6833330031000000003381F403414141FC01201BFF00F403424242010A4E3301850343434386000C01000B815023435908F50000FFFFFF0003F43333";
		String contentStr = theme + ";";
		for (String str : messageArr) {
			contentStr += str + ";";
		}
		
		contentStr = contentStr.substring(0, contentStr.length() - 1);
		
		// TODO : for HTC open here!!!
		// c.addRegId("APA91bGHk1vmRNrpdMyo-imbCaA88-nZMC_ZHm24kSee_D_Yzz4LIrMFmaOYSPwYX0rNBToZmqFHYfjb811R-jbFQfIRwH7tChkmYlpIUCYPYxw_y0tQFQp7caqFPOYbN_81UBy28SGg");
		// TODO : for SMS open here!!!
		// c.addRegId("APA91bG1_iJ8jwLNt1VprCf8VcyI46CqUR2_gCeKbZqwOiuhC2uuyDszVT0ZFb7PkYiEaoNU9-qQq6pqpCiK_e4jcZOtaoEdbLiuRH-6OhVmfAuD_8OWOC-VZcvwTyO89m4qgQEKfm_z");
		c.addRegId(mobileId);
		c.createData("Test Title", contentStr);
		
		return c;
	}
    
    public static void sendMessage(String[] messageArr, String mobileId) {
    	
    	System.out.println( "Sending POST to GCM" );
        
        String apiKey = "AIzaSyAsMn49fxxMKL7H2guNNo0TPrDSGY7ivq4";
        Content content = createContent(messageArr, mobileId);
        
        POST2GCM.post(apiKey, content);
    	
    }
    
}
