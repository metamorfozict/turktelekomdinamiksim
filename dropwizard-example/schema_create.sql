
    drop table TBL_OTASS_DSTK_INSTANCES_BCKP cascade constraints

    create table TBL_OTASS_DSTK_INSTANCES_BCKP (
        id number(10,0) not null,
        creationTime date,
        modificationTime date,
        T_AID varchar2(32),
        T_CONTENT blob,
        N_PK_INSTANCE number(10,0),
        T_PK_INSTANCE_HEADER varchar2(100),
        N_PK_ORDER number(10,0),
        T_INTR_BLOCK varchar2(250),
        T_JSON blob,
        T_PROFILE varchar2(200),
        T_TAR varchar2(10),
        primary key (id)
    )
